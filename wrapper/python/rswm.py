import ctypes
import ctypes.util

libpath = ctypes.util.find_library('rswmc')

if not libpath:
    raise 'rswmc library is not found'

lib = ctypes.cdll.LoadLibrary(libpath)

lib.task_create.restype = ctypes.c_void_p
lib.task_destroy.argtypes = [ctypes.c_void_p]

lib.task_set_name.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
lib.task_get_name.argtypes = [ctypes.c_void_p]
lib.task_get_name.restype = ctypes.c_char_p

lib.task_set_file.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
lib.task_get_file.argtypes = [ctypes.c_void_p]
lib.task_get_file.restype = ctypes.c_char_p

lib.task_set_workdir.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
lib.task_get_workdir.argtypes = [ctypes.c_void_p]
lib.task_get_workdir.restype = ctypes.c_char_p

lib.task_set_stdout_log.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
lib.task_get_stdout_log.argtypes = [ctypes.c_void_p]
lib.task_get_stdout_log.restype = ctypes.c_char_p

lib.task_set_stderr_log.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
lib.task_get_stderr_log.argtypes = [ctypes.c_void_p]
lib.task_get_stderr_log.restype = ctypes.c_char_p

lib.task_set_timeout_soft.argtypes = [ctypes.c_void_p, ctypes.c_uint]
lib.task_get_timeout_soft.argtypes = [ctypes.c_void_p]
lib.task_get_timeout_soft.restype = ctypes.c_uint

lib.task_set_timeout_hard.argtypes = [ctypes.c_void_p, ctypes.c_uint]
lib.task_get_timeout_hard.argtypes = [ctypes.c_void_p]
lib.task_get_timeout_hard.restype = ctypes.c_uint

lib.task_add_arg.argtypes = [ctypes.c_void_p, ctypes.c_char_p]

lib.task_set_shell_cmd.argtypes = [ctypes.c_void_p, ctypes.c_char_p]

lib.taskgraph_create.restype = ctypes.c_void_p
lib.taskgraph_destroy.argtypes = [ctypes.c_void_p]

lib.taskgraph_add.restype = ctypes.c_ulonglong
lib.taskgraph_add.argtypes = [ctypes.c_void_p, ctypes.c_void_p]

lib.taskgraph_add_next.restype = ctypes.c_ulonglong
lib.taskgraph_add_next.argtypes = [ctypes.c_void_p, ctypes.c_void_p]

lib.taskgraph_add_after.restype = ctypes.c_ulonglong
lib.taskgraph_add_after.argtypes = [ctypes.c_void_p, ctypes.c_void_p, ctypes.c_ulonglong]

lib.pipeline_create.restype = ctypes.c_void_p
lib.pipeline_destroy.argtypes = [ctypes.c_void_p]
lib.pipeline_write_profile.argtypes = [ctypes.c_void_p, ctypes.c_char_p]
lib.pipeline_run.argtypes = [ctypes.c_void_p, ctypes.c_void_p]
lib.pipeline_set_max_accel_delta.argtypes = [ctypes.c_void_p, c_double]
lib.pipeline_set_ucb_coeff.argtypes = [ctypes.c_void_p, c_double]
lib.pipeline_set_fairness_coeff.argtypes = [ctypes.c_void_p, c_double]
lib.pipeline_set_efficiency_coeff.argtypes = [ctypes.c_void_p, c_double]

class Task(object):
    def __init__(self):
        self.obj = lib.task_create()

    def __del__(self):
        lib.task_destroy(self.obj)

    def set_name(self, s):
        buf = s.encode('utf-8')
        lib.task_set_name(self.obj, buf)

    def get_name(self):
        buf = lib.task_get_name(self.obj)
        return buf.decode('utf-8')

    def set_file(self, s):
        lib.task_set_file(self.obj, s.encode('utf-8'))

    def get_file(self):
        buf = lib.task_get_file(self.obj)
        return buf.decode('utf-8')

    def set_workdir(self, s):
        lib.task_set_workdir(self.obj, s.encode('utf-8'))

    def get_workdir(self):
        buf = lib.task_get_workdir(self.obj)
        return buf.decode('utf-8')

    def set_timeout_hard(self, sec):
        lib.task_set_timeout_hard(self.obj, sec)

    def get_timeout_hard(self):
        return lib.task_get_timeout_hard(self.obj)

    def set_timeout_soft(self, sec):
        lib.task_set_timeout_soft(self.obj, sec)

    def get_timeout_soft(self):
        return lib.task_get_timeout_soft(self.obj)

    def set_stdout_log(self, s):
        lib.task_set_stdout_log(self.obj, s.encode('utf-8'))

    def get_stdout_log(self):
        buf = lib.task_get_stdout_log(self.obj)
        return buf.decode('utf-8')

    def set_stderr_log(self, s):
        lib.task_set_stderr_log(self.obj, s.encode('utf-8'))

    def get_stderr_log(self):
        buf = lib.task_get_stderr_log(self.obj)
        return buf.decode('utf-8')

    def set_shell_cmd(self, s):
        lib.task_set_workdir(self.obj, s.encode('utf-8'))

    def add_arg(self, s):
        lib.task_add_arg(self.obj, s.encode('utf-8'))

    def set_args(self, args):
        for a in args:
            self.add_arg(str(a))

class TaskGraph(object):
    def __init__(self):
        self.obj = lib.taskgraph_create()

    def __del__(self):
        lib.taskgraph_destroy(self.obj)

    def add(self, task):
        return lib.taskgraph_add(self.obj, task.obj)

    def add_next(self, task):
        return lib.taskgraph_add_next(self.obj, task.obj)

    def add_after(self, task_id, task):
        return lib.taskgraph_add_after(self.obj, task_id, task.obj)

class Pipeline(object):
    def __init__(self):
        self.obj = lib.pipeline_create()

    def __del__(self):
        lib.pipeline_destroy(self.obj)

    def run(self, graph):
        lib.pipeline_run(self.obj, graph.obj)

    def write_profile(self, filename):
        lib.pipeline_write_profile(self.obj, filename.encode('utf-8'))

    def set_config(self, cfg):
        if cfg['max_accel_delta']:
            lib.pipeline_set_max_accel_delta(self.obj, cfg['max_accel_delta'])
        if cfg['ucb_coeff']:
            lib.pipeline_set_ucb_coeff(self.obj, cfg['ucb_coeff'])
        if cfg['fairness_coeff']:
            lib.pipeline_set_fairness_coeff(self.obj, cfg['fairness_coeff'])
        if cfg['efficiency_coeff']:
            lib.pipeline_set_efficiency_coeff(self.obj, cfg['efficiency_coeff'])

