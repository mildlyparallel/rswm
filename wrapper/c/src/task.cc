#include <chrono>

#include "include/task.h"

#include "Task.hh"

Task* task_create(){
	return new Task();
}

void task_destroy(Task *task) {
	delete task;
}

void task_set_name(Task *task, const char *name)
{
	task->set_name(name);
}

const char *task_get_name(Task *task)
{
	return task->get_name().c_str();
}

void task_set_file(Task *task, const char *file)
{
	task->set_file(file);
}

const char *task_get_file(Task *task)
{
	return task->get_file().c_str();
}

void task_set_workdir(Task *task, const char *wd)
{
	task->set_workdir(wd);
}

const char *task_get_workdir(Task *task)
{
	return task->get_workdir().c_str();
}

void task_set_shell_cmd(Task *task, const char *cmd)
{
	task->set_shell_cmd(cmd);
}

void task_add_arg(Task *task, const char *arg)
{
	task->add_arg(arg);
}

void task_set_stdout_log(Task *task, const char *file)
{
	task->set_stdout_log(file);
}

const char *task_get_stdout_log(Task *task)
{
	return task->get_stdout_log().c_str();
}

void task_set_stderr_log(Task *task, const char *file)
{
	task->set_stderr_log(file);
}

const char *task_get_stderr_log(Task *task)
{
	return task->get_stderr_log().c_str();
}

void task_set_timeout_soft(Task *task, unsigned sec)
{
	task->set_timeout_soft(std::chrono::duration<unsigned>(sec));
}

double task_get_timeout_soft(Task *task)
{
	return task->get_timeout_soft().count();
}

void task_set_timeout_hard(Task *task, unsigned sec)
{
	task->set_timeout_hard(std::chrono::duration<unsigned>(sec));
}

double task_get_timeout_hard(Task *task)
{
	return task->get_timeout_hard().count();
}

