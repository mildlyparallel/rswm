#include "include/pipeline.h"

#include "Task.hh"
#include "Pipeline.hh"

Pipeline *pipeline_create()
{
	return new Pipeline();
}

void pipeline_destroy(Pipeline *p)
{
	delete p;
}

void pipeline_run(Pipeline *pipeline, const TaskGraph *graph)
{
	pipeline->run(*graph);
}

void pipeline_write_profile(Pipeline *pipeline, const char *p)
{
	pipeline->write_profile(p);
}

void pipeline_set_max_accel_delta(Pipeline *pipeline, double v)
{
	pipeline->set_max_accel_delta(v);
}

void pipeline_set_ucb_coeff(Pipeline *pipeline, double v)
{
	pipeline->set_ucb_coeff(v);
}

void pipeline_set_fairness_coeff(Pipeline *pipeline, double v)
{
	pipeline->set_fairness_coeff(v);
}

void pipeline_set_efficiency_coeff(Pipeline *pipeline, double v)
{
	pipeline->set_efficiency_coeff(v);
}
