#include "include/taskgraph.h"

#include "Task.hh"
#include "TaskGraph.hh"

TaskGraph *taskgraph_create()
{
	return new TaskGraph();
}

void taskgraph_destroy(TaskGraph *graph)
{
	delete graph;
}

uint64_t taskgraph_add(TaskGraph *graph, const Task *task)
{
	return graph->add(*task);
}

uint64_t taskgraph_add_next(TaskGraph *graph, const Task *task)
{
	return graph->add_next(*task);
}

uint64_t taskgraph_add_after(TaskGraph *graph, uint64_t task_id, const Task *task)
{
	return graph->add_after(task_id, *task);
}
