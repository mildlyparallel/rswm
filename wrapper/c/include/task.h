class Task;

extern "C" {
	Task* task_create();

	void task_destroy(Task *task);

	void task_set_name(Task *task, const char *name);

	const char *task_get_name(Task *task);

	void task_set_file(Task *task, const char *file);

	const char *task_get_file(Task *task);

	void task_set_workdir(Task *task, const char *wd);

	const char *task_get_workdir(Task *task);

	void task_set_shell_cmd(Task *task, const char *cmd);

	void task_add_arg(Task *task, const char *arg);

	void task_set_stdout_log(Task *task, const char *file);

	const char *task_get_stdout_log(Task *task);

	void task_set_stderr_log(Task *task, const char *file);

	const char *task_get_stderr_log(Task *task);

	void task_set_timeout_soft(Task *task, unsigned sec);

	double task_get_timeout_soft(Task *task);

	void task_set_timeout_hard(Task *task, unsigned sec);

	double task_get_timeout_hard(Task *task);
}

