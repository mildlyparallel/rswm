#include <cstdint>

class Task;
class TaskGraph;

extern "C" {
	TaskGraph *taskgraph_create();

	void taskgraph_destroy(TaskGraph *graph);

	uint64_t taskgraph_add(TaskGraph *graph, const Task *task);

	uint64_t taskgraph_add_next(TaskGraph *graph, const Task *task);

	uint64_t taskgraph_add_after(TaskGraph *graph, uint64_t task_id, const Task *task);
}

