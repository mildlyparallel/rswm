#include <cstdint>

class TaskGraph;
class Pipeline;

extern "C" {
	Pipeline *pipeline_create();

	void pipeline_destroy(Pipeline *pipeline);

	void pipeline_run(Pipeline *pipeline, const TaskGraph *graph);

	void pipeline_write_profile(Pipeline *pipeline, const char *p);

	void pipeline_set_max_accel_delta(Pipeline *pipeline, double v);

	void pipeline_set_ucb_coeff(Pipeline *pipeline, double v);

	void pipeline_set_fairness_coeff(Pipeline *pipeline, double v);

	void pipeline_set_efficiency_coeff(Pipeline *pipeline, double v);
}

