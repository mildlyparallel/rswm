#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)

file <- "stats.csv"

if (length(args) >= 1)
	file <- args[1]

raw <- read.csv(file)

wids <- unique(raw$wid)
wids <- wids[order(wids)]

cids <- unique(raw$cid)
cids <- cids[order(cids)]

fields_cum <- c(
	'NetworkRead',
	'NetworkWrite',
	'Instructions',
	'CacheMisses',
	'CacheReferences',
	'BusAccess',
	'BusCycles',
	'CpuCycles'
	# 'StorageRead',
	# 'StorageWrite',
	# 'ReadChars',
	# 'WriteChars',
	# 'UserTime',
	# 'SystemTime'
)

fields_dif <- c('Threads', 'RunningThreads')

to_mbits <- function(x) 8 * x / 1e6
to_m <- function(x) x / 1e6
to_mbs <- function(x) x / (1024*1024)
to_x <- function(x) x

fields_cum_units <- c(
	to_mbits,
	to_mbits,
	to_m,
	to_m,
	to_m,
	to_m,
	to_m,
	to_m
	# to_mbs,
	# to_mbs,
	# to_mbs,
	# to_mbs,
	# to_x,
	# to_x
)

cvals <- NULL

for (c in seq_along(cids)) {
	cid <- cids[c]
	# cmdname <- cmdnames[c]

	cdata <- raw[raw$cid == cid,]
	wids <- unique(cdata$wid)

	cmdname <- cdata$name[1]
	wvals <- NULL

	cmd_max <- NULL
	cmd_mean <- NULL

	xstart <- 0
	xend <- 1e99

	for (i in seq_along(fields_cum)) {

		field <- fields_cum[i]

		print(field)

		wymax <- NULL
		wymean <- NULL

		for (w in seq_along(wids)) {
			wid <- wids[w]
			wdata <- cdata[cdata$wid == wid,]

			y <- wdata[, fields_cum[i]]
			y <- fields_cum_units[[i]](y)

			x <- (wdata$Timestamp[-1] - wdata$Timestamp[1]) / 1e3
			xstart <- max(xstart, min(x))
			xend <- min(xend, max(x))

			wymax <- c(wymax, max(y))

			y <- diff(y)
			y[which(y < 0)] = 0
			dt <- diff(wdata$Timestamp) / 1e3
			y <- y / dt
			wymean <- c(wymean, mean(y))
		}

		print(c(mean(wymean), mean(wymax)))

		cmd_max <- cbind(cmd_max, mean(wymax))
		cmd_mean <- cbind(cmd_mean, mean(wymean))
	} 

	thawed_units <- NULL
	for (w in seq_along(wids)) {
		wid <- wids[w]
		wdata <- cdata[cdata$wid == wid,]
		t <- sum(wdata$Frozen == 0)
		thawed_units <- c(thawed_units, t)
	}
	active_time <- mean(thawed_units) * 250 / 1000

	time <- xend - xstart

	print('Time')
	print(time)

	print('Active')
	print(active_time)

	cmd_data <- cbind(time, active_time, cmd_max, cmd_mean)
	colnames(cmd_data) <- c(
		'Time',
		'Active',
		paste(fields_cum, '_max', sep=''),
		paste(fields_cum, '_mean', sep='')
	)

	rownames(cmd_data) <- NULL

	filename <- paste('series-', cmdname, '-max-mean.csv', sep='')
	write.csv(cmd_data, filename, row.names=FALSE)
}

