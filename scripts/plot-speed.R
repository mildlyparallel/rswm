#!/usr/bin/env Rscript
args = commandArgs(trailingOnly=TRUE)
inpfile <- 'profile.csv'
if (length(args) >= 1)
	inpfile <- args[1]

raw <- read.csv(inpfile)

cids <- unique(raw$cid)
cids <- cids[order(cids)]

clk_tck = 100
col <- rainbow(length(cids))

png('img.png', width = 1000, height = 600)

names <- NULL

for (c in seq_along(cids)) {
	cid <- cids[c]

	cdata <- raw[raw$cid == cid & raw$wid == 1,]

	# ms to sec
	ts <- (cdata$Timestamp[-1] - raw$Timestamp[1]) / 1e3

	inst <- diff(cdata$Instructions)
	cycl <- diff(cdata$CpuCycles)
	ipc <- inst / cycl
	ipc[which(ipc < 0)] = 0

	# clk to sec
	utime <- diff(cdata$UserTime) / clk_tck
	stime <- diff(cdata$SystemTime) / clk_tck
	# ms to sec
	wtime <- diff(cdata$Timestamp) / 1e3
	ctime <- (utime + stime) / wtime
	ctime[which(ctime < 0)] = 0

	speed <- ipc * ctime

	if (c == 1) {
		plot(
			x    = ts,
			y    = speed,
			type = 's',
			col  = col[c],
			xlim = range((raw$Timestamp[-1] - raw$Timestamp[1])/1e3),
			xlab = 'Time, s',
			ylab = 'Task speed'
		)
	} else {
		lines(
			x    = ts,
			y    = speed,
			type = 's',
			col  = col[c]
		)
	}

	names <- c(names, cdata$name[1])
}

legend(
	'bottomright',
	legend = names,
	lwd    = 2,
	col    = col
)

dev.off()
