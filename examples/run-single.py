#!/usr/bin/env python3

import sys
import os

import rswm

if len(sys.argv) < 2:
    print('Usage:')
    print(sys.argv[0], '<executable> [args]')
    sys.exit(1)

file = sys.argv[1]
args = sys.argv[2:]
name = os.path.basename(file)

task = rswm.Task()
task.set_name(name)
task.set_file(file)
task.set_args(args)
task.set_stdout_log('/dev/stdout')
task.set_stderr_log('/dev/stderr')

graph = rswm.TaskGraph()
graph.add(task)

pipeline = rswm.Pipeline()
pipeline.write_profile(f'{name}-profile.csv')
pipeline.run(graph)

