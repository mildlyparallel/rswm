#include <iostream>
#include <filesystem>

#include "Task.hh"
#include "TaskGraph.hh"
#include "Pipeline.hh"

int main(int argc, char *argv[])
{
	if (argc < 2) {
		std::cerr << "Usage:" << std::endl;
		std::cerr << argv[0] << " <executable> [args]" << std::endl;
		std::exit(EXIT_FAILURE);
	}

	std::string file = argv[1];
	std::string name = std::filesystem::path(file).filename();

	Task task = Task();
	task.set_name(name);
	task.set_file(file);
	for (int i = 2; i < argc; ++i)
		task.add_arg(argv[i]);
	task.set_stdout_log("/dev/stdout");
	task.set_stderr_log("/dev/stderr");

	TaskGraph graph = TaskGraph();
	graph.add(task);

	Pipeline pipeline = Pipeline();
	pipeline.write_profile(name + "-profile.csv");
	pipeline.run(graph);

	return 0;
}
