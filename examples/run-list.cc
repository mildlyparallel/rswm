#include <iostream>
#include <filesystem>

#include "Task.hh"
#include "TaskGraph.hh"
#include "Pipeline.hh"

int main(int argc, char *argv[])
{
	if (argc < 2) {
		std::cerr << "Usage:" << std::endl;
		std::cerr << argv[0] << " <command_list>" << std::endl;
		std::exit(EXIT_FAILURE);
	}

	std::string cmdlist = argv[1];

	double fair = 0;
	double eff = 0;
	double ucb = 1;
	if (argc > 2)
		fair = std::atof(argv[2]);
	if (argc > 3)
		eff = std::atof(argv[3]);
	if (argc > 4)
		ucb = std::atof(argv[4]);

	TaskGraph graph = TaskGraph();

	std::ifstream in(cmdlist);
	std::string cmd;
	while (std::getline(in, cmd)) {
		std::istringstream ss(cmd);
		std::string file;
		ss >> file;
		std::string name = std::filesystem::path(file).filename();

		Task task = Task();
		task.set_name(name);
		task.set_shell_cmd(cmd);
		task.set_stdout_log("/dev/null");
		task.set_stderr_log("/dev/null");

		graph.add(task);
	}

	Pipeline pipeline = Pipeline();
	pipeline.set_max_accel_delta(0.15);
	pipeline.set_fairness_coeff(fair);
	pipeline.set_efficiency_coeff(eff);
	pipeline.set_ucb_coeff(ucb);
	pipeline.write_profile("list-profile.csv");
	pipeline.run(graph);

	return 0;
}
