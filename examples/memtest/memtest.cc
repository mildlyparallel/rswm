#include <iostream>
#include <vector>
#include <chrono>
#include <thread>
#include <random>
#include <cstring>
#include <atomic>
#include <future>
#include <cassert>

#include <signal.h>
#include <unistd.h>

typedef uint64_t elem_t;

const size_t stride = 1024;
const size_t streams = 4;
const size_t page_size = 4096;

std::atomic_bool killed(false);

void sig_handler(int signum){
	killed = true;
}

std::vector<elem_t> init_array(size_t len)
{
	std::vector<elem_t> data(len);
	std::random_device rd;
	std::mt19937 gen(rd());
	std::uniform_int_distribution<> distrib(1, data.size());

	for (size_t i = 0; i < data.size(); i++)
		data[i] = i;

	for (size_t i = 0; i < data.size(); i++) {
		size_t j = i + distrib(gen) % (data.size() - i);
		std::swap(data[i], data[j]);
	}

	return data;
}

template <typename T>
void do_not_opt_out(T &&x) {
	static auto ttid = std::this_thread::get_id();
	if (ttid == std::thread::id()) {
		const auto* p = &x;
		putchar(*reinterpret_cast<const char*>(p));
		std::abort();
	}
}

void run_pointer_chasing(size_t cnt)
{
	std::vector<elem_t> data = init_array(cnt);

	std::cout << "Streams: " << streams << "\n";
	std::cout << "Starting pointer chaising" << std::endl;

	uint64_t ops = 0;
	size_t id[streams];
	for (size_t i = 0; i < streams; ++i)
		id[i] = data[i];

	auto st = std::chrono::high_resolution_clock::now();

	while (!killed.load(std::memory_order_relaxed)) {
		for (size_t i = 0; i < streams; ++i)
			id[i] = data[id[i]];

		ops += streams;
	}

	auto en = std::chrono::high_resolution_clock::now();
	uint64_t dt = std::chrono::duration_cast<std::chrono::milliseconds>(en - st).count();

	for (size_t i = 0; i < streams; ++i)
		do_not_opt_out(id[i]);

	std::cout << "\nTime(ms): " << dt << "\n";
	std::cout << "Operations: " << ops << "\n";
	double rate = 1. * ops / (dt * 1e3);
	std::cout << "Rate: " << rate << std::endl;
}

std::pair<uint64_t, uint64_t> run_copy_loop(size_t cnt) {
	std::vector<elem_t> data = init_array(cnt);
	uint64_t p = reinterpret_cast<uint64_t>(&data[0]);
	size_t offset = page_size - (p & (page_size - 1));
	assert(offset <= page_size);
	offset /= sizeof(elem_t);

	size_t n = stride / sizeof(elem_t);
	elem_t tmp[streams * n];

	uint64_t ops = 0;

	auto st = std::chrono::high_resolution_clock::now();

	while (!killed.load(std::memory_order_relaxed)) {
		for (size_t i = offset; i < data.size(); i += n * streams) {
			for (size_t j = 0; j < streams; ++j) {
				memcpy(tmp + j * n, &data[i + j * n], stride);
			}
			ops += streams * stride;
		}
	}

	auto en = std::chrono::high_resolution_clock::now();
	uint64_t dt = std::chrono::duration_cast<std::chrono::milliseconds>(en - st).count();

	for (size_t i = 0; i < streams*n; ++i)
		do_not_opt_out(tmp[i]);

	return {ops, dt};
}

void run_seq_copy(size_t cnt, size_t nthreads) {
	size_t n = stride / sizeof(elem_t);

	std::cout << "Threads: " << nthreads << "\n";
	std::cout << "Stride: " << stride << "b = ";
	std::cout << n << "x" << sizeof(elem_t) << "b\n";
	std::cout << "Streams: " << streams << "\n";
	std::cout << "Starting sequential copy" << "\n";

	std::vector<std::future<std::pair<uint64_t, uint64_t>>> futures(nthreads);
	double rate_sum = 0;

	for (size_t i = 0; i < nthreads; ++i) {
		auto d = std::async(run_copy_loop, cnt);
		futures[i] = std::move(d);
	}

	for (size_t i = 0; i < nthreads; ++i) {
		auto [ops, dt] = futures[i].get();
		double dt_sec = dt / 1e3;
		double rate = 1. * ops / dt_sec;

		std::cout << "\nThread " << i << " rate " << (rate / (1<<30));

		rate_sum += rate;
	}

	std::cout << "\n";
	// std::cout << "Rate Sum [b/s]:  " << rate_sum << "\n";
	// std::cout << "Rate Sum [kb/s]: " << rate_sum / (1 << 10) << "\n";
	// std::cout << "Rate Sum [mb/s]: " << rate_sum / (1 << 20) << "\n";
	std::cout << "Rate Sum [gb/s]: " << rate_sum / (1 << 30) << "\n";

	double rate_avg = rate_sum / nthreads;
	// std::cout << "\n";
	// std::cout << "Rate Avg [b/s]:  " << rate_avg << "\n";
	// std::cout << "Rate Avg [kb/s]: " << rate_avg / (1 << 10) << "\n";
	// std::cout << "Rate Avg [mb/s]: " << rate_avg / (1 << 20) << "\n";
	std::cout << "Rate Avg [gb/s]: " << rate_avg / (1 << 30) << "\n";
}

int main(int argc, char *argv[])
{
	struct sigaction new_action;
	new_action.sa_handler = sig_handler;
	sigemptyset (&new_action.sa_mask);
	new_action.sa_flags = 0;
	sigaction(SIGINT, &new_action, NULL);
	sigaction(SIGHUP, &new_action, NULL);
	sigaction(SIGTERM, &new_action, NULL);
	sigaction(SIGALRM, &new_action, NULL);


	uint64_t sz = 1*1024;
	size_t nthreads = 1;
	size_t timeout_sec = 20;

	std::string mode = "copy";
	if (argc > 1)
		mode = argv[1];
	if (argc > 2)
		sz = std::atoll(argv[2]);
	if (argc > 3)
		nthreads = std::atoll(argv[3]);

	sz = std::max(sz, sizeof(elem_t) * streams);
	size_t cnt = page_size + sz / sizeof(elem_t);

	std::cout << "Size: ";
	std::cout << sz << "b = ";
	std::cout << sz/(1 << 10) << "kb = ";
	std::cout << sz/(1 << 20) << "mb = ";
	std::cout << sz/(1 << 30) << "gb = ";
	std::cout << cnt << "x" << sizeof(elem_t) << "b\n";
	std::cout << "Timeout: " << timeout_sec << " sec" << std::endl;

	alarm(timeout_sec);

	if (mode == "copy")
		run_seq_copy(cnt, nthreads);
	else
		run_pointer_chasing(cnt);

	return 0;
}

