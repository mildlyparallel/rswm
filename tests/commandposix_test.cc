#include <fstream>
#include <filesystem>
#include <thread>
#include <sys/stat.h>

#include "gtest/gtest.h"

#include "posix/CommandPosix.hh"

using namespace ::testing;

const std::string script_dft = "test-cmdp.sh";

void write_script(const std::string &s, const std::string &p = script_dft) {
	{
		std::ofstream out(p);
		out << s;
	}

	chmod(p.c_str(), S_IRWXU);
}

void run_and_sync(CommandPosix &cmd, const std::string &s) {
	write_script(s);

	auto wd = std::filesystem::current_path();

	cmd.set_file(std::filesystem::current_path() / script_dft);

	cmd.spawn();

	while (!cmd.sync())
		std::this_thread::yield();
}

TEST(CommandPosixTest, state__exit_0__exited_rc0) {
	CommandPosix cmd;

	run_and_sync(cmd, R"(#!/bin/bash
exit 0
)");

	EXPECT_EQ(cmd.get_state(), Command::Exited);
	EXPECT_EQ(cmd.get_return_code(), 0);
}

TEST(CommandPosixTest, state__exit_42__exited_rc42) {
	CommandPosix cmd;

	run_and_sync(cmd, R"(#!/bin/bash
exit 42
)");

	EXPECT_EQ(cmd.get_state(), Command::Exited);
	EXPECT_EQ(cmd.get_return_code(), 42);
}

TEST(CommandPosixTest, state__kill9__terminated) {
	CommandPosix cmd;

	run_and_sync(cmd, R"(#!/bin/bash
kill -9 $$
exit 0
)");

	EXPECT_EQ(cmd.get_state(), Command::Terminated);
}

TEST(CommandPosixTest, state__kill_term__terminated) {
	CommandPosix cmd;

	run_and_sync(cmd, R"(#!/bin/bash
kill -TERM $$
exit 0
)");

	EXPECT_EQ(cmd.get_state(), Command::Terminated);
}

TEST(CommandPosixTest, workdir__no_wd__executed_in_current) {
	{
		std::ofstream out("test.sh");
		out << "#!/bin/bash\n";
		out << "[ \"$(pwd)\" == \"" << std::filesystem::current_path() << "\" ] && exit 42\n";
	}

	chmod("test.sh", S_IRWXU);

	CommandPosix cmd;
	cmd.set_file(std::filesystem::current_path() / "test.sh");

	cmd.set_stdout_log("/dev/stdout");
	cmd.set_stderr_log("/dev/stderr");
	cmd.spawn();

	while (!cmd.sync())
		std::this_thread::yield();

	EXPECT_EQ(cmd.get_state(), Command::Exited);
	EXPECT_EQ(cmd.get_return_code(), 42);
}

TEST(CommandPosixTest, workdir__different_wd__wd_changed) {
	if (!std::filesystem::exists("test-dir"))
		std::filesystem::create_directories("./test-dir/");

	write_script(R"EOF(#!/bin/bash
[ "$(basename $(pwd))" == test-dir ] && exit 42
)EOF", "test.sh");

	CommandPosix cmd;
	cmd.set_file(std::filesystem::current_path() / "test.sh");
	cmd.set_workdir(std::filesystem::current_path() / "test-dir");

	cmd.spawn();

	while (!cmd.sync())
		std::this_thread::yield();

	EXPECT_EQ(cmd.get_state(), Command::Exited);
	EXPECT_EQ(cmd.get_return_code(), 42);
}

TEST(CommandPosixTest, workdir__relative_wd__wd_changed) {
	if (!std::filesystem::exists("test-dir"))
		std::filesystem::create_directories("./test-dir/");

	write_script(R"EOF(#!/bin/bash
[ "$(basename $(pwd))" == test-dir ] && exit 42
)EOF", "test.sh");

	CommandPosix cmd;
	cmd.set_file(std::filesystem::current_path() / "test.sh");
	cmd.set_workdir("./test-dir");

	cmd.spawn();

	while (!cmd.sync())
		std::this_thread::yield();

	EXPECT_EQ(cmd.get_state(), Command::Exited);
	EXPECT_EQ(cmd.get_return_code(), 42);
}

TEST(CommandPosixTest, workdir__not_exists__failed) {
	ASSERT_FALSE(std::filesystem::exists("test-dir-notexists"));

	write_script(R"EOF(#!/bin/bash
exit 0
)EOF", "test.sh");

	CommandPosix cmd;
	cmd.set_file("./test.sh");
	cmd.set_workdir(std::filesystem::current_path() / "test-dir-notexists");

	cmd.spawn();

	EXPECT_EQ(cmd.get_state(), Command::Failed);
}

TEST(CommandPosixTest, workdir__not_dir__failed) {
	{
		std::ofstream out("test-not-a-dir");
		out << "atata\n";
	}

	ASSERT_FALSE(std::filesystem::is_directory("test-not-a-dir"));

	write_script(R"EOF(#!/bin/bash
exit 0
)EOF", "test.sh");

	CommandPosix cmd;
	cmd.set_file("./test.sh");
	cmd.set_workdir(std::filesystem::current_path() / "test-not-a-dir");

	cmd.spawn();

	EXPECT_EQ(cmd.get_state(), Command::Failed);
}

TEST(CommandPosixTest, file__rel_path__executed) {
	if (!std::filesystem::exists("test-dir"))
		std::filesystem::create_directories("./test-dir/");

	write_script(R"(#!/bin/bash
exit 42
)", "./test-dir/test.sh");

	CommandPosix cmd;
	cmd.set_file("./test-dir/test.sh");

	cmd.spawn();

	while (!cmd.sync())
		std::this_thread::yield();

	EXPECT_EQ(cmd.get_state(), Command::Exited);
	EXPECT_EQ(cmd.get_return_code(), 42);
}

TEST(CommandPosixTest, file__rel_to_workdir__executed) {
	if (!std::filesystem::exists("test-dir-workdir"))
		std::filesystem::create_directories("./test-dir-workdir/");

	write_script(R"(#!/bin/bash
exit 43
)", "./test-dir-workdir/test.sh");

	CommandPosix cmd;
	cmd.set_workdir("./test-dir-workdir");
	cmd.set_file("./test.sh");

	cmd.spawn();

	while (!cmd.sync())
		std::this_thread::yield();

	EXPECT_EQ(cmd.get_state(), Command::Exited);
	EXPECT_EQ(cmd.get_return_code(), 43);
}

TEST(CommandPosixTest, file__rel_path_no_dir__worked) {
	write_script(R"(#!/bin/bash
exit 42
)", "./test.sh");

	CommandPosix cmd;
	cmd.set_file("./test.sh");

	cmd.spawn();

	while (!cmd.sync())
		std::this_thread::yield();

	EXPECT_EQ(cmd.get_state(), Command::Exited);
	EXPECT_EQ(cmd.get_return_code(), 42);
}


TEST(CommandPosixTest, file__not_exists__failed) {
	ASSERT_FALSE(std::filesystem::exists("not-exists.sh"));

	CommandPosix cmd;
	cmd.set_file("./not-exists.sh");

	cmd.spawn();

	EXPECT_EQ(cmd.get_state(), Command::Failed);
}

TEST(CommandPosixTest, file__not_executable__failed) {
	{
		std::ofstream out("test-no-x.sh");
		out << R"(#!/bin/bash
exit 42
)";
	}

	CommandPosix cmd;
	cmd.set_file("./test-no-x.sh");

	cmd.spawn();

	EXPECT_EQ(cmd.get_state(), Command::Failed);
}

TEST(CommandPosixTest, args__add_multiple) {
	write_script(R"(#!/bin/bash
[ $# != 3 ] && exit 1
[ $1 == aa ] && [ $2 == bb ] && [ $3 == cc ] && exit 42
)", "test.sh");

	CommandPosix cmd;
	cmd.set_file("./test.sh");
	cmd.add_args("aa", "bb", "cc");

	cmd.spawn();

	while (!cmd.sync())
		std::this_thread::yield();

	EXPECT_EQ(cmd.get_state(), Command::Exited);
	EXPECT_EQ(cmd.get_return_code(), 42);
}

TEST(CommandPosixTest, envs__add_multiple) {
	write_script(R"(#!/bin/bash
[ z$e1 == zv1 ] && [ z$e2 == zv2 ] && exit 42
)", "test.sh");

	CommandPosix cmd;
	cmd.set_file("./test.sh");
	cmd.add_env("e1", "v1");
	cmd.add_env("e2", "v2");

	cmd.spawn();

	while (!cmd.sync())
		std::this_thread::yield();

	EXPECT_EQ(cmd.get_state(), Command::Exited);
	EXPECT_EQ(cmd.get_return_code(), 42);
}

TEST(CommandPosixTest, envs__inherit__var_inhereted) {
	write_script(R"(#!/bin/bash
[ z$RSWM_TEST_ENV == zabc ] && exit 42 
)", "test.sh");

	CommandPosix cmd;
	cmd.set_file("./test.sh");
	cmd.set_inherit_env(true);

	char s[] = "RSWM_TEST_ENV=abc";
	putenv(s);

	cmd.spawn();

	while (!cmd.sync())
		std::this_thread::yield();

	EXPECT_EQ(cmd.get_state(), Command::Exited);
	EXPECT_EQ(cmd.get_return_code(), 42);
}

TEST(CommandPosixTest, envs__no_inherit__var_no_inhereted) {
	write_script(R"(#!/bin/bash
[ -z $RSWM_TEST_ENV ] && exit 42 
)", "test.sh");

	CommandPosix cmd;
	cmd.set_file("./test.sh");
	cmd.set_inherit_env(false);

	char s[] = "RSWM_TEST_ENV=abc";
	putenv(s);

	cmd.spawn();

	while (!cmd.sync())
		std::this_thread::yield();

	EXPECT_EQ(cmd.get_state(), Command::Exited);
	EXPECT_EQ(cmd.get_return_code(), 42);
}


TEST(CommandPosixTest, shellcmd__spawned) {
	CommandPosix cmd;
	cmd.set_shell_cmd("[ $k == v ] && exit 42");

	cmd.add_env("k", "v");
	cmd.spawn();

	while (!cmd.sync())
		std::this_thread::yield();

	EXPECT_EQ(cmd.get_state(), Command::Exited);
	EXPECT_EQ(cmd.get_return_code(), 42);
}

TEST(CommandPosixTest, output__stdout__written_to_file) {
	CommandPosix cmd;
	cmd.set_shell_cmd("echo 123");
	cmd.set_stdout_log("test-stdout");
	cmd.spawn();

	while (!cmd.sync())
		std::this_thread::yield();

	EXPECT_EQ(cmd.get_state(), Command::Exited);

	std::ifstream in("test-stdout");
	std::string s;
	in >> s;

	EXPECT_EQ(s, "123");
}

TEST(CommandPosixTest, output__stderr__written_to_file) {
	CommandPosix cmd;
	cmd.set_shell_cmd("echo 456 > /dev/stderr");
	cmd.set_stderr_log("test-stderr");
	cmd.spawn();

	while (!cmd.sync())
		std::this_thread::yield();

	EXPECT_EQ(cmd.get_state(), Command::Exited);

	std::ifstream in("test-stderr");
	std::string s;
	in >> s;

	EXPECT_EQ(s, "456");
}

TEST(CommandPosixTest, output__both__written_to_files) {
	CommandPosix cmd;
	cmd.set_shell_cmd("echo abc > /dev/stderr; echo def");
	cmd.set_stdout_log("test-stdout");
	cmd.set_stderr_log("test-stderr");
	cmd.spawn();

	while (!cmd.sync())
		std::this_thread::yield();

	EXPECT_EQ(cmd.get_state(), Command::Exited);

	std::ifstream in_e("test-stderr");
	std::string s_e;
	in_e >> s_e;
	EXPECT_EQ(s_e, "abc");

	std::ifstream in_o("test-stdout");
	std::string s_o;
	in_o >> s_o;
	EXPECT_EQ(s_o, "def");
}

TEST(CommandPosixTest, output__stdout_dir_not_exists__failed) {
	CommandPosix cmd;
	cmd.set_shell_cmd("exit 1");
	cmd.set_stdout_log("not/eixts/aa");
	cmd.spawn();

	EXPECT_EQ(cmd.get_state(), Command::Failed);
}

TEST(CommandPosixTest, output__stderr_dir_not_exists__failed) {
	CommandPosix cmd;
	cmd.set_shell_cmd("exit 1");
	cmd.set_stderr_log("not/eixts/aa");
	cmd.spawn();

	EXPECT_EQ(cmd.get_state(), Command::Failed);
}

#if ENABLE_CGROUPS

TEST(CommandPosixTest, freezer__freezed_thawed__affects_time) {
	write_script(R"EOF(#!/bin/bash
for i in {1..4}
do
	date +%s
	sleep 1
done
)EOF", "test.sh");

	CommandPosix cmd;
	cmd.set_file("./test.sh");
	cmd.set_stdout_log("test-output");
	cmd.spawn();

	std::this_thread::sleep_for(std::chrono::milliseconds(100));

	cmd.freeze(true);

	std::this_thread::sleep_for(std::chrono::seconds(2));

	cmd.freeze(false);

	while (!cmd.sync())
		std::this_thread::sleep_for(std::chrono::milliseconds(100));

	std::ifstream in("test-output");
	uint64_t ts = 0;
	uint64_t ts_prev = 0;

	for (size_t i = 0; i < 4; ++i) {
		ASSERT_TRUE((in >> ts));

		uint64_t dt = ts - ts_prev;
		ts_prev = ts;
		if (i == 1) {
			EXPECT_TRUE(dt >= 2);
			if (dt != 2)
				std::cerr << "dt = " << dt << std::endl;
		}

		if (i > 1) {
			EXPECT_TRUE(dt >= 1);
			if (dt != 1)
				std::cerr << "dt = " << dt << std::endl;
		}
	}
}

#endif

TEST(CommandPosixTest, cputlist__list_set_to_0__affinity_changed) {

	CommandPosix cmd;
	cmd.set_cpulist({0});

	run_and_sync(cmd, R"EOF(#!/bin/bash
l=$(taskset -cp $$ | cut -d: -f2-)
[ $l == "0" ] && exit 42
)EOF");

	EXPECT_EQ(cmd.get_state(), Command::Exited);
	EXPECT_EQ(cmd.get_return_code(), 42);
}

TEST(CommandPosixTest, cputlist__list_set_to_01__affinity_changed)
{
	size_t n = std::thread::hardware_concurrency();
	if (n <= 1)
		return;

	CommandPosix cmd;
	cmd.set_cpulist({0, 1});

	run_and_sync(cmd, R"EOF(#!/bin/bash
l=$(taskset -cp $$ | cut -d: -f2-)
[ $l == "0,1" ] && exit 43
)EOF");

	EXPECT_EQ(cmd.get_state(), Command::Exited);
	EXPECT_EQ(cmd.get_return_code(), 43);
}
