#include "gtest/gtest.h"

#include "CommandStub.hh"

using namespace ::testing;

TEST(Command, serialization__file) {
	CommandStub c1;
	c1.set_file("file1");

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_file(), c1.get_file());
}

TEST(Command, serialization__workdir) {
	CommandStub c1;
	c1.set_workdir("wd1");

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_workdir(), c1.get_workdir());
}

TEST(Command, serialization__stdin) {
	CommandStub c1;
	c1.set_stdin("stdin1");

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_stdin(), c1.get_stdin());
}

TEST(Command, serialization__stdout) {
	CommandStub c1;
	c1.set_stdout("stdout1");

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_stdout(), c1.get_stdout());
}

TEST(Command, serialization__stderr) {
	CommandStub c1;
	c1.set_stderr("stderr1");

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_stderr(), c1.get_stderr());
}

TEST(Command, serialization__inherit_env) {
	CommandStub c1;
	c1.set_inherit_env(!c1.get_inherit_env());

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_inherit_env(), c1.get_inherit_env());
}

TEST(Command, serialization__return_code) {
	CommandStub c1;
	c1.set_return_code(123);

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_return_code(), c1.get_return_code());
}

TEST(Command, serialization__state) {
	CommandStub c1;
	c1.set_state(Command::Exited);

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_state(), c1.get_state());
}

TEST(Command, serialization__args) {
	CommandStub c1;
	c1.set_args({"a", "b", "c"});

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_args(), c1.get_args());
}

TEST(Command, serialization__envs) {
	CommandStub c1;
	c1.set_envs({"a", "b", "c"});

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_envs(), c1.get_envs());
}

TEST(Command, serialization__timeout_soft_sec) {
	CommandStub c1;
	c1.set_timeout_soft_sec(123);

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_timeout_soft_sec(), c1.get_timeout_soft_sec());
}

TEST(Command, serialization__timeout_hard_sec) {
	CommandStub c1;
	c1.set_timeout_hard_sec(124);

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_timeout_hard_sec(), c1.get_timeout_hard_sec());
}

TEST(Command, serialization__cpulist) {
	CommandStub c1;
	c1.set_cpulist({1, 3, 6, 10});

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_cpulist(), c1.get_cpulist());
}

TEST(Command, serialization__nr_nodes) {
	CommandStub c1;
	c1.set_nr_nodes(31);

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_nr_nodes(), c1.get_nr_nodes());
}

TEST(Command, serialization__nr_sockets) {
	CommandStub c1;
	c1.set_nr_sockets(32);

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_nr_sockets(), c1.get_nr_sockets());
}

TEST(Command, serialization__nr_cores) {
	CommandStub c1;
	c1.set_nr_cores(33);

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_nr_cores(), c1.get_nr_cores());
}

TEST(Command, serialization__nr_threads) {
	CommandStub c1;
	c1.set_nr_threads(34);

	CommandStub c2;
	c2.deserialize(c1.serialize());

	EXPECT_EQ(c2.get_nr_threads(), c1.get_nr_threads());
}
