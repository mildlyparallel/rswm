#include <thread>
#include <chrono>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "CommandPool.hh"
#include "mocks/CommandMock.hh"

using namespace ::testing;

TEST(CommandPoolTest, ctor__empty) {
	CommandPool commands;
	EXPECT_EQ(commands.size(), 0ul);
}

TEST(CommandPoolTest, add_with_next_id__added_2__id_incrementeed) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>();
	auto c2 = std::make_shared<CommandMock>();

	commands.add_with_next_id(c1);
	commands.add_with_next_id(c2);

	EXPECT_EQ(commands.size(), 2ul);
	EXPECT_EQ(c1->get_id(), 1ul);
	EXPECT_EQ(c2->get_id(), 2ul);
}

TEST(CommandPoolTest, add_pending__added_2__size_2) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	EXPECT_EQ(commands.size(), 2ul);
}


TEST(CommandPoolTest, add_pending__added_2__state_is_pending) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	EXPECT_EQ(c1->get_state(), Command::Pending);
	EXPECT_EQ(c2->get_state(), Command::Pending);
}

TEST(CommandPoolTest, find__existing__found) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	auto c = commands.find(2);

	EXPECT_EQ(c, c2);
}

TEST(CommandPoolTest, find__not_existing__not_found) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	auto c = commands.find(3);

	EXPECT_FALSE(c);
}

TEST(CommandPoolTest, spawn_pending_commands__no_pending__nothing) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	c1->set_state(Command::Spawned);
	c2->set_state(Command::Spawned);

	std::vector<std::shared_ptr<Command>> spawned;
	commands.spawn_pending_commands(spawned);

	EXPECT_EQ(spawned.size(), 0ul);
}

TEST(CommandPoolTest, spawn_pending_commands__one_pending__spawned) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);

	commands.add_pending(c1);

	EXPECT_CALL(*c1, spawn());

	std::vector<std::shared_ptr<Command>> spawned;
	commands.spawn_pending_commands(spawned);

	EXPECT_EQ(c1->get_state(), Command::Spawned);

	EXPECT_EQ(spawned.size(), 1ul);
	EXPECT_EQ(spawned[0], c1);
}

TEST(CommandPoolTest, spawn_pending_commands__two_pending__two_spawned) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	EXPECT_CALL(*c1, spawn());
	EXPECT_CALL(*c2, spawn());

	std::vector<std::shared_ptr<Command>> spawned;
	commands.spawn_pending_commands(spawned);

	EXPECT_EQ(c1->get_state(), Command::Spawned);
	EXPECT_EQ(c2->get_state(), Command::Spawned);

	EXPECT_EQ(spawned.size(), 2ul);
	EXPECT_TRUE(std::find(spawned.begin(), spawned.end(), c1) != spawned.end());
	EXPECT_TRUE(std::find(spawned.begin(), spawned.end(), c2) != spawned.end());
}

TEST(CommandPoolTest, spawn_pending_commands__many_pending__limit_reached) {
	CommandPool commands;

	constexpr size_t overhead = 10;
	constexpr size_t nr_commands = CommandPool::nr_spawned_limit + overhead;

	std::shared_ptr<CommandMock> cmd[nr_commands];
	for (size_t i = 0; i < nr_commands; ++i) {
		cmd[i] = std::make_shared<CommandMock>(i + 1);
		commands.add_pending(cmd[i]);
	}

	for (size_t i = 0; i < CommandPool::nr_spawned_limit; ++i) {
		EXPECT_CALL(*(cmd[i]), spawn());
	}

	std::vector<std::shared_ptr<Command>> spawned;
	commands.spawn_pending_commands(spawned);

	EXPECT_EQ(spawned.size(), CommandPool::nr_spawned_limit);

	for (size_t i = 0; i < nr_commands; ++i) {
		if (i < CommandPool::nr_spawned_limit) {
			EXPECT_EQ(cmd[i]->get_state(), Command::Spawned);
			EXPECT_TRUE(std::find(spawned.begin(), spawned.end(), cmd[i]) != spawned.end());
		} else {
			EXPECT_EQ(cmd[i]->get_state(), Command::Pending);
		}
	}
}

TEST(CommandPoolTest, sync_finished_commands__no_spawned__empty_list) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	std::vector<std::shared_ptr<Command>> finished;
	commands.sync_finished_commands(finished);

	EXPECT_TRUE(finished.empty());
}

TEST(CommandPoolTest, sync_finished_commands__spawned_not_finished__empty_list) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	c1->set_state(Command::Spawned);
	c2->set_state(Command::Spawned);

	EXPECT_CALL(*c1, sync()).WillOnce(Return(false));
	EXPECT_CALL(*c2, sync()).WillOnce(Return(false));

	std::vector<std::shared_ptr<Command>> finished;
	commands.sync_finished_commands(finished);

	EXPECT_TRUE(finished.empty());
}

TEST(CommandPoolTest, sync_finished_commands__spawned_one_finished__returned) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	c1->set_state(Command::Spawned);
	c2->set_state(Command::Spawned);

	EXPECT_CALL(*c1, sync()).WillOnce(Return(false));
	EXPECT_CALL(*c2, sync()).WillOnce(Return(true));

	std::vector<std::shared_ptr<Command>> finished;
	commands.sync_finished_commands(finished);

	EXPECT_EQ(finished.size(), 1ul);
	EXPECT_EQ(finished[0], c2);
}

TEST(CommandPoolTest, sync_finished_commands__spawned_two_finished__returned) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	c1->set_state(Command::Spawned);
	c2->set_state(Command::Spawned);

	EXPECT_CALL(*c1, sync()).WillOnce(Return(true));
	EXPECT_CALL(*c2, sync()).WillOnce(Return(true));

	std::vector<std::shared_ptr<Command>> finished;
	commands.sync_finished_commands(finished);

	EXPECT_EQ(finished.size(), 2ul);
	EXPECT_TRUE(std::find(finished.begin(), finished.end(), c1) != finished.end());
	EXPECT_TRUE(std::find(finished.begin(), finished.end(), c2) != finished.end());
}

TEST(CommandPoolTest, read_command_stats__no_spawned__nothing) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	std::vector<std::shared_ptr<Command>> updated;
	commands.read_command_stats(updated);
	EXPECT_EQ(updated.size(), 0ul);
}

TEST(CommandPoolTest, read_command_stats__two_spawned__read) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	c1->set_state(Command::Spawned);
	c2->set_state(Command::Spawned);

	EXPECT_CALL(*c1, read_stats());
	EXPECT_CALL(*c2, read_stats());

	std::vector<std::shared_ptr<Command>> updated;
	commands.read_command_stats(updated);
	EXPECT_EQ(updated.size(), 2ul);
	EXPECT_TRUE(std::find(updated.begin(), updated.end(), c1) != updated.end());
	EXPECT_TRUE(std::find(updated.begin(), updated.end(), c2) != updated.end());
}

TEST(CommandPoolTest, stop_all__no_spawned__nothing) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	commands.stop_all(true);
}

TEST(CommandPoolTest, stop_all__two_spawned__called) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	c1->set_state(Command::Spawned);
	c2->set_state(Command::Spawned);

	EXPECT_CALL(*c1, stop(false));
	EXPECT_CALL(*c2, stop(false));

	commands.stop_all(false);
}

TEST(CommandPoolTest, stop_all__two_spawned_forced__called) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	c1->set_state(Command::Spawned);
	c2->set_state(Command::Spawned);

	EXPECT_CALL(*c1, stop(true));
	EXPECT_CALL(*c2, stop(true));

	commands.stop_all(true);
}

TEST(CommandPoolTest, stop_tardy_commands__no_spawned__nothing) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	commands.stop_tardy_commands();
}

TEST(CommandPoolTest, stop_tardy_commands__before_timeout__nothing) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	c1->set_state(Command::Spawned);
	c2->set_state(Command::Spawned);

	c1->set_timeout_soft(std::chrono::seconds(1));
	c1->set_timeout_hard(std::chrono::seconds(2));
	c2->set_timeout_soft(std::chrono::seconds(1));
	c2->set_timeout_hard(std::chrono::seconds(2));

	commands.stop_tardy_commands();
}

TEST(CommandPoolTest, stop_tardy_commands__both_timeouts__stopped_hard) {
	CommandPool commands;

	auto c1 = std::make_shared<CommandMock>(1);
	auto c2 = std::make_shared<CommandMock>(2);

	commands.add_pending(c1);
	commands.add_pending(c2);

	c1->set_state(Command::Spawned);
	c2->set_state(Command::Spawned);

	c1->set_timeout_soft(std::chrono::seconds(1));
	c1->set_timeout_hard(std::chrono::seconds(2));
	c2->set_timeout_soft(std::chrono::seconds(1));
	c2->set_timeout_hard(std::chrono::seconds(2));

	EXPECT_CALL(*c1, stop(false));
	EXPECT_CALL(*c2, stop(false));

	std::this_thread::sleep_for(std::chrono::milliseconds(1010));

	commands.stop_tardy_commands();

	EXPECT_CALL(*c1, stop(true));
	EXPECT_CALL(*c2, stop(true));

	std::this_thread::sleep_for(std::chrono::milliseconds(1010));

	commands.stop_tardy_commands();
}
