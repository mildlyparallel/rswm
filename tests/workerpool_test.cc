#include <thread>
#include <chrono>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "WorkerPool.hh"
#include "mocks/WorkerMock.hh"

using namespace ::testing;

TEST(WorkerPoolTest, ctor__empty) {
	WorkerPool workers;
	EXPECT_EQ(workers.size(), 0ul);
}

TEST(WorkerPoolTest, add__without_name__id_is_set) {
	WorkerPool workers;

	auto w1 = std::make_shared<WorkerMock>();
	auto w2 = std::make_shared<WorkerMock>();

	workers.add(w1);
	workers.add(w2);

	EXPECT_EQ(workers.size(), 2ul);
	EXPECT_EQ(w1->get_id(), 1);
	EXPECT_EQ(w2->get_id(), 2);
}

TEST(WorkerPoolTest, add__with_different_names__id_is_set) {
	WorkerPool workers;

	auto w1 = std::make_shared<WorkerMock>();
	w1->set_name("w1");
	auto w2 = std::make_shared<WorkerMock>();
	w2->set_name("w2");

	workers.add(w1);
	workers.add(w2);

	EXPECT_EQ(workers.size(), 2ul);
	EXPECT_EQ(w1->get_id(), 1);
	EXPECT_EQ(w2->get_id(), 2);
}

TEST(WorkerPoolTest, add__with_same_names__first_stopped) {
	WorkerPool workers;

	auto w1 = std::make_shared<WorkerMock>();
	w1->set_name("w1");
	auto w2 = std::make_shared<WorkerMock>();
	w2->set_name("w1");

	EXPECT_CALL(*w1, stop());

	workers.add(w1);
	workers.add(w2);

	EXPECT_EQ(workers.size(), 1ul);
	EXPECT_EQ(w1->get_id(), 1);
	EXPECT_EQ(w2->get_id(), 2);
}

TEST(WorkerPoolTest, remove__existing__removed) {
	WorkerPool workers;

	auto w1 = std::make_shared<WorkerMock>();
	auto w2 = std::make_shared<WorkerMock>();

	workers.add(w1);
	workers.add(w2);

	workers.remove(w1->get_id());
	workers.remove(w2->get_id());

	EXPECT_EQ(workers.size(), 0ul);
}

TEST(WorkerPoolTest, remove__not_existing__not_removed) {
	WorkerPool workers;

	auto w1 = std::make_shared<WorkerMock>();
	auto w2 = std::make_shared<WorkerMock>();

	workers.add(w1);
	workers.add(w2);

	workers.remove(10);

	EXPECT_EQ(workers.size(), 2ul);
}

TEST(WorkerPoolTest, find__existing__found) {
	WorkerPool workers;

	auto w1 = std::make_shared<WorkerMock>();
	auto w2 = std::make_shared<WorkerMock>();

	workers.add(w1);
	workers.add(w2);

	auto f1 = workers.find(w1->get_id());
	auto f2 = workers.find(w2->get_id());

	EXPECT_EQ(f1, w1);
	EXPECT_EQ(f2, w2);
}

TEST(WorkerPoolTest, find__not_existing__not_found) {
	WorkerPool workers;

	auto w1 = std::make_shared<WorkerMock>();
	auto w2 = std::make_shared<WorkerMock>();

	workers.add(w1);
	workers.add(w2);

	auto f1 = workers.find(10);

	EXPECT_FALSE(f1);
}
