#include <thread>
#include <chrono>
#include <fstream>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "ResourceStats.hh"
#include "posix/CGroupFreezer.hh"

using namespace ::testing;

class FreezerTest : public Test
{
public:
	CGroupFreezer freezer;

	inline static const std::filesystem::path cgdir = "freezer-test";
	inline static const std::filesystem::path statepath = cgdir / "freezer.state";

	void write_state(const char *s) {
		std::ofstream out(statepath);
		out << s;
	}

	virtual void SetUp() override {
		freezer.set_dir(cgdir);
		std::filesystem::create_directories(cgdir);
	}

	virtual void TearDown() override {
		std::filesystem::remove(statepath);
		std::filesystem::remove(cgdir);
	}

protected:

private:

};

TEST_F(FreezerTest, read_state__frozen__returns_frozen) {
	write_state("FROZEN\n");
	EXPECT_EQ(freezer.read_state(), CGroupFreezer::Frozen);
}

TEST_F(FreezerTest, read_state__freezing__returns_freezing) {
	write_state("FREEZING\n");
	EXPECT_EQ(freezer.read_state(), CGroupFreezer::Freezing);
}

TEST_F(FreezerTest, read_state__thawed__returns_thawed) {
	write_state("THAWED\n");
	EXPECT_EQ(freezer.read_state(), CGroupFreezer::Thawed);
}

TEST_F(FreezerTest, is_frozen__frozen__returns_frozen) {
	write_state("FROZEN\n");
	EXPECT_TRUE(freezer.is_frozen());
}

TEST_F(FreezerTest, is_frozen__freezing__returns_freezing) {
	write_state("FREEZING\n");
	EXPECT_FALSE(freezer.is_frozen());
}

TEST_F(FreezerTest, is_frozen__thawed__returns_thawed) {
	write_state("THAWED\n");
	EXPECT_FALSE(freezer.is_frozen());
}

TEST_F(FreezerTest, update__frozen__returns_frozen) {
	write_state("FROZEN\n");
	ResourceStats stats;
	freezer.update(stats);
	EXPECT_EQ(stats.get(ResourceStats::Frozen), 1ul);
}

TEST_F(FreezerTest, update__freezing__returns_freezing) {
	write_state("FREEZING\n");
	ResourceStats stats;
	freezer.update(stats);
	EXPECT_EQ(stats.get(ResourceStats::Frozen), 0ul);
}

TEST_F(FreezerTest, update__thawed__returns_thawed) {
	write_state("THAWED\n");
	ResourceStats stats;
	freezer.update(stats);
	EXPECT_EQ(stats.get(ResourceStats::Frozen), 0ul);
}
