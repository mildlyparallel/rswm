#include <thread>
#include <chrono>

#include "gtest/gtest.h"

#include "mocks/WorkerMock.hh"
#include "Assignment.hh"

TEST(AssignmentTest, default_ctor__finished) {
	Assignment a;
	EXPECT_TRUE(a.is_finished());
}

TEST(AssignmentTest, default_ctor__size_0) {
	Assignment a;
	EXPECT_EQ(a.size(), 0);
}

TEST(AssignmentTest, create_commands__one_worker__finished) {
	Task t;

	std::vector< std::shared_ptr<Worker> > w = {
		std::make_shared<WorkerMock>()
	};

	Assignment a;
	a.create_commands(t, w);
	EXPECT_FALSE(a.is_finished());
}

TEST(AssignmentTest, create_commands_one_worker_size_1) {
	Task t;

	std::vector< std::shared_ptr<Worker> > w = {
		std::make_shared<WorkerMock>()
	};

	Assignment a;
	a.create_commands(t, w);
	EXPECT_EQ(a.size(), 1);
}

TEST(AssignmentTest, set_id__id_is_set) {
	Assignment a;
	a.set_id(123);
	EXPECT_EQ(a.get_id(), 123u);
}

TEST(AssignmentTest, commands__two_workers__two_commands) {
	Task t;

	std::vector< std::shared_ptr<Worker> > workers = {
		std::make_shared<WorkerMock>(),
		std::make_shared<WorkerMock>()
	};

	Assignment a;
	a.create_commands(t, workers);
	EXPECT_EQ(a.commands().size(), 2ul);
}

TEST(AssignmentTest, commands__two_workers__differnt_commands) {
	Task t;

	std::vector< std::shared_ptr<Worker> > workers = {
		std::make_shared<WorkerMock>(),
		std::make_shared<WorkerMock>()
	};

	Assignment a;
	a.create_commands(t, workers);

	auto [w0, c0] = a.commands()[0];
	auto [w1, c1] = a.commands()[1];

	EXPECT_EQ(w0, workers[0]);
	EXPECT_EQ(w1, workers[1]);
	EXPECT_NE(c0, c1);
}

TEST(AssignmentTest, commands__two_workers__hostnames_are_different) {
	Task t;

	std::vector< std::shared_ptr<Worker> > workers = {
		std::make_shared<WorkerMock>(),
		std::make_shared<WorkerMock>()
	};

	Assignment a;
	a.create_commands(t, workers);

	auto h0 = a.commands()[0].second->get_hostname();
	auto h1 = a.commands()[1].second->get_hostname();

	EXPECT_FALSE(h0.empty());
	EXPECT_FALSE(h1.empty());
	EXPECT_NE(h0, h1);
}

TEST(AssignmentTest, find__empty__returns_nullptr) {
	Assignment a;

	auto [w, c] = a.find();

	EXPECT_FALSE(w);
	EXPECT_FALSE(c);
}

TEST(AssignmentTest, find__no_host__returns_first) {
	Task t;

	std::vector< std::shared_ptr<Worker> > workers = {
		std::make_shared<WorkerMock>(),
		std::make_shared<WorkerMock>(),
	};

	Assignment a;
	a.create_commands(t, workers);

	auto [w, c] = a.find();

	EXPECT_TRUE(w);
	EXPECT_TRUE(c);
	EXPECT_EQ(w, workers[0]);
}


TEST(AssignmentTest, find__unknwn_host__returns_nullptr) {
	Task t;

	std::vector< std::shared_ptr<Worker> > workers = {
		std::make_shared<WorkerMock>(),
		std::make_shared<WorkerMock>(),
	};

	Assignment a;
	a.create_commands(t, workers);

	auto [w, c] = a.find("abc");

	EXPECT_FALSE(w);
	EXPECT_FALSE(c);
}

TEST(AssignmentTest, find__known_host__returns_pair) {
	Task t;

	std::vector< std::shared_ptr<Worker> > workers = {
		std::make_shared<WorkerMock>(),
		std::make_shared<WorkerMock>(),
	};

	Assignment a;
	a.create_commands(t, workers);

	ASSERT_EQ(a.size(), 2);
	auto [wrk, cmd] = a.commands().back();

	auto [w, c] = a.find(cmd->get_hostname());

	EXPECT_EQ(w, wrk);
	EXPECT_EQ(c, cmd);
}

TEST(AssignmentTest, is_finished__one_finished__not_finished) {
	Task t;

	std::vector< std::shared_ptr<Worker> > workers = {
		std::make_shared<WorkerMock>(),
		std::make_shared<WorkerMock>()
	};

	Assignment a;
	a.create_commands(t, workers);

	ASSERT_EQ(a.size(), 2);
	auto c1 = a.commands()[1].second;
	c1->set_state(Command::Exited);

	EXPECT_FALSE(a.is_finished());
}


TEST(AssignmentTest, is_finished__two_finished__finished) {
	Task t;

	std::vector< std::shared_ptr<Worker> > workers = {
		std::make_shared<WorkerMock>(),
		std::make_shared<WorkerMock>()
	};

	Assignment a;
	a.create_commands(t, workers);

	ASSERT_EQ(a.size(), 2);
	auto [w0, c0] = a.commands()[0];
	auto [w1, c1] = a.commands()[1];

	c0->set_state(Command::Exited);
	c1->set_state(Command::Terminated);

	EXPECT_TRUE(a.is_finished());
}
