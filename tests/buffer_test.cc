#include "gtest/gtest.h"

#include "rpc/Buffer.hh"

using namespace rpc;

TEST(MessageTest, default_ctor__position_is_0) {
	Buffer b;
	EXPECT_EQ(b.position(), 0);
}

TEST(MessageTest, default_ctor__size_is_0) {
	Buffer b;
	EXPECT_EQ(b.size(), 0);
}

TEST(MessageTest, default_ctor__data_is_0) {
	Buffer b;
	EXPECT_EQ(b.data(), nullptr);
}

TEST(MessageTest, grow__empty__size_is_changed) {
	Buffer b;
	b.grow(32);
	EXPECT_EQ(b.size(), 32);
}

TEST(MessageTest, grow__empty__data_is_set) {
	Buffer b;
	b.grow(32);
	EXPECT_NE(b.data(), nullptr);
}

TEST(MessageTest, grow__not_empty__size_is_changed) {
	Buffer b;
	b.grow(32);
	EXPECT_EQ(b.size(), 32);
}

TEST(MessageTest, grow__not_empty__data_is_set) {
	Buffer b;
	b.grow(32);
	EXPECT_NE(b.data(), nullptr);
}

TEST(MessageTest, store__empty__size_increased) {
	Buffer b;
	uint64_t n = 10;
	b.store(n);

	EXPECT_GE(b.size(), sizeof(uint64_t));
}

TEST(MessageTest, store__empty__position_changed) {
	Buffer b;
	uint64_t n = 10;
	b.store(n);

	EXPECT_EQ(b.position(), sizeof(uint64_t));
}

TEST(MessageTest, rewind__after_store__position_is_0) {
	Buffer b;
	uint64_t n = 10;
	b.store(n);

	b.rewind();

	EXPECT_EQ(b.position(), 0);
}

TEST(MessageTest, load__single__same_values) {
	Buffer b;
	uint64_t n1 = 10;
	b.store(n1);

	b.rewind();

	uint64_t n2;
	b.load(n2);

	EXPECT_EQ(n1, n2);
}

TEST(MessageTest, load__multiple__same_values) {
	Buffer b;

	static const size_t n = 200;

	std::array<uint16_t, n> data;

	for (size_t i = 0; i < n; i++) {
		data[i] = i*i;
		b.store(data[i]);
	}
	
	b.rewind();

	for (size_t i = 0; i < n; i++) {
		uint16_t a;
		b.load(a);

		EXPECT_EQ(a, data[i]);
	}
}

TEST(MessageTest, load__out_of_bounds__throws) {
	Buffer b;
	uint64_t n = 10;
	b.store(n);

	b.rewind();

	b.load(n);

	uint8_t n2;
	EXPECT_THROW({b.load(n2);}, std::range_error);
}

TEST(MessageTest, store_load__string) {
	Buffer b;
	std::string s1 = "asfdsdf";

	b.store(s1);

	b.rewind();

	std::string s2;
	b.load(s2);

	EXPECT_EQ(s1, s2);
}

TEST(MessageTest, store_load__string_empty) {
	Buffer b;
	std::string s1;

	b.store(s1);

	b.rewind();

	std::string s2;
	b.load(s2);

	EXPECT_EQ(s1, s2);
}

TEST(MessageTest, store_load__string_null) {
	Buffer b;
	std::string s1 = "asdf\0asdfsf";

	b.store(s1);

	b.rewind();

	std::string s2;
	b.load(s2);

	EXPECT_EQ(s1, s2);
}

TEST(MessageTest, store_load__path) {
	Buffer b;
	std::filesystem::path p1 = "/etc/fstab";

	b.store(p1);

	b.rewind();

	std::filesystem::path p2;
	b.load(p2);

	EXPECT_EQ(p1, p2);
}

TEST(MessageTest, store_load__vector_empty) {
	Buffer b;
	std::vector<std::string> vs1;

	b.store(vs1);

	b.rewind();

	std::vector<std::string> vs2;
	b.load(vs2);

	EXPECT_EQ(vs1, vs2);
}

TEST(MessageTest, store_load__vector) {
	Buffer b;
	std::vector<int> vs1;
	vs1.push_back(10);
	vs1.push_back(11);
	vs1.push_back(12);

	b.store(vs1);

	b.rewind();

	std::vector<int> vs2;
	b.load(vs2);

	EXPECT_EQ(vs1, vs2);
}

TEST(MessageTest, store_load__vector_of_strings) {
	Buffer b;
	std::vector<std::string> vs1;
	vs1.push_back("s1");
	vs1.push_back("\0");
	vs1.push_back("");
	vs1.push_back("sad");

	b.store(vs1);

	b.rewind();

	std::vector<std::string> vs2;
	b.load(vs2);

	EXPECT_EQ(vs1, vs2);
}

TEST(MessageTest, store_load__array) {
	Buffer b;
	double a1[4] = { 4.3, 1.2, 2./3 };

	b.store(a1);

	b.rewind();

	double a2[4];

	b.load(a2);

	for (size_t i = 0; i < 3; ++i) {
		EXPECT_EQ(a1[0], a2[0]);
	}
}

TEST(MessageTest, store_load__std__array) {
	Buffer b;
	std::array<std::string, 3> a1 = {
		"s1", "s2", "s3"
	};

	b.store(a1);

	b.rewind();

	std::array<std::string, 3> a2;

	b.load(a2);

	EXPECT_EQ(a1, a2);
}

TEST(MessageTest, store_load__tuple) {
	Buffer b;
	std::tuple<long unsigned int, uint16_t, double> t1 = {
		1, 89, 2.71
	};

	b.store(t1);

	b.rewind();

	std::tuple<long unsigned int, uint16_t, double> t2;

	b.load(t2);

	EXPECT_EQ(t1, t2);
}

TEST(MessageTest, store_tuple_load_elemnts) {
	Buffer b;
	std::tuple<long unsigned int, std::string, double> t1 = {
		1, "asdfsadf", 2.71
	};

	b.store(t1);

	b.rewind();

	long unsigned int t21;
	std::string t22;
	double t23;

	b.load(t21);
	b.load(t22);
	b.load(t23);

	EXPECT_EQ(std::get<0>(t1), t21);
	EXPECT_EQ(std::get<1>(t1), t22);
	EXPECT_EQ(std::get<2>(t1), t23);
}

TEST(MessageTest, store_elemtens_load_tuple) {
	Buffer b;
	long unsigned int t11 = 13;
	std::string t12 = "asdfasfd";
	double t13 = 3.14;

	b.store(t11);
	b.store(t12);
	b.store(t13);

	b.rewind();

	std::tuple<long unsigned int, std::string, double> t2;

	b.load(t2);

	EXPECT_EQ(t11, std::get<0>(t2));
	EXPECT_EQ(t12, std::get<1>(t2));
	EXPECT_EQ(t13, std::get<2>(t2));
}
TEST(MessageTest, operator_insert__multiple) {
	Buffer b;
	std::array<std::string, 3> a1 = {
		"s1", "s2", "s3"
	};

	std::vector<int> v1;
	v1.push_back(10);
	v1.push_back(11);

	uint16_t n1 = 42;

	std::string s1 = "i";

	std::tuple<int, std::string, bool> t1 = { 10, "asd", true };

	b << a1 << v1 << n1 << s1 << t1;

	b.rewind();

	std::array<std::string, 3> a2;
	std::vector<int> v2;
	uint16_t n2;
	std::string s2;
	std::tuple<int, std::string, bool> t2;

	b >> a2 >> v2 >> n2 >> s2 >> t2;

	EXPECT_EQ(a1, a2);
	EXPECT_EQ(v1, v2);
	EXPECT_EQ(n1, n2);
	EXPECT_EQ(s1, s2);
}

TEST(MessageTest, copy_ctor) {
	Buffer m1;

	int n1 = 10;
	std::string s1 = "adfsafdadsf";

	m1 << n1 << s1;

	Buffer m2(m1);

	ASSERT_EQ(m1.position(), m2.position());
	ASSERT_GE(m1.size(), m1.position());

	int n2;
	std::string s2;

	m2.rewind();

	m2 >> n2 >> s2;

	EXPECT_EQ(n1, n2);
	EXPECT_EQ(s1, s2);
}

TEST(MessageTest, move_ctor) {
	Buffer m1;

	int n1 = 10;
	std::string s1 = "adfsafdadsf";

	m1 << n1 << s1;

	Buffer m2(std::move(m1));

	int n2;
	std::string s2;

	m2.rewind();

	m2 >> n2 >> s2;

	EXPECT_EQ(m1.position(), 0);
	EXPECT_EQ(m1.size(), 0);
	EXPECT_EQ(m1.data(), nullptr);
	EXPECT_EQ(n1, n2);
	EXPECT_EQ(s1, s2);
}

class TestTypeBase {
public:
	int n;

private:
	friend Buffer &operator <<(Buffer &buf, const TestTypeBase &t);
	friend const Buffer &operator >>(const Buffer &buf, TestTypeBase &t);
};

Buffer &operator <<(Buffer &buf, const TestTypeBase &t)
{
	buf << t.n;
	return buf;
}

const Buffer &operator >>(const Buffer &buf, TestTypeBase &t)
{
	buf >> t.n;
	return buf;
}

class TestType : public TestTypeBase {
public:
	using value_type = uint64_t;

	void set_data(std::array <value_type, 3> data_) {
		data = data_;
	}

	void set_s(std::string s_) {
		s = s_;
	}

	std::string get_s() {
		return s;
	}

	std::array <value_type, 3> get_data() {
		return data;
	}

private:
	friend Buffer &operator <<(Buffer &buf, const TestType &t);
	friend const Buffer &operator >>(const Buffer &buf, TestType &t);

	std::array <value_type, 3> data;
	std::string s;

};

Buffer &operator <<(Buffer &buf, const TestType &t)
{
	buf << t.data;
	buf << t.s;
	return buf;
}

const Buffer &operator >>(const Buffer &buf, TestType &t)
{
	buf >> t.data;
	buf >> t.s;
	return buf;
}

TEST(MessageTest, custom_class) {
	Buffer buf;

	TestTypeBase b1;
	b1.n = 42;
	TestType t1;
	t1.set_data({1, 2, 3});
	t1.set_s("asdfa");

	buf << b1 << t1;

	buf.rewind();

	TestTypeBase b2;
	TestType t2;
	buf >> b2 >> t2;

	EXPECT_EQ(b1.n, b2.n);
	EXPECT_EQ(t2.get_s(), t1.get_s());
	EXPECT_EQ(t2.get_data()[0], t1.get_data()[0]);
	EXPECT_EQ(t2.get_data()[1], t1.get_data()[1]);
	EXPECT_EQ(t2.get_data()[2], t1.get_data()[2]);
}

TEST(MessageTest, custom_class_in_tuple) {
	Buffer buf;

	TestTypeBase b1;
	b1.n = 42;
	TestType t1;
	t1.set_data({1, 2, 3});
	t1.set_s("asdfa");

	std::tuple<TestTypeBase, TestType> tt1(b1, t1);

	buf << tt1;

	buf.rewind();

	std::tuple<TestTypeBase, TestType> tt2;

	buf >> tt2;

	TestTypeBase b2 = std::get<0>(tt2);
	TestType t2 = std::get<1>(tt2);

	EXPECT_EQ(b2.n, b1.n);
	EXPECT_EQ(t2.get_s(), t1.get_s());
	EXPECT_EQ(t2.get_data()[0], t1.get_data()[0]);
	EXPECT_EQ(t2.get_data()[1], t1.get_data()[1]);
	EXPECT_EQ(t2.get_data()[2], t1.get_data()[2]);
}
