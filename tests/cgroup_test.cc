#include <thread>
#include <chrono>
#include <fstream>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "ResourceStats.hh"
#include "posix/CGroupFreezer.hh"

using namespace ::testing;

class CGroupTest : public Test
{
public:
	CGroup cg;

	inline static const std::filesystem::path cgdir = "cgroup-test";
	inline static const std::filesystem::path tgids = cgdir / "cgroup.procs";
	inline static const std::filesystem::path pids = cgdir / "tasks";

	void write_tgids(const std::initializer_list<int> ids) {
		std::ofstream out(tgids);
		for (auto i : ids) {
			out << i << "\n";
		}
	}

	void write_pids(const std::initializer_list<int> ids) {
		std::ofstream out(pids);
		for (auto i : ids) {
			out << i << "\n";
		}
	}

	void write(const std::initializer_list<int> ids) {
		write_tgids(ids);
		write_pids(ids);
	}

	virtual void SetUp() override {
		cg.create();
	}

	virtual void TearDown() override {
		if (std::filesystem::exists(tgids))
			std::filesystem::remove(tgids);
		if (std::filesystem::exists(pids))
			std::filesystem::remove(pids);
		cg.remove();
	}

protected:

private:

};

TEST(CGroupTest, set_dir) {
	CGroup cg;
	cg.set_dir("aa");
	EXPECT_EQ(cg.get_dir(), "aa");
}

TEST(CGroupTest, create_remove__not_exists__created_and_removed) {
	std::string dir = "cgtestdir";
	ASSERT_FALSE(std::filesystem::exists(dir));

	CGroup cg;
	cg.set_dir(dir);
	cg.create();
	EXPECT_TRUE(std::filesystem::exists(dir));

	cg.remove();
	EXPECT_FALSE(std::filesystem::exists(dir));
}

TEST(CGroupTest, create_remove__exists__not_created_and_removed) {
	std::string dir = "cgtestdir";
	ASSERT_FALSE(std::filesystem::exists(dir));

	CGroup cg;
	cg.set_dir(dir);
	cg.create();
	EXPECT_TRUE(std::filesystem::exists(dir));

	cg.create();
	EXPECT_TRUE(std::filesystem::exists(dir));

	cg.remove();
	EXPECT_FALSE(std::filesystem::exists(dir));
}

TEST(CGroupTest, add_pid__empty__written_to_threads) {
	std::filesystem::path dir = "cgtestdir";

	CGroup cg;
	cg.set_dir(dir);
	cg.create();

	cg.add_pid(100);

	int id = 0;

	{
		std::ifstream in(dir / "cgroup.procs");
		in >> id;
	}

	EXPECT_EQ(id, 100);

	std::filesystem::remove(dir / "cgroup.procs");
	cg.remove();
}

TEST(CGroupTest, get_threads_path__dir_set__return_threads_path) {
	std::filesystem::path dir = "cgtestdir";

	CGroup cg;
	cg.set_dir(dir);

	EXPECT_EQ(cg.get_tgids_path(), dir / "cgroup.procs");
}

TEST(CGroupTest, get_procs_path__dir_set__return_procs_path) {
	std::filesystem::path dir = "cgtestdir";

	CGroup cg;
	cg.set_dir(dir);

	EXPECT_EQ(cg.get_pids_path(), dir / "tasks");
}
