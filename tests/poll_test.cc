#include <thread>
#include <chrono>

#include <signal.h>

#include "gtest/gtest.h"

#include "Poll.hh"

TEST(PollTest, default_ctor__not_active) {
	Poll poll;
	EXPECT_FALSE(poll.is_active());
}

TEST(PollTest, is_active__create__active) {
	Poll poll;
	poll.create();
	EXPECT_TRUE(poll.is_active());
}

TEST(PollTest, is_active__create_close__not_active) {
	Poll poll;
	poll.create();
	poll.close();
	EXPECT_FALSE(poll.is_active());
}

TEST(PollTest, notify__not_enabled__nothing) {
	Poll poll;
	poll.create();
	poll.notify();
}

TEST(PollTest, notify__thread_notifies__only_events_alerted) {
	Poll poll;
	poll.create();
	poll.notify_on_events();

	size_t nr_events = 4;

	std::thread t1([&](){
		for (size_t i = 0; i < nr_events; ++i) {
			std::this_thread::sleep_for(std::chrono::milliseconds(250));
			poll.notify();
		}
	});

	Poll::Alerts alerts;
	for (size_t i = 0; i < nr_events; ++i) {
		poll.wait(alerts);
		ASSERT_EQ(alerts.events, 1ul);
	}

	t1.join();

	poll.close();
}

TEST(PollTest, notify__thread_notifies__alerts_reset) {
	Poll poll;
	poll.create();
	poll.notify_on_events();

	size_t nr_events = 4;

	std::thread t1([&](){
		for (size_t i = 0; i < nr_events; ++i) {
			std::this_thread::sleep_for(std::chrono::milliseconds(250));
			poll.notify();
		}
	});

	Poll::Alerts alerts;
	alerts.timer = 1;
	alerts.events = 100;
	alerts.signals.push_back(1);
	alerts.userdata.push_back(1);
	for (size_t i = 0; i < nr_events; ++i) {
		poll.wait(alerts);
		ASSERT_EQ(alerts.events, 1ul);
		ASSERT_EQ(alerts.timer, 0ul);
		ASSERT_TRUE(alerts.signals.empty());
		ASSERT_TRUE(alerts.userdata.empty());
	}

	t1.join();

	poll.close();
}

TEST(PollTest, timer__enabled__only_timer_alerted) {
	Poll poll;
	poll.create();
	poll.notify_on_timer(std::chrono::milliseconds(250));

	size_t nr_ticks = 4;

	uint64_t tp_prev = std::chrono::duration_cast<std::chrono::milliseconds>(
		std::chrono::system_clock::now().time_since_epoch()
	).count();

	Poll::Alerts alerts;
	for (size_t i = 0; i < nr_ticks; ++i) {
		poll.wait(alerts);

		ASSERT_EQ(alerts.timer, 1ul);

		uint64_t tp_curr = std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::system_clock::now().time_since_epoch()
		).count();

		uint64_t dt = tp_curr - tp_prev;

		ASSERT_TRUE(dt > 240 && dt < 260);

		tp_prev = tp_curr;
	}

	poll.close();
}

TEST(PollTest, timer__enabled__alerts_reset) {
	Poll poll;
	poll.create();
	poll.notify_on_timer(std::chrono::milliseconds(250));

	size_t nr_ticks = 2;

	Poll::Alerts alerts;
	alerts.timer = 100;
	alerts.events = 100;
	alerts.signals.push_back(1);
	alerts.userdata.push_back(1);

	for (size_t i = 0; i < nr_ticks; ++i) {
		poll.wait(alerts);

		ASSERT_EQ(alerts.events, 0ul);
		ASSERT_EQ(alerts.timer, 1ul);
		ASSERT_TRUE(alerts.signals.empty());
		ASSERT_TRUE(alerts.userdata.empty());
	}

	poll.close();
}

TEST(PollTest, signals__listen_2_signals__only_singals_alerted) {
	Poll poll;
	poll.create();
	poll.notify_on_signals({SIGUSR1, SIGUSR2});

	pid_t pid = getpid();
	size_t nr_events = 4;

	std::thread t1([&](){
		for (size_t i = 0; i < nr_events; ++i) {
			std::this_thread::sleep_for(std::chrono::milliseconds(250));
			kill(pid, SIGUSR1);
			kill(pid, SIGUSR2);
		}
	});

	Poll::Alerts alerts;
	for (size_t i = 0; i < nr_events; ++i) {
		poll.wait(alerts);

		ASSERT_TRUE(alerts.have_signal(SIGUSR1));
		ASSERT_TRUE(alerts.have_signal(SIGUSR2));
		ASSERT_TRUE(alerts.have_signals({SIGUSR1, SIGUSR2}));
	}

	t1.join();

	poll.close();
}

TEST(PollTest, signals__notified__alerts_reset) {
	Poll poll;
	poll.create();
	poll.notify_on_signals({SIGUSR1, SIGUSR2});

	pid_t pid = getpid();
	size_t nr_events = 4;

	std::thread t1([&](){
		for (size_t i = 0; i < nr_events; ++i) {
			std::this_thread::sleep_for(std::chrono::milliseconds(250));
			kill(pid, SIGUSR1);
			kill(pid, SIGUSR2);
		}
	});

	Poll::Alerts alerts;
	alerts.timer = 1;
	alerts.events = 100;
	alerts.signals.push_back(1);
	alerts.userdata.push_back(1);
	for (size_t i = 0; i < nr_events; ++i) {
		poll.wait(alerts);

		ASSERT_EQ(alerts.events, 0ul);
		ASSERT_EQ(alerts.timer, 0ul);
		ASSERT_TRUE(alerts.userdata.empty());
		ASSERT_TRUE(alerts.have_signals({SIGUSR1, SIGUSR2}));
	}

	t1.join();

	poll.close();
}
