#pragma once

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "Worker.hh"

class WorkerMock : public Worker
{
public:
	WorkerMock()
	{ }

	virtual ~WorkerMock()
	{ }

	MOCK_METHOD(void, stop, (), (override));

	MOCK_METHOD(void, submit_command, (std::shared_ptr<Command>), (override));

	// MOCK_METHOD(void, freeze_command, (Command::id_type, bool), (override));

private:

};

