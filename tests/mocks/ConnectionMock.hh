#pragma once

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "rpc/Connection.hh"
#include "rpc/Runtime.hh"

using namespace ::testing;

rpc::Runtime protocol;
rpc::Socket socket;

class ConnectionMock : public rpc::Connection
{
public:
	ConnectionMock()
	: rpc::Connection(std::move(socket), protocol, 0)
	{ }

	virtual ~ConnectionMock()
	{ }

protected:

private:

};
