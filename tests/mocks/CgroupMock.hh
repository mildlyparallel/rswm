#pragma once

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "posix/Cgroup.hh"

using namespace ::testing;

class CgroupMock : public Command
{
public:
	CgroupMock()
	{ }

	CgroupMock(const Task &other)
	: Command(other)
	{ }

	virtual ~CgroupMock()
	{ }

	MOCK_METHOD(void, spawn, (), (override));
	MOCK_METHOD(bool, sync, (), (override));
	MOCK_METHOD(void, stop, (bool), (override));
	MOCK_METHOD(void, read_stats, (), (override));
	MOCK_METHOD(void, freeze, (bool), (override));
};

