#pragma once

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "ConnectionMock.hh"
#include "DispatcherClient.hh"

class DispatcherClientMock : public DispatcherClient
{
public:
	DispatcherClientMock()
	: DispatcherClient(std::make_shared<ConnectionMock>())
	{ }

	virtual ~DispatcherClientMock()
	{ }

	// MOCK_METHOD(bool, submit_command, (std::shared_ptr<Command>), (override));
    //
	// MOCK_METHOD(void, stop_all_commands, (bool), (override));
    //
	// MOCK_METHOD(void, freeze_command, (Command::id_type, bool), (override));

protected:

private:

};

