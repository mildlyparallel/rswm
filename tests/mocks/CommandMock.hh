#pragma once

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "Command.hh"

using namespace ::testing;

class CommandMock : public Command
{
public:
	CommandMock()
	{ }

	CommandMock(Command::id_type id)
	{
		set_id(id);
	}

	CommandMock(const Task &other)
	: Command(other)
	{ }

	virtual ~CommandMock()
	{ }

	MOCK_METHOD(void, spawn, (), (override));
	MOCK_METHOD(bool, sync, (), (override));
	MOCK_METHOD(void, stop, (bool), (override));
	MOCK_METHOD(void, read_stats, (), (override));
	MOCK_METHOD(void, freeze, (bool), (override));
};
