#pragma once

#include "gtest/gtest.h"
#include "gmock/gmock.h"

#include "WorkerServer.hh"

class WorkerServerMock : public WorkerServer
{
public:
	WorkerServerMock(DispatcherClient &dispatcher)
	: WorkerServer(dispatcher)
	{ }

	virtual ~WorkerServerMock()
	{ }

protected:
	MOCK_METHOD(std::shared_ptr<Command>, create_command, (const Command &command), (override, const));

private:

};
