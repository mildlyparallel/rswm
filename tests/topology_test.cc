#include "gtest/gtest.h"

#include <fstream>
#include <filesystem>

#include "posix/Topology.hh"

using namespace ::testing;

TEST(CpuTest, returned_empty_graph) {
	Topology topo;
	topo.read();
	std::cerr << "topo.get_nr_threads() = " << topo.get_nr_threads() << std::endl;
	std::cerr << "topo.get_nr_cores() = " << topo.get_nr_cores() << std::endl;
	std::cerr << "topo.get_nr_sockets() = " << topo.get_nr_sockets() << std::endl;

}


