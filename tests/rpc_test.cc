#include <thread>
#include <chrono>

#include "gtest/gtest.h"
#include "gmock/gmock.h"

using namespace ::testing;

#include "rpc/Runtime.hh"
#include "rpc/Client.hh"
#include "rpc/Server.hh"

using namespace rpc;


class TestMethods
{
public:
	enum Method : method_name_type {
		MethodOne = 1,
		MethodTwo,
		EventOne,
		EventTwo
	};

	MOCK_METHOD(int, method_one, (
		std::shared_ptr<Connection>,
		int
	), ());

	MOCK_METHOD(int, method_two, (
		std::shared_ptr<Connection>,
		std::string
	));

	MOCK_METHOD(void, event_one, (
		std::shared_ptr<Connection> con,
		int
	));

	MOCK_METHOD(void, event_two, (
		std::shared_ptr<Connection>,
		std::string
	));

	void bind(Runtime &runtime)
	{
		runtime.bind(MethodOne, &TestMethods::method_one, this);
		runtime.bind(MethodTwo, &TestMethods::method_two, this);
		runtime.bind(EventOne, &TestMethods::event_one, this);
		runtime.bind(EventTwo, &TestMethods::event_two, this);
	}

protected:

private:

};

class RPCTest : public Test {
public:
	Server server;
	Client client;

	TestMethods cli_methods, srv_methods;
	std::shared_ptr<Connection> cli_con, srv_con;

	void SetUp() override {
		std::atomic_int n(0);

		server.on_connection([&](std::shared_ptr<Connection> con){
			ASSERT_FALSE(srv_con);
			srv_con = con;
			n++;
		});

		cli_methods.bind(client);
		srv_methods.bind(server);

		server.listen(9123);
		cli_con = client.connect(9123);

		while (n == 0)
			std::this_thread::sleep_for(std::chrono::milliseconds(10));
	}

	void TearDown() override {
		client.disconnect();
		server.stop();

		cli_con = nullptr;
		srv_con = nullptr;
	}
};

TEST_F(RPCTest, client_call_and_wait) {
	std::string abc = "abc";

	EXPECT_CALL(srv_methods, method_one(srv_con, 1))
		.WillOnce(Return(6));
	EXPECT_CALL(srv_methods, method_one(srv_con, 2))
		.WillOnce(Return(5));
	EXPECT_CALL(srv_methods, method_two(srv_con, abc))
		.WillOnce(Return(4));

	int r0, r1, r2;
	cli_con->call_and_wait(TestMethods::MethodOne, 1) >> r0;
	cli_con->call_and_wait(TestMethods::MethodOne, 2) >> r1;
	cli_con->call_and_wait(TestMethods::MethodTwo, abc) >> r2;

	EXPECT_EQ(r0, 6);
	EXPECT_EQ(r1, 5);
	EXPECT_EQ(r2, 4);
}

TEST_F(RPCTest, client_publish) {
	std::string abc = "abc";

	EXPECT_CALL(srv_methods, event_one(srv_con, 1));
	EXPECT_CALL(srv_methods, event_two(srv_con, abc));

	cli_con->publish(TestMethods::EventOne, 1);
	cli_con->publish(TestMethods::EventTwo, abc);

	std::this_thread::sleep_for(std::chrono::milliseconds(10));
}

TEST_F(RPCTest, server_publish) {
	std::string abc = "abc";

	EXPECT_CALL(cli_methods, event_one(cli_con, 1));
	EXPECT_CALL(cli_methods, event_two(cli_con, abc));

	srv_con->publish(TestMethods::EventOne, 1);
	srv_con->publish(TestMethods::EventTwo, abc);

	std::this_thread::sleep_for(std::chrono::milliseconds(10));
}

TEST_F(RPCTest, server_call_and_wait) {
	std::string abc = "abc";

	EXPECT_CALL(cli_methods, method_one(cli_con, 1))
		.WillOnce(Return(6));
	EXPECT_CALL(cli_methods, method_one(cli_con, 2))
		.WillOnce(Return(5));
	EXPECT_CALL(cli_methods, method_two(cli_con, abc))
		.WillOnce(Return(4));

	int r0, r1, r2;
	srv_con->call_and_wait(TestMethods::MethodOne, 1) >> r0;
	srv_con->call_and_wait(TestMethods::MethodOne, 2) >> r1;
	srv_con->call_and_wait(TestMethods::MethodTwo, abc) >> r2;

	EXPECT_EQ(r0, 6);
	EXPECT_EQ(r1, 5);
	EXPECT_EQ(r2, 4);
}

TEST_F(RPCTest, simul_send) {
	size_t nr_calls = 30;

	for (size_t i = 0; i < nr_calls; ++i) {
		EXPECT_CALL(cli_methods, method_one(cli_con, i))
			.WillOnce(Return(i+1));
		EXPECT_CALL(srv_methods, method_one(srv_con, i))
			.WillOnce(Return(i+1));
	}

	auto fn = [&](std::shared_ptr<Connection> con){
		for (size_t i = 0; i < nr_calls; ++i) {
			int n = 0;
			con->call_and_wait(TestMethods::MethodOne, i) >> n;
			EXPECT_EQ(n, i + 1);
		}
	};

	std::thread srv_thr(fn, cli_con);
	std::thread cli_thr(fn, srv_con);

	srv_thr.join();
	cli_thr.join();
}
