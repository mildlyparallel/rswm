#pragma once

#include <array>
#include <atomic>
#include <chrono>
#include <vector>

#include "Logger.hh"

#include <sys/epoll.h>

class Poll
{
public:
	Poll();

	Poll(const Poll &other) = delete;

	Poll(Poll &&other);

	Poll &operator=(const Poll &other) = delete;

	Poll &operator=(Poll &&other);

	virtual ~Poll();

	static constexpr size_t max_events = 10;

	// XXX: is it neccessary?

	using userdata_type = uint64_t;

	using userdata_array = std::array<userdata_type, Poll::max_events>;

	using signals_array = std::array<uint32_t, Poll::max_events>;

	enum EventName : uint32_t {
		IN        = EPOLLIN,
		OUT       = EPOLLOUT,
		RDHUP     = EPOLLRDHUP,
		PRI       = EPOLLPRI,
		ERR       = EPOLLERR,
		HUP       = EPOLLHUP,
		ET        = EPOLLET,
		ONESHOT   = EPOLLONESHOT,
		WAKEUP    = EPOLLWAKEUP,
		EXCLUSIVE = EPOLLEXCLUSIVE,
		Hup       = EPOLLHUP | EPOLLRDHUP
	};

	void create();

	void add(uint32_t events, int fd, userdata_type userdata);

	void modify(uint32_t events, int fd, userdata_type userdata);

	void remove(int fd);

	bool is_active() const;

	struct Alerts {
		size_t timer = 0;
		size_t events = 0;
		std::vector<uint32_t> signals;
		std::vector<userdata_type> userdata;

		bool have_signal(uint32_t signo) const {
			return std::find(signals.cbegin(), signals.cend(), signo) != signals.cend();
		}

		bool have_signals(std::initializer_list<uint32_t> signals) const {
			for (auto s : signals) {
				if (have_signal(s))
					return true;
			}
			return false;
		}

		void reset() {
			timer = 0;
			events = 0;
			signals.clear();
			userdata.clear();
		}
	};

	void wait();

	void wait(Alerts &alerts);

	void notify();

	void close();

	void notify_on_events();

	void notify_on_signals(std::initializer_list<uint32_t> signals);

	void notify_on_timer(std::chrono::milliseconds interval);

protected:

private:
	void poll_ctl(
		int op,
		uint32_t events,
		int fd,
		userdata_type userdata
	);

	size_t read_signals(std::vector<uint32_t> &signals);

	size_t read_events();

	size_t read_timer();

	bool read(int fd, void *p, size_t s);

	enum Userdata : uint64_t {
		Event  = 0xffffffff,
		Signal = 0xfffffffe,
		Timer  = 0xfffffffd,
	};

	Logger m_logger;

	int m_poll_fd;

	int m_signals_fd;

	int m_event_fd;

	int m_timer_fd;

	std::atomic_size_t m_recv_term_signals;
};

