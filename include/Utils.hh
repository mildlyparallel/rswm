#pragma once

#include <system_error>
#include <string>

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#define THROW_IF(cond) {\
	if ((cond)) \
		throw std::system_error(errno, std::generic_category(), __FILE__ ":" STR(__LINE__)); \
} while(false);

namespace Utils {

std::string generate_stirng(size_t size);

} /* namespace Utils */

