#pragma once

#include <memory>
#include <vector>

#include "Dispatcher.hh"

class Poll;
class WorkerPool;
class CommandPool;

class DispatcherLocal : public Dispatcher
{
public:
	DispatcherLocal(CommandPool &commands);

	virtual ~DispatcherLocal();

	virtual std::shared_ptr<Assignment> assign(
		const Task &task,
		const std::vector<std::shared_ptr<Worker>> &workers
	);

protected:
	CommandPool &m_commands;
};

