#pragma once

#include <map>
#include <tuple>

#include "Scheduler.hh"

#include "TaskGraph.hh"

class Dispatcher;
class WorkerPool;

class SchedulerLifo : public Scheduler
{
public:
	SchedulerLifo(WorkerPool &workers, Dispatcher &dispatcher);

	virtual ~SchedulerLifo();

	virtual Result run(const TaskGraph &graph);

private:
	TaskGraph::const_iterator find_next(const TaskGraph &graph) const;

	void stop_submitted();
};
