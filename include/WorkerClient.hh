#pragma once

#include <string>
#include <vector>
#include <map>

#include "Worker.hh"


#include "Command.hh"

namespace rpc {
class Connection;
}

class WorkerClient : public Worker
{
public:
	WorkerClient(std::shared_ptr<rpc::Connection> connection);

	WorkerClient(const WorkerClient &other) = delete;

	WorkerClient(WorkerClient &&other) = default;

	virtual ~WorkerClient();

	void set_connection(std::shared_ptr<rpc::Connection> &client);

	virtual void submit_command(std::shared_ptr<Command> command);

// 	virtual void reset() final;

	virtual void stop_all_commands(bool hard);

	// void kill(Command::id_type cid);

	virtual void freeze_command(Command::id_type cid, bool freeze);

	virtual void stop();

private:
	std::shared_ptr<rpc::Connection> m_connection;
};
