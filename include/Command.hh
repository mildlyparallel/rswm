#pragma once

#include <string>
#include <chrono>
#include <chrono>
#include <shared_mutex>

#include "Task.hh"
#include "ResourceRates.hh"
#include "Logger.hh"

class Worker;

class Command : public Task
{
public:
	enum State : uint8_t {
		Empty = 0,
		Pending,
		Spawned,
		Exited,
		Terminated,
		Failed,

		nr_states
	};

	using id_type = uint64_t;

	using clock_type = std::chrono::steady_clock;

	using timepoint_type = std::chrono::time_point<clock_type>;

	using result_type = std::tuple<State, int>;

	Command();

	Command(const Command &cmd);

	Command(const Task &task);

	virtual ~Command();

	void set_id(id_type id);
	id_type get_id() const;

	void set_hostnames(const std::vector<std::string> &hosts);
	const std::vector<std::string> &get_hostnames() const;

	void set_hostname(const std::string &host);
	const std::string &get_hostname() const;

	State get_state() const;

	void set_state(State s);

	timepoint_type get_state_update_tp() const;

	void set_return_code(int rc);

	bool finished() const;

	int get_return_code() const;

	virtual void spawn();

	virtual void stop(bool force = false);

	virtual bool sync();

	virtual void read_stats();

	virtual void freeze(bool freeze);

	virtual const ResourceStats &get_stats() const;

	virtual void update_rates(const ResourceStats &stats);

	virtual const ResourceRates &get_rates() const;

	void set_worker(std::shared_ptr<Worker> worker);

	std::shared_ptr<Worker> get_worker() const;

protected:
	Logger m_logger;

	ResourceRates m_rates;

private:
	friend std::ostream &operator<<(std::ostream &os, Command::State s);

	friend rpc::Buffer &operator<<(rpc::Buffer &msg, const Command &err);

	friend const rpc::Buffer &operator>>(const rpc::Buffer &msg, Command &err);

	static constexpr const char *state_str[nr_states] = {
		"Empty",
		"Pending",
		"Spawned",
		"Exited",
		"Terminated",
		"Failed"
	};

	id_type m_id;

	timepoint_type m_state_update_tp;

	State m_state;

	int m_return_code;

	mutable std::shared_mutex m_state_lock;

	std::vector<std::string> m_hostnames;

	std::string m_hostname;

	std::shared_ptr<Worker> m_worker;
};

rpc::Buffer &operator<<(rpc::Buffer &msg, const Command &cmd);

const rpc::Buffer &operator>>(const rpc::Buffer &msg, Command &cmd);

std::ostream &operator<<(std::ostream &os, Command::State s);

