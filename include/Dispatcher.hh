#pragma once

#include <memory>
#include <vector>

#include "AssignmentPool.hh"
#include "rpc/Types.hh"

class Task;
class Worker;
class Assignment;
class Poll;

class Dispatcher
{
public:
	Dispatcher();

	virtual ~Dispatcher();

	enum Method : rpc::method_name_type {
		RegisterWorker = 2,
		UnregisterWorker,
		UpdateCommandState,
		UpdateCommandRates,
		RunRemoteCommand,

		nr_methods
	};

	virtual std::shared_ptr<Assignment> assign(
		const Task &task,
		const std::vector<std::shared_ptr<Worker>> &workers
	);

	void set_poll(Poll *poll);

	bool stopped() const;

protected:
	Logger m_logger;

	Poll *m_poll;

	AssignmentPool m_assignments;

	std::atomic_bool m_stopped;

private:
	// static constexpr const char *method_str[nr_methods] = {
	// 	"Add",
	// 	"Remove",
	// 	"Heartbeat",
	// 	"HandleCommandFinish",
	// 	"HandleCommandStats",
	// 	"RunRemoteCommand",
	// };

};
