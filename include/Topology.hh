#pragma once

class Topology
{
public:
	Topology();

	virtual ~Topology();

	virtual void read() = 0;

	size_t get_nr_threads() const;

	size_t get_nr_cores() const;

	size_t get_nr_sockets() const;

	void set_sysfs_prefix(const std::filesystem::path &p);

protected:
	using cpulist_type = std::bitset<64>;

	std::vector<unsigned> m_threads;

	std::vector<cpulist_type> m_cores;

	std::vector<cpulist_type> m_sockets;

private:

};
