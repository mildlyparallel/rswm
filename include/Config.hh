#pragma once

#include <filesystem>
#include <chrono>
#include <tuple>
#include <array>

#define VERSION_STR "1.0.0"

#define ENABLE_CGROUPS 1

#define ENABLE_PERF 1

#define CGROUP_FREEZER_ROOT "/sys/fs/cgroup/freezer/rk/"

#define MAX_COMBINATION_SIZE 16

#include "ResourceStats.hh"

#if ENABLE_PERF
#include <linux/perf_event.h>
#endif

class Config
{
public:
	inline static unsigned port = 9123;

	inline static std::string host = "0.0.0.0";

	inline static const constexpr size_t max_comb_size = MAX_COMBINATION_SIZE;

#if ENABLE_CGROUPS
	inline static std::filesystem::path cgroup_freezer_root = CGROUP_FREEZER_ROOT;
#endif

	inline static std::chrono::milliseconds stats_poll_interval = std::chrono::milliseconds(250);

#if ENABLE_PERF
	struct PerfEvent {
		ResourceStats::Entry name;
		uint32_t type;
		uint32_t config;
	};

	static constexpr PerfEvent perf_events[] = {
		{ResourceStats::Instructions, PERF_TYPE_HARDWARE, PERF_COUNT_HW_INSTRUCTIONS},
		{ResourceStats::CpuCycles, PERF_TYPE_HARDWARE, PERF_COUNT_HW_CPU_CYCLES},
		// {ResourceStats::CacheReferences, PERF_TYPE_HARDWARE, PERF_COUNT_HW_CACHE_REFERENCES},
		// {ResourceStats::CacheMisses, PERF_TYPE_HARDWARE, PERF_COUNT_HW_CACHE_MISSES},
		// {ResourceStats::BusCycles, PERF_TYPE_HARDWARE, PERF_COUNT_HW_BUS_CYCLES},
		// {ResourceStats::MemLoad, PERF_TYPE_HW_CACHE, PERF_COUNT_HW_CACHE_NODE | (PERF_COUNT_HW_CACHE_OP_READ << 8) | (PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16)},
		// {ResourceStats::MemStore, PERF_TYPE_HW_CACHE, PERF_COUNT_HW_CACHE_NODE | (PERF_COUNT_HW_CACHE_OP_WRITE << 8) | (PERF_COUNT_HW_CACHE_RESULT_ACCESS << 16)},
		// {ResourceStats::StallFrontend, PERF_TYPE_HARDWARE, PERF_COUNT_HW_STALLED_CYCLES_FRONTEND},
		// {ResourceStats::StallBackend, PERF_TYPE_HARDWARE, PERF_COUNT_HW_STALLED_CYCLES_BACKEND}
		// {ResourceStats::MemStore, PERF_TYPE_HW_CACHE, PERF_COUNT_HW_CPU_CYCLES},
		// {ResourceStats::BusAccess, PERF_TYPE_RAW, 0x19},
	};
#endif

	inline static int command_process_uid = -1;

	inline static int command_process_gid = -1;

	enum Action {
		Unknown = 0,
		RunLocal,
		RunRemote,
		Worker
	};

	inline static Action action;

	inline static std::string graph_script;

	inline static std::vector<std::string> graph_script_args;

	static void parse_cmdline(int argc, const char *argv[]);
};

