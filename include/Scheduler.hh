#pragma once

#include <map>
#include <memory>

#include "Logger.hh"
#include "TaskGraph.hh"

class WorkerPool;
class Dispatcher;
class Assignment;

class Scheduler
{
public:
	using clock_type = std::chrono::steady_clock;

	using timepoint_type = std::chrono::time_point<clock_type>;

	Scheduler(WorkerPool &workers, Dispatcher &dispatcher);

	virtual ~Scheduler();

	enum Result {
		Unknown = 0,
		Done,
		Submitted,
		WaitWorkers,
		WaitComplition,
		Failed,
		MeasuredBase
	};

	virtual Result run(const TaskGraph &graph) = 0;

protected:
	Result update_finished();

	Logger m_logger;

	WorkerPool &m_workers;
	Dispatcher &m_dispatcher;

	std::map<TaskGraph::task_id, std::shared_ptr<Assignment> > m_submitted;

	std::map<TaskGraph::task_id, std::shared_ptr<Assignment> > m_finished;

private:
	bool check_if_failed(std::shared_ptr<Assignment> assignment) const;
};
