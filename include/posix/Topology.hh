#pragma once

#include <bitset>
#include <vector>
#include <filesystem>

class Topology
{
public:
	Topology();

	virtual ~Topology();

	void read();

	size_t get_nr_threads() const;

	size_t get_nr_cores() const;

	size_t get_nr_sockets() const;

	void set_sysfs_prefix(const std::filesystem::path &p);

protected:

private:
	using cpulist_type = std::bitset<64>;

	void read_cpus();

	void read_nodes();

	void read_threads();

	static cpulist_type read_cpulist(const std::filesystem::path &p);

	std::filesystem::path m_sysfs;

	std::vector<unsigned> m_threads;

	std::vector<cpulist_type> m_cores;

	std::vector<cpulist_type> m_sockets;
};
