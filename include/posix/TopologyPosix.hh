#pragma once

#include <bitset>
#include <vector>
#include <filesystem>

#include "Topology.hh"

class TopologyPosix : public Topology
{
public:
	TopologyPosix();

	virtual ~TopologyPosix();

	virtual void read() final;

	void set_sysfs_prefix(const std::filesystem::path &p);

protected:

private:
	void read_cpus();

	void read_nodes();

	void read_threads();

	static cpulist_type read_cpulist(const std::filesystem::path &p);

	std::filesystem::path m_sysfs;
};
