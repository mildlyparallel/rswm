#pragma once

#include <string>
#include <vector>

// TODO: why?
#include <unistd.h>
#include <sys/types.h>

#include "Config.hh"
#include "Command.hh"

#if ENABLE_CGROUPS
#include "CGroupFreezer.hh"
#endif

#if ENABLE_PERF
#include "Perf.hh"
#endif

class CommandPosix : public Command
{
public:
	CommandPosix();

	CommandPosix(const Command &other);

	virtual ~CommandPosix();

	virtual void spawn() final;

	virtual bool sync() final;

	virtual void stop(bool force = false) final;

	virtual void read_stats() final;

	virtual void freeze(bool freeze = true) final;

private:
	void append_runtime_env();

	void cd_to_workdir() const;

	void redirect_outputs();

	void redirect_output(int fd, const std::string &file);

	char * const *args_to_argv() const;

	char * const *envs_to_envp() const;

	void read_outputs();

	void set_cpu_affinity();

	bool validate();

	pid_t m_pid;

	int m_exit_status;

#if ENABLE_CGROUPS
	CGroupFreezer m_freezer;
#endif

#if ENABLE_PERF
	Perf m_perf;
#endif
};

