#pragma once

#include <string>
#include <array>
#include <vector>
#include <tuple>
#include <filesystem>

#include "ResourceStats.hh"
#include "Logger.hh"
#include "CGroup.hh"

#if ENABLE_CGROUPS == 0
#error "File is being compiled but cgroups are not enabled"
#endif

class CGroupFreezer : public CGroup
{
public:
	CGroupFreezer();

	virtual ~CGroupFreezer();

	void freeze(bool freeze = true);

	enum State: unsigned {
		Frozen = 0,
		Freezing,
		Thawed
	};

	State read_state() const;

	bool is_frozen() const;

	virtual void update(ResourceStats &stats) const;

private:
	void create_dir(const std::filesystem::path &dir) const;
};
