#pragma once

#include <filesystem>

#include "ResourceStats.hh"

class ProcFS
{
public:
	static void update(
		ResourceStats &stats,
		const std::filesystem::path &threads
	);

protected:

private:
	struct thread_stat_t {
		uint64_t id;

		char state;
		uint64_t utime;
		uint64_t stime;

		uint64_t rchar;
		uint64_t wchar;
		uint64_t read_bytes;
		uint64_t write_bytes;
	};

	static void read_threads(ResourceStats &stats);

	static bool fill_thread_stats(thread_stat_t &thr);

	static bool fill_thread_io(thread_stat_t &thr);

	static void count_thread(const thread_stat_t &ts, ResourceStats &stats);
};
