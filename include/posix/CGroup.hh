#pragma once

#include <filesystem>

#include "ResourceStats.hh"
#include "Logger.hh"
#include "Config.hh"

#if ENABLE_CGROUPS == 0
#error "File is being compiled but cgroups are not enabled"
#endif

class CGroup
{
public:
	CGroup(const std::string &loggername = "cgroup");

	virtual ~CGroup();

	virtual void set_dir(const std::filesystem::path &dir);

	virtual const std::filesystem::path &get_dir() const;

	virtual void update(ResourceStats &stats) const;

	virtual void create();

	virtual void remove();

	virtual void add_pid(unsigned pid);

	std::filesystem::path get_tgids_path() const;

	std::filesystem::path get_pids_path() const;

protected:
	std::filesystem::path m_dir;

	Logger m_logger;

private:
	struct thread_stat_t {
		uint64_t id;

		char state;
		uint64_t utime;
		uint64_t stime;

		uint64_t rchar;
		uint64_t wchar;
		uint64_t read_bytes;
		uint64_t write_bytes;
	};

	void read_threads(ResourceStats &stats) const;

	bool fill_thread_stats(thread_stat_t &thr) const;

	bool fill_thread_io(thread_stat_t &thr) const;

	void count_thread(const thread_stat_t &ts, ResourceStats &stats);

	void kill_all() const;


};
