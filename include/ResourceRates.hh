#pragma once

#include <array>
#include <cstdint>
#include <ostream>

#include "ResourceStats.hh"

class ResourceRates
{
public:
	ResourceRates();

	void reset();

	void push(const ResourceStats &stats);

	double get_rate(ResourceStats::Entry e) const;

	const ResourceStats &get_last() const;

protected:

private:
	static constexpr size_t buffer_size = 10;

	std::array<ResourceStats, buffer_size> m_buffer;

	size_t m_last_id;
};

