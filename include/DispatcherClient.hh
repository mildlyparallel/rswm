#pragma once

#include "Dispatcher.hh"

#include "Worker.hh"
#include "Command.hh"
#include "Logger.hh"

namespace rpc {
class Connection;
}

class DispatcherClient : public Dispatcher
{
public:
	DispatcherClient(std::shared_ptr<rpc::Connection> connection);

	virtual ~DispatcherClient();

	virtual void register_worker(Worker &worker);

	virtual void send_states(const std::vector<std::shared_ptr<Command>> &commands);

	virtual void send_stats(const std::vector<std::shared_ptr<Command>> &commands);

private:
	Logger m_logger;

	std::shared_ptr<rpc::Connection> m_connection;
};
