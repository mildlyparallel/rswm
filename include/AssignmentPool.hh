#pragma once

#include <shared_mutex>
#include <set>
#include <tuple>

#include "Assignment.hh"

class AssignmentPool
{
public:
	AssignmentPool();

	virtual ~AssignmentPool();

	void add(std::shared_ptr<Assignment> a);

	std::tuple<std::shared_ptr<Worker>, std::shared_ptr<Command>>
		find(const std::string &hostname) const;

protected:

private:
	mutable std::shared_mutex m_lock;
	Assignment::id_type m_last_id;
	std::set<std::shared_ptr<Assignment>> m_assignments;

};
