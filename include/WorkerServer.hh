#pragma once

#include "Worker.hh"
#include "Task.hh"
#include "Command.hh"

class Poll;
class CommandPool;
class DispatcherClient;

namespace rpc {
class Runtime;
class Connection;
}

class WorkerServer : public Worker
{
public:
	WorkerServer(
		CommandPool &pool,
		DispatcherClient &dispatcher
	);

	virtual ~WorkerServer();

	// void reset();

	void run_commands();

	// void kill(Command::id_type cid);

	void bind_methods(rpc::Runtime &runtime);

	void set_poll(Poll *poll);

	// TODO: move it to private and call in thru rpc in tests
	bool submit_command(
		std::shared_ptr<rpc::Connection> con,
		const Command &cmd
	);

	void stop(std::shared_ptr<rpc::Connection> con);

	void stop_all_commands(
		std::shared_ptr<rpc::Connection> con,
		bool force
	);

private:
	DispatcherClient &m_dispatcher;

	CommandPool &m_pool;

	Poll *m_poll;
};

