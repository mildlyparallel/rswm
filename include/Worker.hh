#pragma once

#include <cstddef>
#include <vector>
#include <string>
#include <memory>
#include <map>
#include <mutex>
#include <atomic>

// #include "SafeMap.hh"
#include "Logger.hh"
#include "Command.hh"
#include "rpc/Types.hh"

class Worker
{
public:
	using id_type = uint64_t;

// 	using commands_container = SafeMap<Command::id_type, std::shared_ptr<Command> >;
//
// 	using clock_type = std::chrono::steady_clock;
//
// 	using timepoint_type = std::chrono::time_point<clock_type>;

	Worker();

	virtual ~Worker();

	void set_name(const std::string &name);

	const std::string &get_name() const;

	void set_id(id_type id);

	id_type get_id();

	virtual void stop();

	virtual void submit_command(std::shared_ptr<Command> command);

	bool stopped() const;

// 	enum Methods : rpc::MethodName {
// 		Submit,
// 		StopAllSpawned,
// 		Reset,
// 		Kill,
// 		Freeze
// 	};
//
// 	const commands_container &commands() const;
//
// 	commands_container &commands();
//
// 	bool is_active() const;
//
// 	std::lock_guard<std::mutex> lock() const;
//
protected:
	enum Method : rpc::method_name_type {
		StopAllCommands = 1,
		SubmitCommand,
		FreezeCommand,
		Stop,
		// Remove,
		// Heartbeat,
		// HandleCommandFinish,
		// HandleCommandStats,
		// RunRemoteCommand,

		nr_methods
	};

	Logger m_logger;

	std::atomic_bool m_stopped;

// 	commands_container m_commands;
//
private:
	id_type m_id;

	std::string m_name;

// 	std::atomic<timepoint_type> m_last_heartbeat;
//
// 	mutable std::mutex m_state_lock;
};
