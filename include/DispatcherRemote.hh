#pragma once

#include <memory>
#include <map>
#include <thread>
#include <atomic>
#include <set>
#include <fstream>
#include <shared_mutex>

#include "Dispatcher.hh"

#include "WorkerClient.hh"
#include "Assignment.hh"
#include "ResourceStats.hh"
#include "CommandPool.hh"

class Poll;
class WorkerPool;

namespace rpc {
class Runtime;
class Connection;
}

class DispatcherRemote : public Dispatcher
{
public:
	DispatcherRemote(WorkerPool &workers);

	virtual ~DispatcherRemote();

	void bind_methods(rpc::Runtime &runtime);

	std::vector<std::shared_ptr<Worker>> get_workers(size_t n = 0) const;

	virtual std::shared_ptr<Assignment> assign(
		const Task &task,
		const std::vector<std::shared_ptr<Worker>> &workers
	);

protected:
	Worker::id_type register_worker(
		std::shared_ptr<rpc::Connection> con,
		const std::string &name
	);

	void unregister_worker(
		std::shared_ptr<rpc::Connection> con,
		Worker::id_type wid
	);

	bool run_remote_command(
		std::shared_ptr<rpc::Connection> con,
		const std::string &hostname,
		const std::string &cmd
	);

	void submit_next_command(
		std::shared_ptr<Worker> worker,
		std::shared_ptr<Command> command,
		const std::string &shellcmd = ""
	);

	bool update_command_state(
		std::shared_ptr<rpc::Connection> con,
		Command::id_type cid,
		Command::State state,
		int return_code
	);

	void update_command_rates(
		std::shared_ptr<rpc::Connection> con,
		Command::id_type cid,
		const ResourceStats &stats
	);

	void on_disconnect(std::shared_ptr<rpc::Connection> con);

private:
	WorkerPool &m_workers;

	CommandPool m_commands;
};

