#pragma once

#include <map>
#include <functional>
#include <cassert>
#include <future>
#include <chrono>

#include "Poll.hh"

#include "Buffer.hh"
#include "Types.hh"
#include "Exception.hh"
#include "Connection.hh"

namespace rpc {

class Runtime
{
public:
	Runtime();

	virtual ~Runtime();

	template<typename Cl, typename P>
	void on_connection(void (Cl::*func)(std::shared_ptr<Connection>), P ptr);

	void on_connection(std::function<void(std::shared_ptr<Connection>)> cb);

	// TODO: implement this for client?
	template<typename Cl, typename P>
	void on_disconnect(void (Cl::*func)(std::shared_ptr<Connection>), P ptr);

	void on_disconnect(std::function<void(std::shared_ptr<Connection>)> cb);

	template<typename Ret, typename Cl, typename P, typename...Args>
	void bind(method_name_type method, Ret (Cl::*func)(std::shared_ptr<Connection>, Args...), P ptr);

	template<typename Cl, typename P, typename...Args>
	void bind(method_name_type method, void (Cl::*func)(std::shared_ptr<Connection>, Args...), P ptr);

	void notify();

protected:
	static constexpr auto call_timeout = std::chrono::seconds(3);

	static constexpr method_name_type reply_message_name = 0;

	void handle_message(std::shared_ptr<Connection> connection);

	Logger m_logger;

	Poll m_poll;

	std::exception_ptr m_worker_ex;

	std::function<void(std::shared_ptr<Connection>)> m_connection_cb;

	std::function<void(std::shared_ptr<Connection>)> m_disconnect_cb;

private:
	using method_fn = std::function<void (std::shared_ptr<Connection>, reply_id_type, const Buffer &) >;

	using event_fn = std::function<void (std::shared_ptr<Connection>, const Buffer &)>;

	std::map<method_name_type, method_fn> m_methods;

	std::map<method_name_type, event_fn> m_events;
};

template<typename Cl, typename P>
void Runtime::on_connection(void (Cl::*func)(std::shared_ptr<Connection>), P ptr)
{
	m_connection_cb = std::bind(func, ptr, std::placeholders::_1);
}

template<typename Cl, typename P>
void Runtime::on_disconnect(void (Cl::*func)(std::shared_ptr<Connection>), P ptr)
{
	m_disconnect_cb = std::bind(func, ptr, std::placeholders::_1);
}

template<typename Ret, typename Cl, typename P, typename...Args>
void Runtime::bind(method_name_type name, Ret (Cl::*func)(std::shared_ptr<Connection>, Args...), P ptr)
{
	assert(!m_methods.contains(name));

	m_methods[name] = [=, this] (
		std::shared_ptr<Connection> con,
		reply_id_type reply_id,
		const Buffer &rbuf
	) {
		std::tuple< std::decay_t<Args>... > args;
		rbuf >> args;

		auto res = std::apply(func, std::tuple_cat(std::make_tuple(ptr, con), args));

		con->reply(reply_id, res);
	};
}

template<typename Cl, typename P, typename...Args>
void Runtime::bind(method_name_type name, void (Cl::*func)(std::shared_ptr<Connection>, Args...), P ptr)
{
	assert(!m_methods.contains(name));
	m_events[name] = [=, this] (std::shared_ptr<Connection> con, const Buffer &rbuf) {
		std::tuple< std::decay_t<Args>... > args;
		rbuf >> args;

		std::apply(func, std::tuple_cat(std::make_tuple(ptr, con), args));
	};
}

} /* namespace rpc */

