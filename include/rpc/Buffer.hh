#pragma once

#include <string>
#include <vector>
#include <tuple>
#include <array>
#include <iostream>
#include <type_traits>
#include <filesystem>

namespace rpc {

class Buffer
{
public:
	using byte_type = char;

	Buffer();

	Buffer(Buffer &&other);

	Buffer(const Buffer &other);

	virtual ~Buffer();

	byte_type *data();

	const byte_type *data() const;;

	bool empty() const;

	bool grow(size_t n);

	size_t position() const;

	size_t size() const;

	void rewind();

	void store(const void *src, size_t n);

	void load(void *src, size_t n) const;

	void store(const std::string &s);

	void load(std::string &s) const;

	void store(const std::filesystem::path &p);

	void load(std::filesystem::path &p) const;

	template<typename T, std::enable_if_t<std::is_scalar<std::decay_t<T>>::value, int> = 0 >
	void store(const T &v);

	template<typename T, std::enable_if_t<std::is_scalar<std::decay_t<T>>::value, int> = 0 >
	void load(T &v) const;

	template<typename T, std::enable_if_t<std::is_array<std::decay_t<T>>::value, int> = 0 >
	void store(const T &v);

	template<typename T, std::enable_if_t<std::is_array<std::decay_t<T>>::value, int> = 0 >
	void load(T &a) const;

	template<typename T, std::enable_if_t<std::is_empty<std::decay_t<T>>::value, int> = 0 >
	void store(const T &v);

	template<typename T, std::enable_if_t<std::is_empty<std::decay_t<T>>::value, int> = 0 >
	void load(T &a) const;

	template<typename T>
	void store(const std::vector<T> &v);

	template<typename T>
	void load(std::vector<T> &v) const;

	template<typename T, size_t N>
	void store(const std::array<T, N> &a);

	template<typename T, size_t N>
	void load(std::array<T, N> &a) const;

	template<typename... Args>
	void store(const std::tuple<Args...> &t);

	template< typename... Args, size_t... Seq>
	void store_tuple(const std::tuple<Args...> &t, std::index_sequence<Seq...>);

	template<typename... Args>
	void load(std::tuple<Args...> &t) const;

	template< typename... Args, size_t... Seq>
	void load_tuple(std::tuple<Args...> &t, std::index_sequence<Seq...>) const;

	template <typename T>
	Buffer &operator<<(const T &v);

	template <typename T>
	const Buffer &operator>>(T &v) const;

private:
	byte_type *m_data;

	mutable size_t m_position;
	size_t m_size;
};

template<typename T, std::enable_if_t<std::is_scalar<std::decay_t<T>>::value, int> = 0 >
void Buffer::store(const T &v) {
	store(&v, sizeof(T));
}

template<typename T, std::enable_if_t<std::is_scalar<std::decay_t<T>>::value, int> = 0 >
void Buffer::load(T &v) const {
	load(&v, sizeof(T));
}

template<typename T, std::enable_if_t<std::is_array<std::decay_t<T>>::value, int> = 0 >
void Buffer::store(const T &a)
{
	for (size_t i = 0; i < sizeof(T) / sizeof(a[0]); ++i)
		store(a[i]);
}

template<typename T, std::enable_if_t<std::is_array<std::decay_t<T>>::value, int> = 0 >
void Buffer::load(T &a) const
{
	for (size_t i = 0; i < sizeof(T) / sizeof(a[0]); ++i)
		load(a[i]);
}

template<typename T, std::enable_if_t<std::is_empty<std::decay_t<T>>::value, int> = 0 >
void Buffer::store(const T &)
{ }

template<typename T, std::enable_if_t<std::is_empty<std::decay_t<T>>::value, int> = 0 >
void Buffer::load(T &) const
{ }

template<typename T>
void Buffer::store(const std::vector<T> &v)
{
	uint64_t sz = v.size();
	store(sz);

	for (size_t i = 0; i < sz; ++i)
		store(v[i]);
}

template<typename T>
void Buffer::load(std::vector<T> &v) const
{
	uint64_t sz = 0;
	load(sz);

	for (size_t i = 0; i < sz; ++i) {
		T tmp;
		load(tmp);
		v.push_back(tmp);
	}
}

template<typename T, size_t N>
void Buffer::store(const std::array<T, N> &a)
{
	for (size_t i = 0; i < N; ++i)
		store(a[i]);
}

template<typename T, size_t N>
void Buffer::load(std::array<T, N> &a) const
{
	for (size_t i = 0; i < N; ++i)
		load(a[i]);
}

template<typename... Args>
void Buffer::store(const std::tuple<Args...> &t)
{
	store_tuple(t, std::make_index_sequence<sizeof...(Args)>());
}

template<typename... Args, size_t... Seq>
void Buffer::store_tuple(const std::tuple<Args...> &t, std::index_sequence<Seq...>)
{
	(void) (int[]) { ( (*this) << std::get<Seq>(t), 0)... };
}

template<typename... Args>
void Buffer::load(std::tuple<Args...> &t) const
{
	load_tuple(t, std::make_index_sequence<sizeof...(Args)>());
}

template<typename... Args, size_t... Seq>
void Buffer::load_tuple(std::tuple<Args...> &t, std::index_sequence<Seq...>) const
{
	(void) (int[]) { ( (*this) >> std::get<Seq>(t), 0)... };
}

template <typename T>
Buffer &Buffer::operator<<(const T &v)
{
	store(v);
	return *this;
}

template <typename T>
const Buffer &Buffer::operator>>(T &v) const
{
	load(v);
	return *this;
}

std::ostream &operator<<(std::ostream &os, const Buffer &m);

} /* namespace rpc */

