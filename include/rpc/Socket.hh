#pragma once

#include <string>

#include "Logger.hh"

namespace rpc {

class Socket
{
public:
	Socket(int sock = -1);

	Socket(const Socket &other) = delete;

	Socket(Socket &&other);

	Socket &operator=(const Socket &other) = delete;

	Socket &operator=(Socket &&other);

	virtual ~Socket();

	void close();

	size_t recv(void *dst, size_t n, bool dontwait = true);

	void send(const void *src, size_t n);

	void connect(uint16_t port, const std::string &host = "");

	uint16_t get_port() const;

	const std::string &get_host() const;

	bool is_active() const;

	int get_fd() const;

protected:

private:
	static constexpr bool connect_ip4_addresess = true;
	static constexpr bool connect_ip6_addresess = true;

	void getpeername();

	Logger m_logger;

	int m_sock;

	uint16_t m_port;
	std::string m_host;
};

} /* namespace rpc */
