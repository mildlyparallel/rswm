#pragma once

#include <atomic>
#include <string>
#include <thread>
#include <exception>

#include "Runtime.hh"
#include "Connection.hh"

#include "Logger.hh"

namespace rpc {

class Client : public Runtime
{
public:
	Client();

	Client(std::shared_ptr<Connection> connection);

	virtual ~Client();

	std::shared_ptr<Connection> connect(uint16_t port, const std::string &host = "");

	void disconnect();

	bool is_terminated();

protected:

private:
	void process_requests();

	std::shared_ptr<Connection> m_connection;

	std::atomic_bool m_terminated;

	std::thread m_worker;
};

} /* namespace rpc */

