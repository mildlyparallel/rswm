#pragma once

#include <stdexcept>

namespace rpc {

class Exception : public std::runtime_error
{
public:
	enum Type: unsigned {
		Unknown = 0,
		ConnectionClosed,
		UnknownReplyId,
		UnknownMethodName,
		UnknownEventName,
		MessageTooLarge,
		Timeout,

		nr_types
	};

	Exception(Type type = Type::Unknown)
	: std::runtime_error(strings[type])
	{ }

private:
	static constexpr const char *strings[nr_types] = {
		"Unknown",
		"ConnectionClosed",
		"UnknownReplyId",
		"UnknownMethodName",
		"UnknownEventName",
		"MessageTooLarge",
		"Timeout",
	};
};

} /* namespace rpc */

