#pragma once

#include <atomic>
#include <mutex>
#include <condition_variable>
#include <functional>
#include <vector>
#include <thread>
#include <map>
#include <shared_mutex>
#include <cassert>

#include "Buffer.hh"
#include "Socket.hh"
#include "Acceptor.hh"
#include "Connection.hh"
#include "Runtime.hh"

namespace rpc {

class Server : public Runtime
{
public:
	Server();

	virtual ~Server();

	void listen(uint16_t port, const std::string &host = "");

	void stop();

	bool is_terminated();

private:
	std::shared_ptr<Connection> add_new_connection(Socket &&socket);

	std::shared_ptr<Connection> find_connection(size_t con_id);

	void remove_connection(std::shared_ptr<Connection> &connection);

	void start_workers();

	void process_events();

	size_t m_last_connection_id;

	std::atomic_bool m_terminated;

	std::map<uint32_t, std::shared_ptr<Connection> > m_connections;

	std::shared_mutex m_connections_lock;

	std::thread m_worker;

	Acceptor m_acceptor;
};

} /* namespace rpc */
