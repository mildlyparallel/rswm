#pragma once

#include <mutex>
#include <array>
#include <queue>
#include <cassert>
#include <tuple>
#include <future>
#include <map>

#include "Logger.hh"

#include "Socket.hh"
#include "Buffer.hh"
#include "Exception.hh"
#include "Types.hh"


namespace rpc {

class Runtime;

class Connection
{
public:
	Connection(Socket &&socket, Runtime &runtime, size_t id = 0);

	Connection(const Connection &c) = delete;

	Connection(Connection &&c);

	template <typename... Args>
	std::future<Buffer> call(
		method_name_type name,
		Args&&... args
	);

	template<typename... Args>
	Buffer call_and_wait(
		method_name_type name,
		Args&&... args
	);

	template <typename... Args>
	void publish(
		method_name_type name,
		Args&&... args
	);

	template <typename R>
	void reply(reply_id_type id, R data);

	bool recv_message();

	bool send_message();

	bool is_active();

	std::tuple<method_name_type, reply_id_type, const Buffer &>
		get_recieved_message();

	void reset_recived_message();

	void close();

	size_t get_id() const;

private:
	void push_message(std::unique_ptr<Buffer> &&m);

	void handle_recv_message();

	bool handle_reply();

	std::tuple<reply_id_type, std::future<Buffer>> create_next_reply();

	static constexpr size_t max_message_size = 4096;

	Logger m_logger;

	size_t m_id;

	Socket m_socket;

	Runtime &m_runtime;

	size_t m_nread;
	uint32_t m_message_size;

	Buffer m_recv_buffer;
	Buffer m_send_buffer;

	std::mutex m_lock;

	std::mutex m_send_queue_lock;
	std::queue<std::unique_ptr<Buffer>> m_send_queue;

	std::mutex m_replies_lock;

	reply_id_type m_next_reply_id;

	std::map<reply_id_type, std::promise<Buffer>> m_replies;
};

template<typename... Args>
std::future<Buffer> Connection::call(
	method_name_type name,
	Args&&... args
) {
	auto [reply_id, fut] = create_next_reply();

	Buffer sbuf;
	(sbuf << name << reply_id << ... << args);
	auto m = std::make_unique<Buffer>(std::move(sbuf));
	push_message(std::move(m));

	return std::move(fut);
}

template<typename... Args>
Buffer Connection::call_and_wait(
	method_name_type name,
	Args&&... args
) {
	std::future fut = call(name, args...);

	std::future_status st;
	do {
		// TODO: 3sec
		st = fut.wait_for(std::chrono::seconds(3));
		if (st == std::future_status::timeout)
			throw Exception(Exception::Timeout);
	} while (st != std::future_status::ready);

	return fut.get();
}

template<typename... Args>
void Connection::publish(
	method_name_type name,
	Args&&... args
) {
	Buffer sbuf;
	(sbuf << name << static_cast<reply_id_type>(0) << ... << args);
	auto m = std::make_unique<Buffer>(std::move(sbuf));
	push_message(std::move(m));
}


template <typename R>
void Connection::reply(reply_id_type reply_id, R data)
{
	Buffer sbuf;
	sbuf << static_cast<method_name_type>(0);
	sbuf << reply_id;
	sbuf << data;

	auto m = std::make_unique<Buffer>(std::move(sbuf));
	push_message(std::move(m));
}

} /* namespace rpc */

