#pragma once

#include <string>

#include "Logger.hh"

#include "Socket.hh"

namespace rpc {

class Acceptor
{
public:
	Acceptor();

	virtual ~Acceptor();

	void bind(uint16_t port, const std::string &host = "");

	void listen(int backlog = -1);

	Socket accept();

	void close();

	uint16_t get_port() const;

	const std::string &get_host() const;

	int get_fd() const;

	bool is_active() const;

protected:

private:
	void set_nonblocking();

	static constexpr bool bind_ip4_addresess = true;
	static constexpr bool bind_ip6_addresess = true;

	Logger m_logger;

	int m_sock;

	uint16_t m_port;
	std::string m_host;
};

} /* namespace rpc */

