#pragma once

#include <stdexcept>

namespace rpc {

using method_name_type = uint16_t;
using reply_id_type = uint16_t;

} /* namespace rpc */

