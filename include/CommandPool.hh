#pragma once

#include <map>
#include <shared_mutex>
#include <memory>
#include <fstream>

#include "Logger.hh"
#include "Command.hh"

class CommandPool
{
public:
	CommandPool();

	virtual ~CommandPool();

	void add_pending(std::shared_ptr<Command> command);

	void add_with_next_id(std::shared_ptr<Command> command);

	std::shared_ptr<Command> find(Command::id_type cid) const;

	void stop_all(bool force);

	void spawn_pending_commands(std::vector<std::shared_ptr<Command>> &spawned);

	void stop_tardy_commands();

	void sync_finished_commands(std::vector<std::shared_ptr<Command>> &finished);

	void read_command_stats(std::vector<std::shared_ptr<Command>> &updated);

	void read_command_stats();

	size_t size() const;

protected:

private:
	std::shared_ptr<Command> get_next_pending() const;

	bool spawn_next_command();

	Logger m_logger;

	size_t m_nr_spawned;

	mutable std::shared_mutex m_lock;

	Command::id_type m_last_id;

	std::map<Command::id_type, std::shared_ptr<Command>> m_commands;
};
