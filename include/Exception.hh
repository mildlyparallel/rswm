#pragma once

#include <stdexcept>

class Exception : public std::runtime_error
{
public:
	enum Type: unsigned {
		Unknown = 0,
		CommandSubmissionFailed,

		nr_types
	};

	Exception(Type type = Type::Unknown)
	: std::runtime_error(strings[type])
	{ }

private:
	static constexpr const char *strings[nr_types] = {
		"Unknown",
		"CommandSubmissionFailed",
	};
};

