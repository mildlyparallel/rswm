#pragma once

#include <vector>
#include <string>
#include <optional>

#include "Task.hh"
#include "Worker.hh"
#include "Command.hh"

class Assignment
{
public:
	using id_type = size_t;

	using container_type =
		std::vector<std::pair<std::shared_ptr<Worker>, std::shared_ptr<Command>> >;

	Assignment();

	void set_id(id_type aid);

	id_type get_id() const;

	void create(const std::vector< std::shared_ptr<Worker> > &workers);

	void add(std::shared_ptr<Command> command);

	void create_commands(
		const Task &task,
		const std::vector<std::shared_ptr<Worker>> &workers
	);

	bool is_finished() const;

	std::pair<std::shared_ptr<Worker>, std::shared_ptr<Command> >
		find(const std::string &hostname = "") const;

// 	void kill_all();
//
	void freeze(bool freeze);
//
// 	const std::vector<std::shared_ptr<Command>> &get_commands();

	size_t size() const;

	const container_type &commands() const;

private:

	id_type m_id;

	std::vector<std::string> m_hostnames;

	container_type m_commands;

};
