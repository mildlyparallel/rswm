#pragma once

#include <array>
#include <cstdint>
#include <ostream>

#include "rpc/Buffer.hh"

class ResourceStats
{
public:
	using value_type = uint64_t;

	enum Entry : size_t {
		Timestamp = 0,

		Threads,
		RunningThreads,

		UserTime,
		SystemTime,

		WriteChars,
		ReadChars,

		StorageWrite,
		StorageRead,

		Instructions,
		CacheMisses,
		CacheReferences,
		BusAccess,
		BusCycles,
		CpuCycles,
		MemLoad,
		MemStore,
		StallFrontend,
		StallBackend,

		Frozen,

		nr_entries,
		Phony
	};

	using entries_container = std::array<value_type, nr_entries>;

	ResourceStats();

	virtual ~ResourceStats();

	void reset();

	const entries_container &get() const;

	value_type &get(Entry v);

	value_type get(Entry v) const;

	static const char *get_entry_name(Entry v);

protected:

private:
	friend rpc::Buffer &operator<<(rpc::Buffer &buf, const ResourceStats &stats);

	friend const rpc::Buffer &operator>>(const rpc::Buffer &buf, ResourceStats &stats);

	static constexpr const char *m_entry_names[ResourceStats::Entry::nr_entries] = {
		"Timestamp",

		"Threads",
		"RunningThreads",

		"UserTime",
		"SystemTime",

		"WriteChars",
		"ReadChars",

		"StorageWrite",
		"StorageRead",

		"Instructions",
		"CacheMisses",
		"CacheReferences",
		"BusAccess",
		"BusCycles",
		"CpuCycles",
		"MemLoad",
		"MemStore",
		"StallFrontend",
		"StallBackend",

		"Frozen"
	};

	entries_container m_values;
};

rpc::Buffer &operator<<(rpc::Buffer &buf, const ResourceStats &stats);

rpc::Buffer &operator>>(rpc::Buffer &buf, ResourceStats &stats);

std::ostream &operator<<(std::ostream &os, const ResourceStats &stats);
