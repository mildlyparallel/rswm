#pragma once

#include "Command.hh"
#include "ResourceStats.hh"

class CommandStub : public Command
{
public:
	CommandStub();

	CommandStub(const Task &other);

	// CommandStub(const Task &other, id_type cid);

	virtual ~CommandStub();

	virtual void spawn() final;

	virtual bool sync() final;

	virtual void stop(bool force = false) final;

	virtual void read_stats() final;

	virtual void freeze(bool freeze) final;
};
