#pragma once

#include <vector>
#include <tuple>
#include <filesystem>

#include "Task.hh"

class TaskGraph
{
public:
	using task_id = uint64_t;

	struct node_type {
		task_id id;
		task_id prev_id;
		Task task;
	};
	
	using task_container = std::vector<node_type>;

	using iterator = task_container::iterator;

	using const_iterator = task_container::const_iterator;

	TaskGraph();

	virtual ~TaskGraph();

	void read(const std::filesystem::path &file);

	task_id add(const Task &task);

	task_id add_next(const Task &task);

	task_id add_after(task_id prev, const Task &task);

	const Task &at(task_id i) const;

	Task &at(task_id i);

	const node_type &node_at(task_id i) const;

	void clear();

	size_t size() const;

	bool empty() const;

	iterator begin();

	iterator end();

	const_iterator cbegin() const;

	const_iterator cend() const;

	void set_name(const std::string &name);

	const std::string &get_name() const;

protected:

private:
	task_container m_tasks; 

	std::string m_name;
};
