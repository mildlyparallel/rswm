#pragma once

#include <optional>

#include "Config.hh"
#include "Task.hh"

class Interpolator
{
public:
	Interpolator();

	Interpolator(size_t task_id, size_t nr_tasks);

	void set_task_id(size_t task_id);

	void set_nr_tasks(size_t nr_tasks);

	std::pair<double, double> get(const Taskset &ts) const;

	void add(const Taskset &ts, double val);

	void flush();

	void flush(size_t task_id);

	void simplify();

protected:

private:
	struct Bound {
		Taskset ts;
		double val = 0;
	};

	struct Estimate {
		double mean = 0.5;
		double delta = 0.5;

		Taskset sup;
		Taskset sub;
	};

	Estimate estimate(const Taskset &ts) const;

	Estimate estimate(
		const Taskset &ts,
		const Bound &sub,
		const Bound &sup
	) const;

	static double interploate_liniear(
		double x,
		double x0,
		double x1,
		double y0,
		double y1
	);

	std::optional<Bound> find(const Taskset &ts) const;

	size_t find_id(const Taskset &ts) const;

	size_t m_task_id = 0;

	size_t m_nr_tasks = 0;

	std::vector<Bound> m_data;

	// TODO: get from config
	double m_min_error = 0.1;
};

