#pragma once

#include <string>
#include <vector>
#include <chrono>
#include <tuple>
#include <chrono>
#include <filesystem>

#include "Config.hh"
#include "Bitset.hh"
#include "rpc/Buffer.hh"

using Taskset = Bitset<Config::max_comb_size>;

class Task
{
public:
	Task();

	virtual ~Task();

	void set_name(const std::string &name);
	const std::string &get_name() const;

	void set_file(const std::filesystem::path &p);
	const std::filesystem::path &get_file() const;

	void set_workdir(const std::filesystem::path &workdir);
	const std::filesystem::path &get_workdir() const;

	template <typename... T>
	void set_args(T&& ...args);
	template <typename... T>
	void add_args(T&& ...args);
	void add_arg(const std::string &arg);
	void set_args(const std::vector<std::string> &args);
	const std::vector<std::string> &get_args() const;

	void set_shell_cmd(const std::string &s);

	void set_envs(const std::vector<std::string> &env);
	void add_env(const std::string &env);
	void add_env(const std::string &k, const std::string &v);
	const std::vector<std::string> &get_envs() const;

	void set_inherit_env(bool inherit = true);
	bool get_inherit_env() const;

	void set_timeout_soft(std::chrono::seconds s);
	std::chrono::seconds get_timeout_soft() const;

	void set_timeout_hard(std::chrono::seconds s);
	std::chrono::seconds get_timeout_hard() const;

	void set_stdout_log(const std::string &file);
	const std::string &get_stdout_log() const;

	void set_stderr_log(const std::string &file);
	const std::string &get_stderr_log() const;

	void set_redirect_err_to_out(bool redirect = true);
	bool get_redirect_err_to_out() const;

	void set_nr_nodes(size_t nr_nodes);
	size_t get_nr_nodes() const;

	void set_nr_threads(size_t n);
	size_t get_nr_threads() const;

	void set_nr_cores(size_t n);
	size_t get_nr_cores() const;

	void set_nr_sockets(size_t n);
	size_t get_nr_sockets() const;

	void set_priority(unsigned value);
	unsigned get_priority() const;

	void set_cpulist(std::vector<int> cpulist);
	const std::vector<int> &get_cpulist() const;

private:
	friend rpc::Buffer &operator<<(rpc::Buffer &msg, const Task &err);

	friend const rpc::Buffer &operator>>(const rpc::Buffer &msg, Task &err);

	std::string m_name;

	std::filesystem::path m_file;
	std::filesystem::path m_workdir;
	std::vector<std::string> m_args;
	std::vector<std::string> m_envs;
	bool m_inherit_env;

	std::chrono::seconds m_timeout_soft;
	std::chrono::seconds m_timeout_hard;

	std::string m_stdout_log;
	std::string m_stderr_log;

	bool m_redirect_err_to_out;

	size_t m_nr_nodes;
	size_t m_nr_threads;
	size_t m_nr_cores;
	size_t m_nr_sockets;

	unsigned m_priority;

	std::vector<int> m_cpulist;
};

template <typename... T>
void Task::set_args(T&& ...args)
{
	m_args = {args...};
}

template <typename... T>
void Task::add_args(T&& ...args)
{
	(void) (int[]) { (add_arg(args), 0)... };
}

rpc::Buffer &operator<<(rpc::Buffer &msg, const Task &task);

const rpc::Buffer &operator>>(const rpc::Buffer &msg, Task &task);

std::ostream &operator<<(std::ostream &os, const Task &task);

