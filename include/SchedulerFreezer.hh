#pragma once

#include <map>
#include <tuple>
#include <fstream>

#include "Assignment.hh"
#include "Scheduler.hh"
#include "Interpolator.hh"

class Worker;

class SchedulerFreezer : public Scheduler
{
public:
	SchedulerFreezer(WorkerPool &workers, Dispatcher &dispatcher);

	virtual ~SchedulerFreezer();

	virtual Result run(const TaskGraph &graph);

	void set_max_accel_delta(double v);

	void set_ucb_coeff(double v);

	void set_fairness_coeff(double v);

	void set_efficiency_coeff(double v);

protected:

private:
	static constexpr std::chrono::seconds time_unit = std::chrono::seconds(8);
	static constexpr std::chrono::seconds measure_unit = std::chrono::seconds(3);

	double m_max_accel_delta = 0.1;
	double m_ucb_coeff = 1;
	double m_fairness_coeff = 0;
	double m_efficiency_coeff = 0;
	size_t m_units_until_resample = 50;

	struct Active {
		using id_type = size_t;

		std::shared_ptr<Assignment> assignment;
		Interpolator interpolator;
		size_t id = 0;
		bool active = false;
		TaskGraph::task_id task_id;
		bool running = false;
		bool resample = false;
		bool flush = false;
		double accel_sum = 1e-5;
		size_t runtime_since_base = 0;

		double current_speed;
		std::optional<double> base_speed;
		std::optional<double> base_speed_old;
		std::optional<double> start_speed;

		uint64_t cum_inst = 0;
		uint64_t cum_cycl = 0;
		uint64_t cum_stime = 0;
		uint64_t cum_utime = 0;

		void reset() {
			interpolator.flush();
			base_speed.reset();
			start_speed.reset();

			accel_sum = 1e-5;
			cum_inst = 0;
			cum_cycl = 0;
			cum_stime = 0;
			cum_utime = 0;
		}
	};

	double get_jian() const;
	std::set<TaskGraph::task_id> find_schedulable(const TaskGraph &graph) const;

	Active::id_type find_active(TaskGraph::task_id task_id) const;

	double get_task_speed(
		Active::id_type aid,
		double dt
	);

	void add_to_active(const std::set<TaskGraph::task_id> &schedulable);

	std::optional<TaskGraph::task_id> find_not_submitted() const;

	Result submit(const TaskGraph &graph, TaskGraph::task_id tid);

	Taskset find_next_combination() const;

	double evaluate(const Taskset &ts) const;

	void run_next_combination(const Taskset &current, const Taskset &comb);

	Taskset get_running();

	Result update_running(Taskset running);

	void freeze_all();

	void freeze_all_except(Active::id_type aid);

	Result submit_stale(const TaskGraph &graph);

	Result submit_resampled(const TaskGraph &graph);

	bool update_changed();

	void update_cum_sum(Active::id_type i);

	void update_active_finished();

	std::array<Active, Config::max_comb_size> m_active;

	timepoint_type m_active_started_at;

	timepoint_type m_measured_at;

	timepoint_type m_init_at;

	std::ofstream m_trace;

	std::ofstream m_trace_glob;
};
