#pragma once

#include <map>
#include <shared_mutex>
#include <memory>

#include "Worker.hh"
#include "Logger.hh"

class WorkerPool
{
public:
	WorkerPool();

	virtual ~WorkerPool();

	Worker::id_type add(std::shared_ptr<Worker> worker);

	void remove(Worker::id_type id);

	std::shared_ptr<Worker> find(Worker::id_type id) const;

	std::vector<std::shared_ptr<Worker>> get(size_t n = 0) const;

	size_t size() const;

	void stop_all();

protected:

private:
	void add_with_name(std::shared_ptr<Worker> worker);

	void add_without_name(std::shared_ptr<Worker> worker);

	Logger m_logger;

	mutable std::shared_mutex m_lock;

	Worker::id_type m_last_id;

	std::map<Worker::id_type, std::shared_ptr<Worker>> m_workers; 
};
