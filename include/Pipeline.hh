#pragma once

#include <fstream>

#include "TaskGraph.hh"
#include "Logger.hh"

class Command;

class Pipeline
{
public:
	Pipeline();

	virtual ~Pipeline();

	void run(const TaskGraph &graph);

	void run_remote(const TaskGraph &graph);

	void write_profile(const std::filesystem::path &p);

	void set_max_accel_delta(double v);

	void set_ucb_coeff(double v);

	void set_fairness_coeff(double v);

	void set_efficiency_coeff(double v);

protected:

private:
	Logger m_logger;

	std::ofstream m_stat_file;

	void write_counters(const std::vector<std::shared_ptr<Command>> &commands);

	double m_max_accel_delta = 0.1;
	double m_ucb_coeff = 1;
	double m_fairness_coeff = 1;
	double m_efficiency_coeff = 0;

};
