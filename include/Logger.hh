#pragma once

#include <iostream>
#include <iomanip>
#include <vector>
#include <tuple>

class Logger
{
public:
	Logger(const std::string &scope = std::string());

	virtual ~Logger();

	void configure_from_env();

	void set_scope(const std::string &scope);

	const std::string &get_scope() const;

	void set_enabled(bool en);

	bool is_enabled() const;

	enum Level: size_t {
		Debug = 0,
		Info,
		Warning,
		Error,

		nr_levels
	};

	template <typename... T>
	void error(T&&... args) const;

	template <typename... T>
	void warning(T&&... args) const;

	template <typename... T>
	void info(T&&... args) const;

	template <typename... T>
	void debug(T&&... args) const;

	Level get_level() const;
	void set_level(Level level);

protected:

private:
	template <typename... T>
	void log(Level level, T&&... args) const;

	static constexpr const char *m_level_str[nr_levels] = {
		"debug",
		"info",
		"warning",
		"error",
	};

	static constexpr const char *scope_var_name = "LOG_SCOPE";
	static constexpr const char *level_var_name = "LOG_LEVEL";

	static bool is_set_in_env(const std::string &scope);

	void set_level_from_env();

	bool m_enabled;

	std::string m_scope;

	Level m_level;
};

template <typename T>
std::ostream &print_container(std::ostream &out, const T &v) {
	out << "{";
	bool comma = false;

	const size_t max_el = 5;

	size_t sz = std::min(v.size(), max_el);
	for (size_t i = 0; i < sz; ++i) {
		if (comma)
			out << ", ";
		out << v[i];
		comma = true;
	}

	if (v.size() > max_el)
		out << ", ... (" << v.size() << ")";

	out << "}";
	return out;
}

template<typename... T, size_t... S>
std::ostream &print_tuple(
	std::ostream &out,
	const std::tuple<T...> &t,
	std::index_sequence<S...>
) {
	out << "(";

	int dummy[] = {
		(out << std::get<S>(t) << ((S + 1) == sizeof...(S) ? "" : ", "), 0)...
	};

	(void) dummy;

	out << ")";

	return out;
}

template <typename T>
std::ostream &operator <<(std::ostream &out, const std::vector<T> &v) {
	return print_container(out, v);
}

template <typename T, size_t N>
std::ostream &operator <<(std::ostream &out, const std::array<T, N> &v) {
	return print_container(out, v);
}

template <typename... T>
std::ostream &operator <<(std::ostream &out, const std::tuple<T...> &t) {
	return print_tuple(out, t, std::make_index_sequence<sizeof...(T)>());
}

template <typename... T>
void Logger::log(Logger::Level level, T&&... args) const
{
	if (level != Error && (level != Warning || m_level != Warning)) {
		if (!m_enabled)
			return;
		if (level < m_level)
			return;
	}

	// TODO use std::osyncstream when it's implementd

	auto t = std::time(nullptr);
	auto tm = *std::localtime(&t);

	std::stringstream ss;
	ss << "[" << std::put_time(&tm, "%T") << "]";

	if (!m_scope.empty())
		ss << "[" << m_scope << "]";

	ss << "[" << m_level_str[level] << "] ";

	(void) (int[]) { (ss << args << " ", 0)...  };

	std::clog << ss.str() << std::endl;
}

template <typename... T>
void Logger::error(T&&... args) const
{
	log(Error, args...);
}

template <typename... T>
void Logger::warning(T&&... args) const
{
	log(Warning, args...);
}

template <typename... T>
void Logger::info(T&&... args) const
{
	log(Info, args...);
}

template <typename... T>
void Logger::debug(T&&... args) const
{
	log(Debug, args...);
}
