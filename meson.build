project(
	'Resource Sharing Workload Manager (rswm)',
	'cpp',
	version: '1.0.0',
	default_options : [
	  'buildtype=plain',
	  'cpp_std=gnu++2a'
	]
)

config = configuration_data()
config.set('version', meson.project_version())

rswm_targets = [
	['rswm-worker', 'src/rswm-worker.cc'],
	['rswm-rsh', 'src/rswm-rsh.cc']
]

rswm_sources = [
	'src/Config.cc',
	'src/Dispatcher.cc',
	'src/DispatcherRemote.cc',
	'src/DispatcherLocal.cc',
	'src/DispatcherClient.cc',
	'src/Task.cc',
	'src/TaskGraph.cc',
	'src/Scheduler.cc',
	'src/SchedulerLifo.cc',
	'src/SchedulerFreezer.cc',
	'src/Command.cc',
	'src/CommandStub.cc',
	'src/Assignment.cc',
	'src/Worker.cc',
	'src/WorkerClient.cc',
	'src/WorkerServer.cc',
	'src/ResourceStats.cc',
	'src/ResourceRates.cc',
	'src/Logger.cc',
	'src/Poll.cc',
	'src/Utils.cc',
	'src/CommandPool.cc',
	'src/WorkerPool.cc',
	'src/AssignmentPool.cc',
	'src/Pipeline.cc',
	'src/Interpolator.cc',

	'src/posix/CommandPosix.cc',
	'src/posix/ProcFS.cc',
	'src/posix/Topology.cc',

	'src/rpc/Connection.cc',
	'src/rpc/Socket.cc',
	'src/rpc/Acceptor.cc',
	'src/rpc/Server.cc',
	'src/rpc/Client.cc',
	'src/rpc/Buffer.cc',
	'src/rpc/Runtime.cc',
]

if get_option('enable-cgroups')
	rswm_sources += [
		'src/posix/CGroup.cc',
		'src/posix/CGroupFreezer.cc',
	]

	config.set('ENABLE_CGROUPS', 1)

	cg_freezer_root = get_option('cgroup-freezer-root')
	message('CGroup freezer root: ' + cg_freezer_root)
	config.set_quoted('CGROUP_FREEZER_ROOT', cg_freezer_root)
else
	config.set('ENABLE_CGROUPS', 0)
	message('CGroups are disabled')
endif

if get_option('enable-perf')
	rswm_sources += [
		'src/posix/Perf.cc',
	]

	config.set('ENABLE_PERF', 1)
	message('Perf is enabled')
else
	config.set('ENABLE_PERF', 0)
	message('Perf is disabled')
endif

config.set('MAX_COMBINATION_SIZE', get_option('max-combination-size'))
# message('Max combination size:', config.get('MAX_COMBINATION_SIZE'))

incdir = [
	include_directories('include')
]

cpp = meson.get_compiler('cpp')

test_flags = [  ]

if get_option('buildtype') == 'release'
	test_flags += '-DNDEBUG=1'
endif

cflags = [ ]

foreach flag : test_flags
	if cpp.has_argument(flag)
		cflags += flag
	endif
endforeach

deps = [ 
	dependency('threads')
]

dashh_dep = dependency('dashh', required : false)

if not dashh_dep.found()
	dashh_subporject = subproject('dashh',
		default_options : [
			'build-as-subproject=true',
		]
	)
	dashh_dep = dashh_subporject.get_variable('dashh_dep')
endif

deps += dashh_dep

test_functions = [
# 	'sincos'
]

foreach func : test_functions
	if cpp.has_function(func)
		config.set('HAVE_' + func.underscorify().to_upper(), true)
	endif
endforeach

configure_file(
	input : 'config.h.in',
	output : 'config.h',
	configuration : config
)

rswm_library = static_library(
	'rswm',
	sources : rswm_sources,
	include_directories : incdir,
	dependencies : deps,
	c_args : cflags
)

foreach t : rswm_targets
	executable(
		t[0],
		sources : t[1],
		include_directories : incdir,
		link_with : rswm_library,
		dependencies : deps,
		cpp_args : cflags,
		install: true
	)
endforeach

subdir('wrapper/c')
subdir('examples')

# if get_option('buildtype').startswith('debug')
# 	subdir('tests')
# endif
