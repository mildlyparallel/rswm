#include <cassert>

#include "DispatcherClient.hh"
#include "rpc/Connection.hh"

DispatcherClient::DispatcherClient(std::shared_ptr<rpc::Connection> connection)
: m_logger("dispatcher")
, m_connection(connection)
{
	assert(connection);
}

DispatcherClient::~DispatcherClient()
{ }

void DispatcherClient::register_worker(Worker &worker)
{
	Worker::id_type wid;

	m_logger.debug("Registering worker with name", worker.get_name());

	m_connection->call_and_wait(Dispatcher::RegisterWorker, worker.get_name()) >> wid;

	worker.set_id(wid);

	m_logger.debug("Registered worker, id", wid);
}

void DispatcherClient::send_states(const std::vector<std::shared_ptr<Command>> &commands)
{
	for (auto &cmd : commands) {
		assert(cmd);

		// TODO: batch send

		bool ok = false;
		m_connection->call_and_wait(Dispatcher::UpdateCommandState,
			cmd->get_id(),
			cmd->get_state(),
			cmd->get_return_code()
		) >> ok;

		// TODO: handle errors
		assert(ok);
	}
}

void DispatcherClient::send_stats(const std::vector<std::shared_ptr<Command>> &commands)
{
	for (auto &cmd : commands) {
		assert(cmd);
		m_connection->publish(Dispatcher::UpdateCommandRates, cmd->get_id(), cmd->get_stats());
	}
}
