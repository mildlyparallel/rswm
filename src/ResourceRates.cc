#include "ResourceRates.hh"

ResourceRates::ResourceRates()
{
	reset();
}

void ResourceRates::reset()
{
	for (auto &r : m_buffer)
		r.reset();
	m_last_id = 0;
}

void ResourceRates::push(const ResourceStats &stats)
{
	m_buffer[m_last_id] = stats;

	m_last_id++;
	if (m_last_id == buffer_size)
		m_last_id = 0;
}

const ResourceStats &ResourceRates::get_last() const
{
	size_t i;
	if (m_last_id == 0)
		i = buffer_size - 1;
	else
		i = m_last_id - 1;

	return m_buffer[i];
}

double ResourceRates::get_rate(ResourceStats::Entry e) const
{
	size_t i = m_last_id;

	i = (++i == buffer_size) ? 0 : i;

	ResourceStats::value_type t0 = m_buffer[i].get(ResourceStats::Timestamp);
	ResourceStats::value_type v0 = m_buffer[i].get(e);

	double avg = 0;
	size_t n = 0;

	for (size_t j = 0; j < buffer_size; ++j) {

		i = (++i == buffer_size) ? 0 : i;

		ResourceStats::value_type t1 = m_buffer[i].get(ResourceStats::Timestamp);
		ResourceStats::value_type v1 = m_buffer[i].get(e);

		if (t1 > t0 && v1 > v0) {
			double dt = 1e-3 * (t1 - t0);
			double dv = 1. * (v1 - v0);
			avg += dv / dt;
			n++;
		}

		t0 = t1;
		v0 = v1;
	}

	if (n == 0)
		return 0;

	avg /= n;

	return avg;
}

