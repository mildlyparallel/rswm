#include <cassert>
#include <iostream>

#include "WorkerClient.hh"

#include "rpc/Connection.hh"
#include "Exception.hh"

WorkerClient::WorkerClient(std::shared_ptr<rpc::Connection> connection)
: m_connection(connection)
{
	assert(connection);
}

WorkerClient::~WorkerClient()
{ }

void WorkerClient::submit_command(std::shared_ptr<Command> command)
{
	Task &task = *command;

	m_logger.debug("Call submit_command():", task);

	bool ok = false;

	m_connection->call_and_wait(Worker::SubmitCommand, *command) >> ok;

	if (ok)
		return;

	m_logger.error("Worker did not accept task", task);
	m_connection->close();
	throw Exception(Exception::CommandSubmissionFailed);
}

void WorkerClient::stop_all_commands(bool hard)
{
	m_logger.debug("Publish stop_all_commands(", hard, ")");

	m_connection->publish(StopAllCommands, hard);

	// auto lg = m_commands.lock();
    //
	// for (auto &[cid, cmd] : m_commands) {
	// 	if (cmd->get_state() == Command::Spawned)
	// 		cmd->set_state(Command::Terminated);
	// }
}

// void WorkerClient::reset()
// {
// 	m_logger.debug("Publish reset()");
// 	m_connection->publish(Reset);
// }

// void WorkerClient::kill(Command::id_type cid)
// {
// 	m_logger.debug("Publish kill(", cid, ")");
// 	m_connection->publish(Kill, cid);
// }

void WorkerClient::freeze_command(Command::id_type cid, bool freeze)
{
	m_logger.debug("Publish freeze_command(", cid, freeze, ")");
	m_connection->publish(FreezeCommand, cid, freeze);
}

void WorkerClient::stop()
{
	m_logger.debug("Publish stop()");
	m_connection->publish(Stop);
}
