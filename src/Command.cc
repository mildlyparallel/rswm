#include <iostream>
#include <cassert>
#include <mutex>

#include "Command.hh"

Command::Command()
: m_logger("command")
, m_id(0)
, m_state(Empty)
, m_return_code(0)
{ }

Command::Command(const Task &task)
: Task(task)
, m_logger("command")
, m_id(0)
, m_state(Empty)
, m_return_code(0)
{ }

Command::Command(const Command &cmd)
: Task(cmd)
, m_logger("command")
, m_id(cmd.m_id)
, m_state(cmd.m_state)
, m_return_code(cmd.m_return_code)
, m_hostnames(cmd.m_hostnames)
, m_hostname(cmd.m_hostname)
{ }

Command::~Command()
{ }

void Command::set_id(Command::id_type id)
{
	assert(id > 0);
	std::unique_lock lg(m_state_lock);
	m_id = id;
}

Command::id_type Command::get_id() const
{
	std::shared_lock lg(m_state_lock);
	return m_id;
}

Command::timepoint_type Command::get_state_update_tp() const
{
	std::shared_lock lg(m_state_lock);
	return m_state_update_tp;
}

void Command::set_return_code(int rc)
{
	std::unique_lock lg(m_state_lock);
	m_return_code = rc;
}

int Command::get_return_code() const
{
	std::shared_lock lg(m_state_lock);
	return m_return_code;
}

void Command::set_state(Command::State state)
{
	std::unique_lock lg(m_state_lock);

	if (m_state == state)
		return;

	m_state_update_tp = clock_type::now();

	m_state = state;
}

Command::State Command::get_state() const
{
	std::shared_lock lg(m_state_lock);
	return m_state;
}

bool Command::finished() const
{
	std::shared_lock lg(m_state_lock);
	return m_state == Exited || m_state == Terminated || m_state == Failed;
}

const ResourceStats &Command::get_stats() const
{
	std::shared_lock lg(m_state_lock);
	return m_rates.get_last();
}

void Command::update_rates(const ResourceStats &stats)
{
	std::unique_lock lg(m_state_lock);
	m_rates.push(stats);
}

const ResourceRates &Command::get_rates() const
{
	std::shared_lock lg(m_state_lock);
	return m_rates;
}

void Command::set_hostnames(const std::vector<std::string> &hosts)
{
	assert(!hosts.empty());
	m_hostnames = hosts;
}

const std::vector<std::string> &Command::get_hostnames() const
{
	return m_hostnames;
}

void Command::set_hostname(const std::string &host)
{
	assert(!host.empty());
	m_hostname = host;
}

const std::string &Command::get_hostname() const
{
	return m_hostname;
}

void Command::set_worker(std::shared_ptr<Worker> worker)
{
	m_worker = worker;
}

std::shared_ptr<Worker> Command::get_worker() const
{
	return m_worker;
}

rpc::Buffer &operator<<(rpc::Buffer &msg, const Command &cmd) {
	const Task &task = cmd;
	msg << task;
	msg << cmd.m_id;
	msg << cmd.m_hostnames;
	msg << cmd.m_hostname;

	return msg;
}

const rpc::Buffer &operator>>(const rpc::Buffer &msg, Command &cmd) {
	Task &task = cmd;
	msg >> task;
	msg >> cmd.m_id;
	msg >> cmd.m_hostnames;
	msg >> cmd.m_hostname;

	return msg;
}

void Command::spawn()
{
	assert(false);
}

void Command::stop(bool /*force*/)
{
	assert(false);
}

bool Command::sync()
{
	assert(false);
}

void Command::read_stats()
{
	assert(false);
}

void Command::freeze(bool /*freeze*/)
{
	assert(false);
}

std::ostream &operator<<(std::ostream &os, Command::State s)
{
	size_t i = static_cast<size_t>(s);
	assert(i < Command::nr_states);
	os << "Command::" << Command::state_str[i];
	return os;
}

std::ostream &operator<<(std::ostream &os, const Command &cmd)
{
	os << "Command(";
	os << "id:" << cmd.get_id();
	os << "; state:" << cmd.get_state();
	os << "; rc:" << cmd.get_return_code();
	os << ")";

	return os;
}

