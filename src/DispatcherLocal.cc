#include <cassert>

#include "DispatcherLocal.hh"
#include "CommandPool.hh"
#include "Assignment.hh"
#include "posix/CommandPosix.hh"

DispatcherLocal::DispatcherLocal(CommandPool &commands)
: m_commands(commands)
{  }

DispatcherLocal::~DispatcherLocal()
{  }

std::shared_ptr<Assignment> DispatcherLocal::assign(
	const Task &task,
	const std::vector<std::shared_ptr<Worker>> &workers
) {
	assert(workers.size() == 1);

	auto command = std::make_shared<CommandPosix>(task);
	command->set_state(Command::Pending);
	m_commands.add_with_next_id(command);

	auto assignment = std::make_shared<Assignment>();
	m_assignments.add(assignment);

	assignment->create(workers);
	assignment->add(command);

	m_logger.debug("Created assignment ", assignment->get_id());

	return assignment;
}
