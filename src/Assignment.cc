#include <cassert>

#include "Assignment.hh"
#include "CommandStub.hh"



Assignment::Assignment()
: m_id(0)
{  }

void Assignment::set_id(id_type aid)
{
	m_id = aid;
}

Assignment::id_type Assignment::get_id() const
{
	return m_id;
}

void Assignment::create(const std::vector< std::shared_ptr<Worker> > &workers)
{
	assert(!workers.empty());
	assert(m_hostnames.empty());

	for (auto &w : workers) {
		assert(w);

		std::stringstream hostname;
		hostname << "host-" << m_id << "-" << w->get_id() << "-" << m_hostnames.size();
		m_hostnames.push_back(hostname.str());

		m_commands.push_back({w, nullptr});
	}
}

void Assignment::add(
	std::shared_ptr<Command> command
) {
	assert(command);
	assert(m_hostnames.size() == m_commands.size());

	command->set_hostnames(m_hostnames);

	size_t i = 0;

	for (auto &[wrk, cmd] : m_commands) {
		if (!cmd) {
			command->set_hostname(m_hostnames[i]);
			command->set_worker(wrk);
			cmd = command;
			return;
		}
		i++;
	}

	assert(false);
}

void Assignment::create_commands(
	const Task &task,
	const std::vector<std::shared_ptr<Worker>> &workers
) {
	assert(!workers.empty());
	assert(m_hostnames.empty());
	assert(m_commands.empty());

	for (auto &w : workers) {
		assert(w);

		std::stringstream hostname;
		hostname << "host-" << m_id << "-" << w->get_id() << "-" << m_hostnames.size();
		m_hostnames.push_back(hostname.str());

		auto cmd = std::make_shared<CommandStub>(task);
		cmd->set_hostname(hostname.str());

		m_commands.push_back({w, cmd});
	}

	for (auto [w, c] : m_commands)
		c->set_hostnames(m_hostnames);
}

std::pair<std::shared_ptr<Worker>, std::shared_ptr<Command> >
Assignment::find(const std::string &hostname) const
{
	if (m_commands.empty())
		return {nullptr, nullptr};
	if (hostname.empty())
		return m_commands.front();

	for (auto [w, c] : m_commands) {
		if (c->get_hostname() == hostname)
			return {w, c};
	}

	return {nullptr, nullptr};
}

bool Assignment::is_finished() const
{
	for (auto [w, c] : m_commands) {
		assert(c);

		if (!c->finished())
			return false;
	}

	return true;
}

// void Assignment::kill_all()
// {
// 	assert(!m_hostnames.empty());
// 	assert(m_hostnames.size() == m_workers.size());
// 	assert(m_hostnames.size() == m_commands.size());
//
// 	for (size_t i = 0; i < m_workers.size(); ++i) {
// 		if (!m_commands[i])
// 			continue;
//
// 		if (m_commands[i]->safe_finished())
// 			continue;
//
// 		m_workers[i]->kill(m_commands[i]->get_id());
// 		
// 	}
// }
//
void Assignment::freeze(bool freeze)
{
	// assert(!m_hostnames.empty());
	// assert(m_hostnames.size() == m_commands.size());

	for (auto [worker, command] : m_commands) {
		command->freeze(freeze);
		// worker->freeze(commnad->get_id(), freeze);
	}
}

// const std::vector<std::shared_ptr<Command>> &Assignment::get_commands()
// {
// 	return m_commands;
// }

size_t Assignment::size() const
{
	return m_hostnames.size();
}

const Assignment::container_type &Assignment::commands() const
{
	return m_commands;
}
