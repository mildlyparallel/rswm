#include <iostream>
#include <cassert>

#include "ResourceStats.hh"

ResourceStats::ResourceStats()
{
	reset();
}

ResourceStats::~ResourceStats()
{  }

const ResourceStats::entries_container &ResourceStats::get() const
{
	return m_values;
}

ResourceStats::value_type ResourceStats::get(Entry v) const
{
	size_t i = static_cast<size_t>(v);
	assert(i < Entry::nr_entries);

	return m_values[i];
}

ResourceStats::value_type &ResourceStats::get(Entry v)
{
	size_t i = static_cast<size_t>(v);
	assert(i < Entry::nr_entries);

	return m_values[i];
}

void ResourceStats::reset()
{
	std::fill(m_values.begin(), m_values.end(), 0);
}

rpc::Buffer &operator<<(rpc::Buffer &buf, const ResourceStats &stats)
{
	buf << stats.m_values;
	return buf;
}

const rpc::Buffer &operator>>(const rpc::Buffer &buf, ResourceStats &stats)
{
	buf >> stats.m_values;
	return buf;
}

std::ostream &operator<<(std::ostream &os, const ResourceStats &stats)
{
	size_t n = 0;
	for (auto &v : stats.get()) {
		if (v > 0)
			n++;
	}

	os << "ResourceStats(" << n << ")";

	return os;
}

const char *ResourceStats::get_entry_name(Entry entry) {
	size_t i = static_cast<size_t>(entry);
	assert(i < Entry::nr_entries);
	return m_entry_names[i];
}

