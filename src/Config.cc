#include <cassert>

#include <Dashh.hh>

#include "Config.hh"

void Config::parse_cmdline(int argc, const char *argv[])
{
	using namespace dashh;

	std::string cmd_str;

	Command cmd;

	cmd << Text("rswm -- Resource Sharing Workload Manager");

	cmd << Text("\nUsage:");
	cmd << Usage("[-h] [command [options]]");
	cmd << "\nCommands:";

	Command cmd_rl(cmd_str, "l|run-local", "Starts execution of specified task at local node");
	cmd_rl << Text("Usage:");
	cmd_rl << Usage("graph [args...]");
	cmd_rl << "\nOptions:";
	cmd_rl << Positional(as_readable(graph_script), "graph", "Lua script defining task graph", Type::Required);
	cmd_rl << Positional(graph_script_args, "args...", "Arguments for graph script", Type::Greedy);
	cmd_rl << Flag(PageHelp(), "-h, -?, --help", "Print help (this message) and exit");
	cmd << cmd_rl;

	Command cmd_rr(cmd_str, "r|run-remote", "Starts execution of specified task at remote nodes");
	cmd_rr << Text("Usage:");
	cmd_rr << Usage("graph [args...]");
	cmd_rr << "\nOptions:";
	cmd_rr << Positional(as_readable(graph_script), "graph", "Lua script defining task graph", Type::Required);
	cmd_rr << Positional(graph_script_args, "args...", "Arguments for graph script", Type::Greedy);
	cmd_rr << Option(port, "-p, --port", "Master node listening port");
	cmd_rr << Option(host, "-h, --host", "Master node listening address");
	cmd_rl << Flag(PageHelp(), "-?, --help", "Print help (this message) and exit");
	cmd << cmd_rr;

	Command cmd_wrk(cmd_str, "w|worker", "Starts remote worker");
	cmd_wrk << Text("Usage:");
	cmd_wrk << Usage("[options]");
	cmd_wrk << "\nOptions:";
	cmd_wrk << Option(port, "-p, --port", "Port of master node");
	cmd_wrk << Option(host, "-h, --host", "Host of master node");
	cmd_wrk << Flag(PageHelp(), "-?, --help", "Print help (this message) and exit");
	cmd << cmd_wrk;

	cmd << "\nOptions:";
	cmd << Flag(PageHelp(), "-h, -?, --help", "Print help (this message) and exit");

	Dashh t(&cmd);
	t.parse(argc, argv);

	if (cmd_str == "l" || cmd_str == "run-local")
		action = Action::RunLocal;
	else if (cmd_str == "r" || cmd_str == "run-remote")
		action = Action::RunRemote;
	else if (cmd_str == "w" || cmd_str == "worker")
		action = Action::Worker;
	else
		assert(false);
}
