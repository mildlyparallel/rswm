#include <iostream>
#include <thread>
#include <atomic>
#include <chrono>

#include <signal.h>

#include "rpc/Client.hh"

#include "CommandPool.hh"
#include "WorkerServer.hh"
// #include "Logger.hh"
#include "DispatcherClient.hh"
#include "Config.hh"

int main(int argc, char *argv[])
{
	std::string name;
	if (argc > 1)
		name = argv[1];

	try {
		Poll poll;
		poll.create();
		poll.notify_on_events();
		poll.notify_on_timer(Config::stats_poll_interval);
		poll.notify_on_signals({SIGINT, SIGHUP, SIGTERM, SIGQUIT, SIGCHLD});

		rpc::Client client;

		auto connection = client.connect(Config::port, Config::host);

		DispatcherClient dispatcher(connection);

		CommandPool commands;

		WorkerServer worker(commands, dispatcher);
		worker.set_poll(&poll);
		worker.bind_methods(client);
		worker.set_name(name);
		dispatcher.register_worker(worker);

		Poll::Alerts alerts;

		std::vector<std::shared_ptr<Command>> state_updates, stats_updates;

		while (!alerts.have_signals({SIGINT, SIGHUP, SIGTERM, SIGQUIT})) {
			state_updates.clear();
			stats_updates.clear();

			if (alerts.events)
				commands.spawn_pending_commands(state_updates);

			if (alerts.timer) {
				commands.stop_tardy_commands();
				commands.read_command_stats(stats_updates);
			}

			if (alerts.have_signal(SIGCHLD))
				commands.sync_finished_commands(state_updates);

			if (!state_updates.empty())
				dispatcher.send_states(state_updates);
			if (!stats_updates.empty())
				dispatcher.send_stats(stats_updates);

			if (worker.stopped())
				break;

			// TODO:: implement graceful termination

			poll.wait(alerts);
		}

		client.disconnect();
		poll.close();
	} catch (const std::exception &ex) {
		std::cerr << "ex.what() = " << ex.what() << std::endl;
	}

	// PCap::start();

	// while (true) {
	// 	WorkerServer worker(mq, pool);
    //
	// 	unsigned wid = pool.add(name);
	// 	worker.set_name(name);
	// 	worker.set_id(wid);
    //
	// 	worker.start();
    //
	// 	worker.run_commands();
    //
	// 	worker.stop();
	// }
    //
	// PCap::stop();

	return 0;
}
