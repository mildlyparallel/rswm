#include <random>
#include <cassert>

#include "Utils.hh"

namespace Utils {

std::string generate_stirng(size_t size)
{
	std::random_device dev;
	std::mt19937 gen(dev());
	std::uniform_int_distribution<int> distr('a', 'z');

	std::string s(size, '\0');
	for (auto &c : s)
		c = distr(gen);

	return s;
}

} /* namespace Utils */
