#include <cassert>

#include "AssignmentPool.hh"

AssignmentPool::AssignmentPool()
: m_last_id(0)
{  }

AssignmentPool::~AssignmentPool()
{  }

void AssignmentPool::add(std::shared_ptr<Assignment> assignment)
{
	std::unique_lock lg(m_lock);

	auto aid = ++m_last_id;

	assignment->set_id(aid);

	m_assignments.insert(assignment);
}

std::tuple<std::shared_ptr<Worker>, std::shared_ptr<Command>>
AssignmentPool::find(const std::string &hostname) const
{
	std::shared_lock lg(m_lock);

	for (auto &a : m_assignments) {
		auto [wrk, cmd] = a->find(hostname);
		if (!wrk)
			continue;

		return {wrk, cmd};
	}

	return {{}, {}};
}
