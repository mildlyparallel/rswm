#include <iostream>
#include <cassert>

#include "Logger.hh"
#include "Task.hh"

Task::Task()
: m_inherit_env(true)
, m_timeout_soft(0)
, m_timeout_hard(0)
, m_redirect_err_to_out(false)
, m_nr_nodes(1)
, m_nr_threads(0)
, m_nr_cores(0)
, m_nr_sockets(0)
, m_priority(0)
{ }

Task::~Task()
{ }

void Task::set_name(const std::string &name)
{
	m_name = name;
}

const std::string &Task::get_name() const
{
	return m_name;
}

void Task::set_file(const std::filesystem::path &p)
{
	m_file = p;
}

const std::filesystem::path &Task::get_file() const
{
	return m_file;
}

void Task::set_workdir(const std::filesystem::path &workdir)
{
	m_workdir = workdir;
}

const std::filesystem::path &Task::get_workdir() const
{
	return m_workdir;
}

void Task::set_args(const std::vector<std::string> &args)
{
	m_args = args;
}

void Task::add_arg(const std::string &a)
{
	m_args.push_back(a);
}

const std::vector<std::string> &Task::get_args() const
{
	return m_args;
}

void Task::set_envs(const std::vector<std::string> &env)
{
	m_envs = env;
}

void Task::add_env(const std::string &env)
{
	m_envs.push_back(env);
}

void Task::add_env(const std::string &k, const std::string &v)
{
	std::string e = k + "=" + v;
	m_envs.push_back(e);
}

const std::vector<std::string> &Task::get_envs() const
{
	return m_envs;
}

void Task::set_shell_cmd(const std::string &s)
{
	m_file = "/bin/sh";
	m_args = { "-c", s };
}

void Task::set_inherit_env(bool inherit)
{
	m_inherit_env = inherit;
}

bool Task::get_inherit_env() const
{
	return m_inherit_env;
}

void Task::set_timeout_soft(std::chrono::seconds s)
{
	m_timeout_soft = s;
}

std::chrono::seconds Task::get_timeout_soft() const
{
	return m_timeout_soft;
}

void Task::set_timeout_hard(std::chrono::seconds s)
{
	m_timeout_hard = s;
}

std::chrono::seconds Task::get_timeout_hard() const
{
	return m_timeout_hard;
}

void Task::set_stdout_log(const std::string &file)
{
	m_stdout_log = file;
}

const std::string &Task::get_stdout_log() const
{
	return m_stdout_log;
}

void Task::set_stderr_log(const std::string &file)
{
	m_stderr_log = file;
}

const std::string &Task::get_stderr_log() const
{
	return m_stderr_log;
}

void Task::set_redirect_err_to_out(bool redirect)
{
	m_redirect_err_to_out = redirect;
}

bool Task::get_redirect_err_to_out() const
{
	return m_redirect_err_to_out;
}

void Task::set_nr_nodes(size_t nr_nodes)
{
	if (nr_nodes > 1)
		m_nr_nodes = nr_nodes;
}

size_t Task::get_nr_nodes() const
{
	return m_nr_nodes;
}

void Task::set_nr_threads(size_t n)
{
	m_nr_threads = n;
}

size_t Task::get_nr_threads() const
{
	return m_nr_threads;
}

void Task::set_nr_cores(size_t n)
{
	m_nr_cores = n;
}

size_t Task::get_nr_cores() const
{
	return m_nr_cores;
}

void Task::set_nr_sockets(size_t n)
{
	m_nr_sockets = n;
}

size_t Task::get_nr_sockets() const
{
	return m_nr_sockets;
}

void Task::set_priority(unsigned value)
{
	m_priority = value;
}

unsigned Task::get_priority() const
{
	return m_priority;
}

void Task::set_cpulist(std::vector<int> cpulist)
{
	m_cpulist = cpulist;
}

const std::vector<int> &Task::get_cpulist() const
{
	return m_cpulist;
}

rpc::Buffer &operator<<(rpc::Buffer &msg, const Task &task) {
	msg << task.m_file;
	msg << task.m_workdir;
	msg << task.m_args;
	msg << task.m_envs;
	msg << task.m_inherit_env;

	uint64_t ts = task.m_timeout_soft.count();
	msg << ts;

	uint64_t th = task.m_timeout_hard.count();
	msg << th;

	msg << task.m_stdout_log;
	msg << task.m_stderr_log;

	msg << task.m_redirect_err_to_out;

	msg << task.m_nr_nodes;
	msg << task.m_nr_sockets;
	msg << task.m_nr_cores;
	msg << task.m_nr_threads;
	msg << task.m_priority;
	msg << task.m_cpulist;

	return msg;
}

const rpc::Buffer &operator>>(const rpc::Buffer &msg, Task &task) {
	msg >> task.m_file;
	msg >> task.m_workdir;
	msg >> task.m_args;
	msg >> task.m_envs;
	msg >> task.m_inherit_env;

	uint64_t ts;
	msg >> ts;
	task.m_timeout_soft = std::chrono::seconds(ts);

	uint64_t th;
	msg >> th;
	task.m_timeout_hard = std::chrono::seconds(th);

	msg >> task.m_stdout_log;
	msg >> task.m_stderr_log;

	msg >> task.m_redirect_err_to_out;

	msg >> task.m_nr_nodes;
	msg >> task.m_nr_sockets;
	msg >> task.m_nr_cores;
	msg >> task.m_nr_threads;
	msg >> task.m_priority;
	msg >> task.m_cpulist;

	return msg;
}

std::ostream &operator<<(std::ostream &os, const Task &task)
{
	os << "Task(";
	os << "file:" << task.get_file();
	os << "; workdir:" << task.get_workdir();
	os << "; args:" << task.get_args();
	os << "; envs:" << task.get_envs();
	os << "; inherit_env:" << task.get_inherit_env();
	os << "; timeout_soft:" << task.get_timeout_soft().count();
	os << "; timeout_hard:" << task.get_timeout_hard().count();
	os << "; stdout_log:" << task.get_stdout_log();
	os << "; stderr_log:" << task.get_stderr_log();
	os << "; redirect_err_to_out:" << task.get_redirect_err_to_out();
	os << "; nr_nodes:" << task.get_nr_nodes();
	os << "; priority:" << task.get_priority();
	os << ")";
	return os;
}
