#include <cassert>

#include "Worker.hh"

Worker::Worker()
: m_logger("worker")
, m_id(0)
{  }

Worker::~Worker()
{  }

void Worker::set_id(id_type id)
{
	assert(id > 0);
	m_id = id;
}

Worker::id_type Worker::get_id()
{
	return m_id;
}

void Worker::set_name(const std::string &name)
{
	m_name = name;
}

const std::string &Worker::get_name() const
{
	return m_name;
}

void Worker::stop()
{
	assert(false);
}

void Worker::submit_command(std::shared_ptr<Command>)
{
	assert(false);
}

// Worker::timepoint_type Worker::get_last_heartbeat() const
// {
// 	return m_last_heartbeat.load();
// }
//
// void Worker::set_last_heartbeat(Worker::timepoint_type tp)
// {
// 	timepoint_type last;
//
// 	do {
// 		last = m_last_heartbeat.load();
//
// 		if (tp <= last)
// 			return;
//
// 	} while (!m_last_heartbeat.compare_exchange_weak(last, tp));
// }
//
// bool Worker::submit(
// 	Command::id_type cid,
// 	const std::string &host,
// 	const std::vector<std::string> &hosts,
// 	const Task &task
// ) {
// 	return assign(cid, host, hosts, task) != nullptr;
// }
//
// const Worker::commands_container &Worker::commands() const
// {
// 	return m_commands;
// }
//
// Worker::commands_container &Worker::commands()
// {
// 	return m_commands;
// }
//
// bool Worker::is_active() const
// {
// 	auto last = m_last_heartbeat.load();
//
// 	if (last == timepoint_type())
// 		return false;
//
// 	auto dt = Worker::clock_type::now() - last;
//
// 	return dt < std::chrono::seconds(5);
// }
//
// std::lock_guard<std::mutex> Worker::lock() const
// {
// 	return std::lock_guard(m_state_lock);
// }

bool Worker::stopped() const
{
	return m_stopped;
}
