#include <cassert>

#include "rpc/Client.hh"

#include "rpc/Exception.hh"

#include <arpa/inet.h>

namespace rpc {

Client::Client(std::shared_ptr<Connection> connection)
: m_connection(connection)
, m_terminated(false)
{ }

Client::Client()
: m_terminated(false)
{ }

Client::~Client()
{
	m_terminated = true;

	try {
		m_poll.notify();

		if (m_worker.joinable())
			m_worker.join();
	} catch (...) { }
}

std::shared_ptr<Connection> Client::connect(uint16_t port, const std::string &host)
{
	assert(!m_poll.is_active());
	assert(!m_connection || !m_connection->is_active());

	Socket socket;
	socket.connect(port, host);

	int sfd = socket.get_fd();

	m_connection = std::make_shared<Connection>(std::move(socket), *this);

	m_poll.create();
	m_poll.notify_on_events();

	m_poll.add(Poll::IN | Poll::ET, sfd, 0);

	m_terminated = false;

	m_worker = std::thread([this](){
		try {
			while (!m_terminated)
				process_requests();
		} catch (const Exception &e) {
			m_logger.debug("Connection exception:", e.what());
			m_terminated = true;
			if (m_disconnect_cb)
				m_disconnect_cb(m_connection);
		} catch (...) {
			m_worker_ex = std::current_exception();
			m_terminated = true;
		}

		m_logger.debug("Worker thread exiting");
	});

	return m_connection;
}

void Client::process_requests()
{
	Poll::Alerts alerts;

	m_poll.wait(alerts);

	while (m_connection->recv_message()) {
		handle_message(m_connection);
	}

	while (m_connection->send_message())
	{  }
}

void Client::disconnect()
{
	m_logger.debug("Disconnecting");

	m_terminated = true;

	m_poll.notify();

	if (m_worker.joinable())
		m_worker.join();

	if (m_connection)
		m_connection->close();

	m_poll.close();

	m_worker_ex = nullptr;
}

bool Client::is_terminated()
{
	return m_terminated;
}

} /* namespace rpc */
