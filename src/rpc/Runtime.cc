#include "rpc/Runtime.hh"
#include "rpc/Connection.hh"

#include "rpc/Exception.hh"

namespace rpc {

Runtime::Runtime()
: m_logger("rpc")
{  }

Runtime::~Runtime()
{  }

void Runtime::on_connection(std::function<void(std::shared_ptr<Connection>)> cb)
{
	m_connection_cb = cb;
}

void Runtime::on_disconnect(std::function<void(std::shared_ptr<Connection>)> cb)
{
	m_disconnect_cb = cb;
}

void Runtime::handle_message(std::shared_ptr<Connection> connection)
{
	assert(connection);

	auto [name, reply_id, data] = connection->get_recieved_message();

	if (reply_id == static_cast<reply_id_type>(0)) {
		auto it = m_events.find(name);
		if (it == m_events.end()) 
			throw Exception(Exception::UnknownEventName);
		it->second(connection, data);
	} else {
		auto it = m_methods.find(name);
		if (it == m_methods.end()) 
			throw Exception(Exception::UnknownMethodName);
		it->second(connection, reply_id, data);
	}

	connection->reset_recived_message();
}

void Runtime::notify()
{
	m_poll.notify();
}

} /* namespace rpc */
