#include <cassert>
#include <iostream>
#include <cstring>

#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <fcntl.h>

#include "rpc/Acceptor.hh"
#include "Utils.hh"

namespace rpc {

Acceptor::Acceptor()
: m_logger("socket")
, m_sock(-1)
, m_port(0)
{ }

Acceptor::~Acceptor()
{
	if (m_sock != -1)
		::close(m_sock);
}

void Acceptor::bind(uint16_t port, const std::string &host)
{
	assert(port > 0);
	assert(m_sock == -1);

	m_logger.debug("Binding to", host, port);

	struct addrinfo hints = {};

	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_canonname = nullptr;
	hints.ai_addr = nullptr;
	hints.ai_next = nullptr;

	if (bind_ip4_addresess && bind_ip6_addresess) {
		hints.ai_family = AF_UNSPEC;
		m_logger.debug("Binding to IPv4 and IPv6 (AF_UNSPEC)");
	} else if (bind_ip6_addresess) {
		hints.ai_family = AF_INET6;
		m_logger.debug("Binding to IPv6 only (AF_INET6)");
	} else {
		hints.ai_family = AF_INET;
		m_logger.debug("Binding to IPv4 only (AF_INET)");
	}

	const char *node = nullptr;
	if (!host.empty())
		node = host.c_str();

	std::string service = std::to_string(port);

	struct addrinfo *result;
	int rc = getaddrinfo(node, service.c_str(), &hints, &result);
	if (rc != 0)
		throw std::system_error(errno, std::generic_category(), gai_strerror(rc));

	m_port = port;
	m_host = host;

	struct addrinfo *rp = nullptr;
	int last_errno = 0;

	for (rp = result; rp != nullptr; rp = rp->ai_next) {
		m_sock = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);

		if (m_sock == -1) {
			m_logger.debug("Failed to create socket:", strerror(errno));
			continue;
		}

		if (::bind(m_sock, rp->ai_addr, rp->ai_addrlen) == 0)
			break;

		last_errno = errno;

		m_logger.error("bind() failed:", strerror(last_errno));

		if (rp->ai_next)
			m_logger.debug("Trying next address");
		else
			m_logger.debug("No more addresses to try");

		::close(m_sock);
		m_sock = -1;
	}

	freeaddrinfo(result);

	if (rp == nullptr)
		throw std::system_error(last_errno, std::generic_category(), "Could not bind socket");

	// set_nonblocking();

	m_logger.info("Binded to", host, port);

	assert(m_sock != -1);
}

void Acceptor::set_nonblocking()
{
	assert(m_sock != -1);

	int f = fcntl(m_sock, F_GETFL, 0);
	THROW_IF(f < 0);

	f = fcntl(m_sock, F_SETFL, f | O_NONBLOCK);
	THROW_IF(f < 0);
}

void Acceptor::listen(int backlog)
{
	assert(m_sock != -1);

	if (backlog < 0 || backlog > SOMAXCONN)
		backlog = SOMAXCONN;

	THROW_IF(::listen(m_sock, backlog) < 0);
}

Socket Acceptor::accept()
{
	assert(m_sock != -1);

	int sfd = ::accept(m_sock, nullptr, nullptr);
	THROW_IF(sfd < 0);

	m_logger.debug("Accepted new connection");

	return Socket(sfd);
}

void Acceptor::close()
{
	if (m_sock == -1)
		return;

	m_logger.debug("Closing accepting socket");

	THROW_IF(::close(m_sock));
	m_sock = -1;
}

uint16_t Acceptor::get_port() const
{
	return m_port;
}

const std::string &Acceptor::get_host() const
{
	return m_host;
}

int Acceptor::get_fd() const
{
	return m_sock;
}

bool Acceptor::is_active() const
{
	return m_sock != -1;
}

} /* namespace rpc */
