#include "rpc/Server.hh"
#include "rpc/Exception.hh"

namespace rpc {

Server::Server()
: m_last_connection_id(0)
{  }

Server::~Server()
{ 
	m_terminated = true;

	try {
		m_poll.notify();

		if (m_worker.joinable())
			m_worker.join();
	} catch (...) { }
}

void Server::listen(uint16_t port, const std::string &host)
{
	assert(!m_acceptor.is_active());

	m_logger.info("Binding to", host, port);

	m_acceptor.bind(port, host);

	m_acceptor.listen();

	m_poll.create();

	int acc_fd = m_acceptor.get_fd();

	m_poll.notify_on_events();

	m_poll.add(Poll::IN, acc_fd, 0);

	start_workers();
}

std::shared_ptr<Connection> Server::add_new_connection(Socket &&socket)
{
	m_logger.debug("Accepted new connection");

	std::unique_lock lg(m_connections_lock);

	do {
		m_last_connection_id++;
	} while (m_connections.contains(m_last_connection_id));

	int sfd = socket.get_fd();

	auto connection = std::make_shared<Connection>(
		std::move(socket),
		*this,
		m_last_connection_id
	);

	m_connections[connection->get_id()] = connection;

	m_poll.add(Poll::IN | Poll::ET, sfd, connection->get_id());

	if (m_connection_cb)
		m_connection_cb(connection);

	return connection;
}

void Server::process_events()
{
	Poll::Alerts alerts;

	m_poll.wait(alerts);

	for (auto ud : alerts.userdata) {
		if (ud == 0) {
			Socket socket = m_acceptor.accept();
			add_new_connection(std::move(socket));
			continue;
		}

		auto con = find_connection(ud);
		if (!con)
			continue;

		try {
			while (con->recv_message()) {
				handle_message(con);
			}

			while (con->send_message())
			{  }

		} catch (const Exception &e) {
			m_logger.error("Connection exception:", e.what());
			remove_connection(con);
		}
	}

	if (alerts.events) {
		// TODO: remove this loop somehow?
		for (auto [cid, con] : m_connections) {
			try {
				while (con->send_message())
				{  }
			} catch (const Exception &e) {
				m_logger.error("Connection exception:", e.what());
				remove_connection(con);
			}
		}
	}
}

std::shared_ptr<Connection> Server::find_connection(size_t con_id)
{
	std::shared_lock lg(m_connections_lock);

	auto con_it = m_connections.find(con_id);
	if (con_it == m_connections.end())
		return {};

	return con_it->second;
}

void Server::remove_connection(std::shared_ptr<Connection> &connection)
{
	if (m_disconnect_cb)
		m_disconnect_cb(connection);

	std::unique_lock lg(m_connections_lock);

	size_t id = connection->get_id();

	assert(m_connections.contains(id));

	m_connections.erase(id);
}

void Server::start_workers()
{
	m_terminated = false;

	m_worker = std::thread([this](){
		try {
			while (!m_terminated)
				process_events();
		} catch (const std::exception &e) {
			m_logger.debug("Worker throws:", e.what());
			m_worker_ex = std::current_exception();
			m_terminated = true;
		}

		m_logger.debug("Worker thread exit");
	});
}

void Server::stop()
{
	m_terminated = true;

	m_poll.notify();

	if (m_worker.joinable())
		m_worker.join();

	m_poll.close();

	for (auto [id, con] : m_connections) {
		if (!con || !con->is_active())
			continue;

		con->close();
		if (m_disconnect_cb)
			m_disconnect_cb(con);
	}

	m_connections.clear();

	if (m_worker_ex) {
		std::cerr << "Retrowing worker exception" << std::endl;
		std::rethrow_exception(m_worker_ex);
	}
}

bool Server::is_terminated()
{
	return m_terminated;
}

} /* namespace rpc */
