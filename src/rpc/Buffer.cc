#include <new>
#include <stdexcept>
#include <iostream>
#include <cstring>
#include <cassert>

#include "rpc/Buffer.hh"

namespace rpc {

Buffer::Buffer()
: m_data(nullptr)
, m_position(0)
, m_size(0)
{ }

Buffer::Buffer(Buffer &&other)
: m_data(other.m_data)
, m_position(other.m_position)
, m_size(other.m_size)
{ 
	other.m_data = nullptr;
	other.m_position = 0;
	other.m_size = 0;
}

Buffer::Buffer(const Buffer &other)
: m_data(nullptr)
, m_position(other.m_position)
, m_size(0)
{
	grow(other.m_size);

	std::memcpy(m_data, other.m_data, m_size);
}

Buffer::~Buffer()
{
	if (m_data)
		::free(m_data);
}

bool Buffer::grow(size_t n)
{
	if (n <= m_size)
		return false;

	m_data = static_cast<byte_type *>(::realloc(m_data, n));
	if (!m_data)
		throw std::bad_alloc();

	m_size = n;
	return true;
}

const Buffer::byte_type *Buffer::data() const
{
	return m_data;
}

Buffer::byte_type *Buffer::data()
{
	return m_data;
}

bool Buffer::empty() const
{
	return m_size == 0;
}

size_t Buffer::position() const
{
	return m_position;
}

size_t Buffer::size() const
{
	return m_size;
}

void Buffer::rewind()
{
	m_position = 0;
}

void Buffer::store(const std::string &s)
{
	uint64_t sz = s.size();

	store(sz);

	for (size_t i = 0; i < sz; ++i) {
		uint8_t c = s[i];
		store(c);
	}
}

void Buffer::load(std::string &s) const
{
	uint64_t sz = 0;
	load(sz);

	std::string res;
	for (size_t i = 0; i < sz; ++i) {
		uint8_t c;
		load(c);

		res += c;
	}

	s = res;
}

void Buffer::store(const std::filesystem::path &p)
{
	std::string s(p.c_str());
	store(s);
}

void Buffer::load(std::filesystem::path &p) const
{
	std::string s;
	load(s);
	p = std::filesystem::path(s);
}

void Buffer::store(const void *src, size_t n)
{
	assert(n);
	assert(src);

	if (m_position + n >= m_size)
		grow(m_position + n);

	std::memcpy(m_data + m_position, src, n);
	m_position += n;
}

void Buffer::load(void *dst, size_t n) const
{
	assert(n);
	assert(dst);
	assert(m_data);

	if (m_position + n > m_size)
		throw std::range_error("buffer out of boundary access");

	std::memcpy(dst, m_data + m_position, n);
	m_position += n;
}

std::ostream &operator<<(std::ostream &os, const Buffer &m)
{
	os << "Buffer(" << m.position() << "/" << m.size()  << ")";

	const size_t max_len = 16;

	size_t n = std::min(max_len, m.size());

	os << std::hex << std::setfill('0');

	for (size_t i = 0; i < n; ++i) {
		if (i % 4 == 0)
			os << " ";

		unsigned char b = m.data()[i];
		unsigned bb = b;
		os << std::setw(2) << bb;
	}

	os << std::dec;

	if (m.size() > max_len)
		os << "..";

	return os;
}

} /* namespace rpc */
