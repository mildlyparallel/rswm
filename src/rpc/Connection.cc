#include <cassert>
#include <thread>
#include <stdexcept>

#include <arpa/inet.h>

#include "rpc/Connection.hh"
#include "rpc/Runtime.hh"
#include "rpc/Exception.hh"

namespace rpc {

Connection::Connection(Socket &&socket, Runtime &runtime, size_t id)
: m_logger("rpc")
, m_id(id)
, m_socket(std::move(socket))
, m_runtime(runtime)
, m_nread(0)
, m_next_reply_id(1)
{ }

Connection::Connection(Connection &&c)
: m_logger("rpc")
, m_id(c.m_id)
, m_socket(std::move(c.m_socket))
, m_runtime(c.m_runtime)
, m_nread(c.m_nread)
, m_message_size(c.m_message_size)
, m_next_reply_id(c.m_next_reply_id)
{ }

bool Connection::recv_message()
{
	std::lock_guard<std::mutex> lg(m_lock);

	if (!m_socket.is_active()) {
		m_logger.warning("Can not recieve message from inactive socket");
		return false;
	}

	static constexpr size_t sz = sizeof(m_message_size);

	size_t nread = 0;

	if (m_nread < sz) {
		uint8_t *buf = reinterpret_cast<uint8_t *>(&m_message_size);
		nread = m_socket.recv(buf + m_nread, sz - m_nread);

		m_nread += nread;

		assert(m_nread <= sz);

		if (m_nread == sz)
			m_message_size = ntohl(m_message_size);
	}

	if (m_nread < sz)
		return false;

	if (m_message_size >= max_message_size) {
		m_logger.warning("Message size is too large");
		m_socket.close();
		throw Exception(Exception::MessageTooLarge);
	}

	if (m_recv_buffer.size() < m_message_size)
		m_recv_buffer.grow(m_message_size);

	assert(m_recv_buffer.size() >= m_message_size);

	size_t msg_end = sz + m_message_size;
	assert(m_nread < msg_end);

	nread = m_socket.recv(&m_recv_buffer.data()[m_nread - sz], msg_end - m_nread);
	m_nread += nread;

	assert(m_nread <= msg_end);

	if (m_nread != msg_end)
		return false;

	if (handle_reply())
		return false;

	m_logger.debug("Recieved message");

	return true;
}

bool Connection::handle_reply()
{
	method_name_type name;
	m_recv_buffer >> name;

	if (name != static_cast<method_name_type>(0)) {
		reset_recived_message();
		return false;
	}

	reply_id_type reply_id; 
	m_recv_buffer >> reply_id;

	m_logger.debug("Recieved reply message for", (size_t)reply_id);

	std::lock_guard<std::mutex> lg(m_replies_lock);

	auto reply = m_replies.find(reply_id);
	if (reply == m_replies.end())
		throw Exception(Exception::UnknownReplyId);

	reply->second.set_value(m_recv_buffer);
	m_replies.erase(reply);

	reset_recived_message();
	return true;
}

void Connection::reset_recived_message()
{
	m_nread = 0;
	m_recv_buffer.rewind();
}

bool Connection::is_active()
{
	return m_socket.is_active();
}

void Connection::close()
{
	m_socket.close();
}

size_t Connection::get_id() const
{
	return m_id;
}

bool Connection::send_message()
{
	std::lock_guard<std::mutex> lg(m_lock);

	if (!m_socket.is_active()) {
		m_logger.warning("Can not send message to inactive socket");
		return false;
	}

	std::unique_ptr<Buffer> message;

	{
		std::lock_guard<std::mutex> lgq(m_send_queue_lock);

		if (m_send_queue.empty()) {
			m_logger.debug("No messages to send");
			return false;
		}

		message = std::move(m_send_queue.front()); 
		m_send_queue.pop();
	}

	assert(message);
	assert(message->position() > 0);

	uint32_t size = htonl(message->position());
	m_socket.send(&size, sizeof(size));
	m_socket.send(message->data(), message->position());

	m_logger.debug("Send message to the socket");

	return true;
}

std::tuple<reply_id_type, std::future<Buffer>> Connection::create_next_reply()
{
	std::lock_guard<std::mutex> lg(m_replies_lock);

	reply_id_type rid = m_next_reply_id;
	m_next_reply_id++;

	assert(!m_replies.contains(rid));

	auto [it, ok] = m_replies.insert({rid, std::promise<Buffer>()});

	assert(ok);

	return {rid, it->second.get_future()};
}

void Connection::push_message(std::unique_ptr<Buffer> &&m)
{
	assert(m);
	std::lock_guard<std::mutex> lg(m_send_queue_lock);
	m_logger.debug("Pushed message size", m->position());
	m_send_queue.push(std::move(m));

	m_runtime.notify();
}

std::tuple<method_name_type, reply_id_type, const Buffer &>
Connection::get_recieved_message()
{
	assert(m_recv_buffer.position() == 0);

	method_name_type name;
	m_recv_buffer >> name;

	reply_id_type reply_id; 
	m_recv_buffer >> reply_id;

	return {name, reply_id, m_recv_buffer};
}

} /* namespace rpc */
