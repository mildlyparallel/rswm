#include <cassert>
#include <system_error>
#include <iostream>
#include <cstring>


#include <unistd.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#include <sys/types.h>
#include <netdb.h>

#include "rpc/Socket.hh"
#include "rpc/Exception.hh"

#include "Utils.hh"

namespace rpc {

Socket::Socket(Socket &&other)
: m_logger("socket")
, m_sock(other.m_sock)
, m_port(other.m_port)
, m_host(std::move(other.m_host))
{
	other.m_sock = -1;
}

Socket::Socket(int sock)
: m_logger("socket")
, m_sock(sock)
, m_port(0)
{ 
	if (sock != -1)
		getpeername();
}

Socket &Socket::operator=(Socket &&other)
{
	close();

	m_sock = other.m_sock;
	m_port = other.m_port;
	m_host = std::move(other.m_host);

	other.m_sock = -1;

	return *this;
}

Socket::~Socket()
{
	if (m_sock != -1)
		::close(m_sock);
}

void Socket::close()
{
	if (m_sock == -1)
		return;

	m_logger.debug("Closing socket");

	int rc = ::close(m_sock);
	THROW_IF(rc < 0);

	m_sock = -1;
}

void Socket::getpeername()
{
	assert(m_sock != -1);

	struct sockaddr_in6 sa6 = {};
	socklen_t len = sizeof(sa6);

	struct sockaddr *sa = (struct sockaddr *) &sa6;

	int rc = ::getpeername(m_sock, sa, &len);
	THROW_IF(rc < 0);

	char addr_str[INET6_ADDRSTRLEN] = {0};

	void *src = nullptr;
	if (sa->sa_family == AF_INET6) {
		src = &(sa6.sin6_addr);
		m_port = ntohs(sa6.sin6_port);
	} else {
		struct sockaddr_in *sa4 = (struct sockaddr_in *) &sa6;
		src = &(sa4->sin_addr);
		m_port = ntohs(sa4->sin_port);
	}

	const char *dst = inet_ntop(sa->sa_family, src, addr_str, sizeof(addr_str));
	THROW_IF(dst == nullptr);

	m_host = addr_str;

	m_logger.debug("Peer name ", m_host, m_port);
}

size_t Socket::recv(void *dst, size_t n, bool dontwait)
{
	if (m_sock == -1)
		throw Exception(Exception::ConnectionClosed);

	assert(dst != nullptr);
	assert(n > 0);

	int f = 0;
	if (dontwait)
		f = MSG_DONTWAIT;

	ssize_t nread = ::recv(m_sock, dst, n, f);

	if (nread < 0 && (errno == EAGAIN || errno == EWOULDBLOCK))
		return 0;

	THROW_IF(nread < 0);

	if ((dontwait == false && (size_t)nread < n) || nread == 0) {
		m_logger.debug("Closing connection: recieved", nread, "bytes instead of", n);
		close();
		throw Exception(Exception::ConnectionClosed);
	}

	if (nread > 0)
		return nread;

	return 0;
}

void Socket::send(const void *src, size_t len)
{
	assert(is_active());

	assert(src != nullptr);
	assert(len > 0);

	size_t nsend = 0;
	const uint8_t *p = reinterpret_cast<const uint8_t *>(src);

	while (nsend < len) {
		ssize_t n = ::send(m_sock, p, len - nsend, MSG_NOSIGNAL);

		if (n < 0) {
			if (errno == EINTR)
				continue;
			m_sock = -1;
		}

		THROW_IF(n < 0);

		nsend += n;
		p += n;
	}

	m_logger.debug("Send", len, "bytes");

	assert(nsend == len);
}

void Socket::connect(uint16_t port, const std::string &host)
{
	assert(port > 0);
	assert(m_sock == -1);

	m_logger.debug("Connecting to", host, port);

	struct addrinfo hints = {};

	hints.ai_socktype = SOCK_STREAM;
	hints.ai_flags = AI_PASSIVE;
	hints.ai_protocol = IPPROTO_TCP;
	hints.ai_canonname = nullptr;
	hints.ai_addr = nullptr;
	hints.ai_next = nullptr;

	if (connect_ip4_addresess && connect_ip6_addresess) {
		hints.ai_family = AF_UNSPEC;
		m_logger.debug("Connecting to IPv4 and IPv6 (AF_UNSPEC)");
	} else if (connect_ip6_addresess) {
		hints.ai_family = AF_INET6;
		m_logger.debug("Connecting to IPv6 only (AF_INET6)");
	} else {
		hints.ai_family = AF_INET;
		m_logger.debug("Connecting to IPv4 only (AF_INET)");
	}

	const char *node = nullptr;
	if (!host.empty())
		node = host.c_str();

	std::string service = std::to_string(port);

	struct addrinfo *result;
	int rc = getaddrinfo(node, service.c_str(), &hints, &result);
	if (rc != 0)
		throw std::system_error(errno, std::generic_category(), gai_strerror(rc));

	m_port = port;
	m_host = host;

	struct addrinfo *rp = nullptr;
	int last_errno = 0;

	for (rp = result; rp != nullptr; rp = rp->ai_next) {
		m_sock = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);

		if (m_sock == -1) {
			m_logger.debug("Failed to create socket:", strerror(errno));
			continue;
		}

		if (::connect(m_sock, rp->ai_addr, rp->ai_addrlen) == 0)
			break;

		last_errno = errno;

		m_logger.error("connect() failed: ", strerror(last_errno));

		if (rp->ai_next)
			m_logger.debug("Trying next address");
		else
			m_logger.debug("No more addresses to try");

		::close(m_sock);
		m_sock = -1;
	}

	freeaddrinfo(result);

	if (rp == nullptr)
		throw std::system_error(last_errno, std::generic_category(), "Could not connect socket");

	m_logger.info("Connected to", host, port);

	assert(m_sock != -1);
}

uint16_t Socket::get_port() const
{
	return m_port;
}

const std::string &Socket::get_host() const
{
	return m_host;
}

bool Socket::is_active() const
{
	return m_sock != -1;
}

int Socket::get_fd() const
{
	return m_sock;
}

} /* namespace rpc */
