#include <cassert>
#include <chrono>
#include <thread>

#include "SchedulerLifo.hh"

#include "Assignment.hh"
#include "Dispatcher.hh"
#include "WorkerPool.hh"

SchedulerLifo::SchedulerLifo(WorkerPool &workers, Dispatcher &dispatcher)
: Scheduler(workers, dispatcher)
{  }

SchedulerLifo::~SchedulerLifo()
{  }

TaskGraph::const_iterator SchedulerLifo::find_next(const TaskGraph &graph) const
{
	for (auto it = graph.cbegin(); it != graph.cend(); ++it) {
		if (m_submitted.contains(it->id))
			continue;

		if (m_finished.contains(it->id))
			continue;

		if (it->id == it->prev_id)
			return it;

		if (!m_finished.contains(it->prev_id))
			continue;

		return it;
	}

	return graph.cend();
}

SchedulerLifo::Result SchedulerLifo::run(const TaskGraph &graph)
{
	Result result = update_finished();

	if (result != Unknown)
		return result;

	if (m_finished.size() == graph.size())
		return Done;

	while (true) {
		auto it = find_next(graph);

		if (it == graph.cend()) {
			m_logger.debug("No more availiable tasks");
			assert(!m_submitted.empty());
			return result != Unknown ? result : WaitComplition;
		}

		m_logger.info("Found task", it->id);
		assert(!m_submitted.contains(it->id));

		auto workers = m_workers.get(it->task.get_nr_nodes());
		if (workers.size() != it->task.get_nr_nodes()) {
			m_logger.debug("Not enoght nodes for the task", it->id);
			return result != Unknown ? result : WaitWorkers;
		}

		assert(!m_submitted.contains(it->id));
		m_logger.debug("Assigning task", it->id, "to", workers.size(), "worker(s)");
		m_submitted[it->id] = m_dispatcher.assign(it->task, workers);
		result = Submitted;
	}

	return result;
}
