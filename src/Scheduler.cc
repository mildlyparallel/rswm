#include <cassert>

#include "Scheduler.hh"
#include "Assignment.hh"
#include "WorkerPool.hh"

Scheduler::Scheduler(WorkerPool &workers, Dispatcher &dispatcher)
: m_logger("scheduler")
, m_workers(workers)
, m_dispatcher(dispatcher)
{  }

Scheduler::~Scheduler()
{  }

bool Scheduler::check_if_failed(std::shared_ptr<Assignment> assignment) const
{
	bool failed = false;

	for (auto [wrk, cmd] : assignment->commands()) {
		// XXX: is it ok to skip?
		if (!cmd)
			continue;

		if (cmd->get_state() == Command::Exited) {
			if (cmd->get_return_code() == 0) {
				m_logger.info("Command", cmd->get_name(), "has finished successfully");
				continue;
			}

			m_logger.error("Command", cmd->get_name(), "has finished with return code", cmd->get_return_code());
			failed = true;
		}

		if (cmd->get_state() == Command::Terminated) {
			m_logger.error("Command", cmd->get_name(), "was terminated");
			failed = true;
		}

		if (cmd->get_state() == Command::Failed) {
			m_logger.error("Command", cmd->get_name(), "failed to start");
			failed = true;
		}
	}

	return failed;
}

Scheduler::Result Scheduler::update_finished()
{
	bool failed = false;

	for (auto it = m_submitted.begin(); it != m_submitted.end();) {
		if (!it->second->is_finished()) {
			++it;
			continue;
		}

		failed |= check_if_failed(it->second);

		m_finished.insert({it->first, it->second});
		it = m_submitted.erase(it);
	}

	if (failed)
		return Failed;

	return Unknown;
}

