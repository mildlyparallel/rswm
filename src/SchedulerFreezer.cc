#include <cassert>
#include <chrono>
#include <thread>
#include <cmath>

#include "SchedulerFreezer.hh"
#include "ResourceStats.hh"
#include "Dispatcher.hh"
#include "WorkerPool.hh"

SchedulerFreezer::SchedulerFreezer(WorkerPool &workers, Dispatcher &dispatcher)
: Scheduler(workers, dispatcher)
{
	for (size_t i = 0; i < m_active.size(); ++i) {
		m_active[i].id = i;
		m_active[i].active = false;
		m_active[i].task_id = -1;
		m_active[i].interpolator.set_task_id(i);
	}
	m_init_at = Command::clock_type::now();

	m_trace = std::ofstream("trace.csv");
	m_trace << "ts,cid,wid,base,current,accel_cur,accel_old,resample\n";
	m_trace.flush();

	m_trace_glob = std::ofstream("trace-glob.csv");
	m_trace_glob << "ts,size,comb_speed,jian\n";
	m_trace_glob.flush();
}

SchedulerFreezer::~SchedulerFreezer()
{  }

std::set<TaskGraph::task_id> SchedulerFreezer::find_schedulable(const TaskGraph &graph) const
{
	std::set<TaskGraph::task_id> found;

	for (auto it = graph.cbegin(); it != graph.cend(); ++it) {
		if (m_submitted.contains(it->id))
			continue;
		if (m_finished.contains(it->id))
			continue;

		if (it->id == it->prev_id) {
			found.insert(it->id);
			continue;
		}

		if (m_finished.contains(it->prev_id))
			found.insert(it->id);
	}

	return found;
}

SchedulerFreezer::Active::id_type SchedulerFreezer::find_active(TaskGraph::task_id tid) const
{
	Active::id_type aid = m_active.size();

	for (size_t i = 0; i < m_active.size(); ++i) {
		if (!m_active[i].active && aid == m_active.size()) {
			aid = i;
			continue;
		}

		if (m_active[i].task_id == tid) {
			return i;
		}
	}

	return aid;
}

double SchedulerFreezer::get_task_speed(
	Active::id_type aid,
	double dt_ms
) {
	assert(m_active[aid].assignment);

	const auto &commands = m_active[aid].assignment->commands(); 

	size_t n = 0;

	uint64_t cum_inst = 0;
	uint64_t cum_cycl = 0;
	uint64_t cum_stime = 0;
	uint64_t cum_utime = 0;

	for (size_t i = 0; i < commands.size(); ++i) {
		if (!commands[i].second)
			continue;

		n++;

		auto &stats = commands[i].second->get_stats();
		cum_inst += stats.get(ResourceStats::Instructions);
		cum_cycl += stats.get(ResourceStats::CpuCycles);
		cum_utime += stats.get(ResourceStats::UserTime);
		cum_stime += stats.get(ResourceStats::SystemTime);
	}

	assert(n > 0);

	if (n == 0)
		return 0;

	cum_inst /= n;
	cum_cycl /= n;
	cum_utime /= n;
	cum_stime /= n;

	uint64_t dinst = 0;
	if (cum_inst - m_active[aid].cum_inst)
		dinst = cum_inst - m_active[aid].cum_inst;

	uint64_t dcycl = 0;
	if (cum_cycl > m_active[aid].cum_cycl)
		dcycl = cum_cycl - m_active[aid].cum_cycl;

	uint64_t dutime = 0;
	if (cum_utime > m_active[aid].cum_utime)
		dutime = cum_utime - m_active[aid].cum_utime;

	uint64_t dstime = 0;
	if (cum_stime > m_active[aid].cum_stime)
		dstime = cum_stime - m_active[aid].cum_stime;

	double ipc = 1. * dinst / dcycl;
	double ctime = 1. * (1. * dutime/100. + dstime/100.) / (dt_ms/1e3);
	if (dutime == 0 && dstime == 0)
		ctime = 1;
	double speed = ipc * ctime;

	// std::cerr << "cum_inst = " << cum_inst << std::endl;
	// std::cerr << "cum_cycl = " << cum_cycl << std::endl;
	// std::cerr << "dinst = " << dinst << std::endl;
	// std::cerr << "dcycl = " << dcycl << std::endl;
	// std::cerr << "ipc = " << ipc << std::endl;
	// std::cerr << "ctime = " << ctime << std::endl;

	return speed;
}

void SchedulerFreezer::update_cum_sum(Active::id_type i)
{
	assert(m_active[i].assignment);

	const auto &commands = m_active[i].assignment->commands(); 

	size_t n = 0;

	uint64_t cum_inst = 0;
	uint64_t cum_cycl = 0;
	uint64_t cum_stime = 0;
	uint64_t cum_utime = 0;

	for (size_t i = 0; i < commands.size(); ++i) {
		if (!commands[i].second)
			continue;

		n++;

		auto &stats = commands[i].second->get_stats();
		cum_inst += stats.get(ResourceStats::Instructions);
		cum_cycl += stats.get(ResourceStats::CpuCycles);
		cum_utime += stats.get(ResourceStats::UserTime);
		cum_stime += stats.get(ResourceStats::SystemTime);
	}

	if (n > 0) {
		cum_inst /= n;
		cum_cycl /= n;
		cum_utime /= n;
		cum_stime /= n;
	}

	m_active[i].cum_inst = cum_inst;
	m_active[i].cum_cycl = cum_cycl;
	m_active[i].cum_utime = cum_utime;
	m_active[i].cum_stime = cum_stime;
}

void SchedulerFreezer::add_to_active(const std::set<TaskGraph::task_id> &schedulable)
{
	for (auto task_id : schedulable) {
		Active::id_type active_id = find_active(task_id);

		if (active_id >= m_active.size()) {
			m_logger.debug("No more active slots");
			break;
		}

		if (m_active[active_id].active)
			continue;

		m_logger.debug("Adding task", task_id, "to active set");
		m_active[active_id].reset();
		m_active[active_id].active = true;
		m_active[active_id].task_id = task_id;

		for (size_t i = 0; i < m_active.size(); ++i) {
			if (!m_active[i].active)
				continue;
			m_active[i].interpolator.flush(active_id);
		}
	}
}

std::optional<TaskGraph::task_id> SchedulerFreezer::find_not_submitted() const
{
	std::optional<TaskGraph::task_id> found;

	for (size_t i = 0; i < m_active.size(); ++i) {
		if (!m_active[i].active)
			continue;
		if (m_active[i].assignment)
			continue;

		found = m_active[i].task_id;
		break;
	}

	return found;
}

// std::set<TaskGraph::task_id> SchedulerFreezer::get_next_combination() const
// {
// 	if (auto tid = find_not_measured(); tid)
// 		return {*tid};
// }

Scheduler::Result SchedulerFreezer::submit(const TaskGraph &graph, TaskGraph::task_id tid)
{
	size_t nr_nodes = 1;
	auto workers = m_workers.get(nr_nodes);

	if (workers.size() != nr_nodes) {
		m_logger.debug("Not enoght nodes");
		return WaitWorkers;
	}

	assert(!m_submitted.contains(tid));
	m_logger.debug("Assigning task", tid);
	m_submitted[tid] = m_dispatcher.assign(graph.at(tid), workers);

	return Submitted;
}

Taskset SchedulerFreezer::get_running()
{
	Taskset ts;
	for (size_t i = 0; i < m_active.size(); ++i) {
		if (m_active[i].running)
			ts.set(i);
	}

	return ts;
}

double SchedulerFreezer::get_jian() const
{
	size_t nr_active = 0;
	double jian_ri_sum = 0;
	double jian_ri_sq_sum = 0;
	for (size_t i = 0; i < m_active.size(); ++i) {
		if (!m_active[i].active)
			continue;
		nr_active++;
		jian_ri_sum += m_active[i].accel_sum;
		jian_ri_sq_sum += m_active[i].accel_sum * m_active[i].accel_sum;
	}

	double jian = 1;
	if (nr_active > 0)
		jian = jian_ri_sum * jian_ri_sum / (1. * nr_active * jian_ri_sq_sum);

	return jian;
}

SchedulerFreezer::Result SchedulerFreezer::update_running(Taskset running)
{
	auto now = Command::clock_type::now();
	auto dt_since_start = now - m_init_at;
	auto dt_since_measure = now - m_measured_at;

	if (dt_since_measure < measure_unit) {
		m_logger.debug("Wait full measure unit");
		return WaitComplition;
	}

	double ms_since_start = std::chrono::duration_cast<std::chrono::milliseconds>(dt_since_start).count();
	double s_since_start = ms_since_start / 1e3;
	double ms_since_measure = std::chrono::duration_cast<std::chrono::milliseconds>(dt_since_measure).count();
	bool measured_base = false;

	double tp = 0;

	std::cerr << "\nTP:";
	for (size_t i = 0; i < m_active.size(); ++i) {
		if (!m_active[i].running)
			continue;

		double current = get_task_speed(i, ms_since_measure);
		m_active[i].current_speed = current;

		if (running.count() == 1) {

			if (m_active[i].base_speed_old && m_active[i].runtime_since_base > 0) {
				double base_old = *m_active[i].base_speed_old;
				double base_delta = std::abs((current - base_old) / base_old);
				if (base_delta > m_max_accel_delta) {
					m_logger.debug("Task", m_active[i].task_id,
						"base changed from ", base_old, "to", current,
						"delta", base_delta
					);

					m_active[i].resample = true;
					m_active[i].flush = true;
				} else {
					m_active[i].resample = false;
					m_active[i].flush = false;
				}
			}

			if (m_active[i].base_speed)
				m_active[i].base_speed_old = *m_active[i].base_speed;

			m_active[i].base_speed = current;
			measured_base = true;
			m_active[i].runtime_since_base = 0;
			m_active[i].accel_sum += 1;
			std::cerr << 1 << " ";
			tp += 1;
		} else {
			double base = *m_active[i].base_speed;
			double accel = current / base;
			accel = std::clamp(accel, 0., 1.);
			// std::cerr << i << " " << running << " " << accel << "\n";
			m_active[i].interpolator.add(running, std::clamp(accel, 0., 1.));
			m_active[i].runtime_since_base += 1;
			m_active[i].accel_sum += accel;

			std::cerr << accel << " ";
			tp += accel;
		}

		if (m_active[i].runtime_since_base > m_units_until_resample) {
			m_logger.debug("Task", m_active[i].task_id, "base outdated");
			m_active[i].resample = true;
		}

		m_trace << s_since_start << ",";
		m_trace << m_active[i].task_id << "," << i << ",";
		m_trace << *m_active[i].base_speed << ",";
		m_trace << m_active[i].current_speed << ",";
		m_trace << m_active[i].resample << "\n";
		m_trace.flush();

		update_cum_sum(i);
	}

	std::cerr << "\n";
	std::cerr << "TP: " << tp << "\n";

	double jian = get_jian();

	m_trace_glob << s_since_start << ",";
	m_trace_glob << running.count() << ",";
	m_trace_glob << tp << ",";
	m_trace_glob << jian << "\n";
	m_trace_glob.flush();

	m_measured_at = now;

	if (measured_base)
		return MeasuredBase;

	return Unknown;
}

bool SchedulerFreezer::update_changed()
{
	bool updated = false;
	for (size_t i = 0; i < m_active.size(); ++i) {
		if (!m_active[i].running)
			continue;
		if (!m_active[i].flush)
			continue;

		m_logger.debug("Flushing task", m_active[i].task_id);

		m_active[i].base_speed.reset();
		m_active[i].start_speed.reset();
		m_active[i].interpolator.flush();

		for (size_t j = 0; j < m_active.size(); ++j) {
			if (m_active[j].active)
				m_active[j].interpolator.flush(i);
		}

		updated = true;
	}

	return updated;
}

void SchedulerFreezer::freeze_all()
{
	for (size_t i = 0; i < m_active.size(); ++i) {
		if (!m_active[i].running)
			continue;

		assert(m_active[i].active);
		assert(m_active[i].assignment);

		m_logger.debug("Freezing", m_active[i].task_id);
		m_active[i].assignment->freeze(true);
		m_active[i].running = false;
	}
}

void SchedulerFreezer::freeze_all_except(Active::id_type aid)
{
	for (size_t i = 0; i < m_active.size(); ++i) {
		if (!m_active[i].running)
			continue;
		if (i == aid)
			continue;

		assert(m_active[i].active);
		assert(m_active[i].assignment);

		m_logger.debug("Freezing", m_active[i].task_id);
		m_active[i].assignment->freeze(true);
		m_active[i].running = false;
	}
}

Scheduler::Result SchedulerFreezer::submit_stale(const TaskGraph &graph)
{
	Active::id_type aid = m_active.size();

	for (size_t i = 0; i < m_active.size(); ++i) {
		if (!m_active[i].active)
			continue;
		if (m_active[i].assignment)
			continue;

		aid = i;
		break;
	}

	if (aid >= m_active.size())
		return Unknown;

	freeze_all();

	Result result = submit(graph, m_active[aid].task_id);

	if (result != Submitted)
		return result;

	auto now = Command::clock_type::now();
	m_active_started_at = now;
	m_measured_at = now;

	m_active[aid].reset();
	m_active[aid].assignment = m_submitted[m_active[aid].task_id];
	m_active[aid].running = true;
	return result;
}

Scheduler::Result SchedulerFreezer::submit_resampled(const TaskGraph &graph)
{
	Active::id_type aid = m_active.size();

	for (size_t i = 0; i < m_active.size(); ++i) {
		if (!m_active[i].active)
			continue;
		if (!m_active[i].assignment)
			continue;
		if (!m_active[i].resample)
			continue;

		aid = i;
		break;
	}

	if (aid >= m_active.size())
		return Unknown;

	m_logger.debug("Resampling", m_active[aid].task_id);

	freeze_all_except(aid);

	if (!m_active[aid].running) {
		m_active[aid].assignment->freeze(false);
	}

	m_active[aid].resample = false;
	m_active[aid].flush = false;
	m_active[aid].running = true;

	auto now = Command::clock_type::now();
	m_active_started_at = now;
	m_measured_at = now;

	return WaitComplition;
}

double SchedulerFreezer::evaluate(const Taskset &ts) const
{
	assert(ts);

	double f_mean = 0;
	double f_upper = 0;

	double s_mean = 0;
	double s_upper = 0;
	// std::cerr << ts << ": ";
	for (size_t i = 0; i < m_active.size(); ++i) {
		if (!ts[i])
			continue;

		auto [a_mean, a_delta] = m_active[i].interpolator.get(ts);

		double a_upper = a_mean + a_delta;
		// std::cerr << a_mean << "+" << a_delta << " ";
		// std::cerr << a_mean << " ";

		s_mean += a_mean;
		s_upper += a_upper;

		double ri = std::pow(m_active[i].accel_sum, m_fairness_coeff);
		f_mean += a_mean / ri;
		f_upper += a_upper / ri;
	}

	f_mean *= std::pow(s_mean / ts.count(), m_efficiency_coeff);
	f_upper *= std::pow(s_upper / ts.count(), m_efficiency_coeff);
	if (f_upper < f_mean)
		f_upper = f_mean;

	double f = f_mean + (f_upper - f_mean) * m_ucb_coeff;
	// double f = f_mean;
	std::cerr << ts << ":  s" << s_mean;
	std::cerr << " e" << std::pow(s_mean / ts.count(), m_efficiency_coeff);
	std::cerr << " f" << f;
	std::cerr << std::endl;

	return f;
}

Taskset SchedulerFreezer::find_next_combination() const
{
	std::vector<size_t> ts_ids;
	for (size_t i = 0; i < m_active.size(); ++i) {
		if (!m_active[i].active || !m_active[i].assignment)
			continue;
		ts_ids.push_back(i);
	}

	assert(!ts_ids.empty());

	double s_max = 0;
	Taskset ts_max;
	Taskset ts_proxy;

	while (ts_proxy.next_combination(0, ts_ids.size())) {
		Taskset ts;

		for (size_t i = 0; i < ts_ids.size(); ++i) {
			if (!ts_proxy[i])
				continue;
			ts.set(ts_ids[i]);
		}

		double s = evaluate(ts);

		if (!ts_max || s > s_max) {
			s_max = s;
			ts_max = ts;
		}
	}

	assert(ts_max);
	return ts_max;
}

void SchedulerFreezer::run_next_combination(const Taskset &current, const Taskset &comb)
{
	for (size_t i = 0; i < m_active.size(); ++i) {
		m_active[i].start_speed.reset();

		if (!current[i] && !comb[i])
			continue;

		update_cum_sum(i);

		assert(m_active[i].assignment);

		if (current[i] && !comb[i]) {
			assert(m_active[i].running);
			m_logger.debug("Freezing ", m_active[i].task_id);
			m_active[i].assignment->freeze(true);
			m_active[i].running = false;
		}

		if (!current[i] && comb[i]) {
			assert(!m_active[i].running);
			m_logger.debug("Unfreezing ", m_active[i].task_id);
			m_active[i].assignment->freeze(false);
			m_active[i].running = true;
		}
	}

	auto now = Command::clock_type::now();
	m_active_started_at = now;
	m_measured_at = now;
}

void SchedulerFreezer::update_active_finished()
{
	for (size_t i = 0; i < m_active.size(); ++i) {
		if (!m_active[i].assignment)
			continue;
		if (!m_active[i].assignment->is_finished())
			continue;

		m_logger.debug("Task", m_active[i].task_id, "finished");
		m_active[i].assignment = nullptr;
		m_active[i].active = false;
		m_active[i].running = false;
		m_active[i].interpolator.flush();

		for (size_t j = 0; j < m_active.size(); ++j) {
			if (!m_active[j].active)
				m_active[j].interpolator.flush(i);
		}
	}
}

Scheduler::Result SchedulerFreezer::run(const TaskGraph &graph)
{
	auto dt_since_active = Command::clock_type::now() - m_active_started_at;
	double ms_since_active = std::chrono::duration_cast<std::chrono::milliseconds>(dt_since_active).count();

	Result result = update_finished();
	if (result != Unknown)
		return result;

	update_active_finished();

	if (m_finished.size() == graph.size())
		return Done;

	std::set<TaskGraph::task_id> schedulable = find_schedulable(graph);

	// m_logger.debug("Schedulable tasks", schedulable.size());
	if (!schedulable.empty())
		add_to_active(schedulable);

	Taskset running = get_running();
	// m_logger.debug("Now running tasks", running.count());

	bool measured_base = false;
	m_logger.debug("t", ms_since_active, "ts", running);

	if (running) {
		result = update_running(running);
		if (result == MeasuredBase)
			measured_base = true;
		else if (result != Unknown)
			return result;

		update_changed();

		std::cerr << "measured_base = " << measured_base << std::endl;
	}

	result = submit_resampled(graph);
	if (result != Unknown)
		return result;

	result = submit_stale(graph);
	if (result != Unknown)
		return result;

	// if (running) {
	// 	if (!measured_base && dt_since_active < time_unit) {
	// 		m_logger.debug("Wait full time unit");
	// 		return WaitComplition;
	// 	}
	// }

	size_t nr_active = 0;
	for (size_t i = 0; i < m_active.size(); ++i) {
		if (m_active[i].active && m_active[i].assignment)
			nr_active++;
	}

	for (size_t i = 0; i < m_active.size(); ++i) {
		if (m_active[i].active && m_active[i].assignment)
			m_active[i].interpolator.set_nr_tasks(nr_active);
	}

	std::cerr << "nr_active = " << nr_active << "\n";


	Taskset comb = find_next_combination();

	m_logger.debug("Found next:", comb);
	run_next_combination(running, comb);

	return WaitComplition;
}

void SchedulerFreezer::set_max_accel_delta(double v)
{
	m_max_accel_delta = v;
	m_logger.debug("max delta:", m_max_accel_delta);
}

void SchedulerFreezer::set_ucb_coeff(double v)
{
	m_ucb_coeff = v;
	m_logger.debug("m_ucb_coeff:", m_ucb_coeff);

}

void SchedulerFreezer::set_fairness_coeff(double v)
{
	m_fairness_coeff = v;
	m_logger.debug("fairness:", m_fairness_coeff);
}

void SchedulerFreezer::set_efficiency_coeff(double v)
{
	m_efficiency_coeff = v;
	m_logger.debug("efficiency:", m_efficiency_coeff);
}

