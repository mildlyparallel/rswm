#include <chrono>
#include <cassert>
#include <mutex>

#include "CommandPool.hh"

CommandPool::CommandPool()
: m_logger("commands")
, m_nr_spawned(0)
, m_last_id(0)
{  }

CommandPool::~CommandPool()
{ }

std::shared_ptr<Command> CommandPool::find(Command::id_type cid) const
{
	std::shared_lock lg(m_lock);

	if (auto it = m_commands.find(cid); it != m_commands.end())
		return it->second;

	return {};
}

void CommandPool::add_pending(std::shared_ptr<Command> command)
{
	assert(command);

	command->set_state(Command::Pending);

	std::unique_lock lg(m_lock);

	auto cid = command->get_id();

	// TODO: throw something instead
	assert(!m_commands.contains(cid));

	m_commands.insert({cid, command});

	m_logger.debug("New pending command, id", cid);
}

void CommandPool::add_with_next_id(std::shared_ptr<Command> command)
{
	assert(command);

	std::unique_lock lg(m_lock);

	auto cid = ++m_last_id;

	command->set_id(cid);

	assert(!m_commands.contains(cid));

	m_commands.insert({cid, command});

	m_logger.debug("New command, id", cid);
}

void CommandPool::stop_all(bool force)
{
	std::shared_lock lg(m_lock);
	for (auto [cid, cmd] : m_commands) {
		
		if (cmd->get_state() != Command::Spawned)
			continue;

		cmd->stop(force);
	}
}

void CommandPool::spawn_pending_commands(std::vector<std::shared_ptr<Command>> &spawned)
{
	while (true) {
		if (m_nr_spawned >= Config::max_comb_size) {
			m_logger.warning("Too much spawned, current", m_nr_spawned, "limit", Config::max_comb_size);
			return;
		}

		auto command = get_next_pending();
		if (!command) {
			m_logger.info("No pending, spawned", m_nr_spawned);
			return;
		}

		command->set_state(Command::Spawned);

		m_logger.info("Spawning command", command->get_id());

		command->spawn();

		spawned.push_back(command);

		m_nr_spawned++;
	}
}

std::shared_ptr<Command> CommandPool::get_next_pending() const
{
	std::shared_ptr<Command> command;

	Command::id_type min_id = std::numeric_limits<Command::id_type>::max();

	std::shared_lock lg(m_lock);

	for (const auto &[cid, cmd] : m_commands) {
		if (cmd->get_state() != Command::Pending)
			continue;

		if (min_id <= cid)
			continue;

		min_id = cid;
		command = cmd;
	}

	return command;
}

void CommandPool::stop_tardy_commands()
{
	static constexpr auto sec0 = std::chrono::seconds::zero();

	std::shared_lock lg(m_lock);

	for (auto [cid, command] : m_commands) {
		assert(command);

		if (command->get_state() != Command::Spawned)
			continue;

		auto dt = Command::clock_type::now() - command->get_state_update_tp();
		auto ts = command->get_timeout_soft();
		auto th = command->get_timeout_hard();

		// TODO: is it thread safe?
		if (th != sec0 && dt >= th)
			command->stop(true);
		else if (ts != sec0 && dt >= ts)
			command->stop(false);
	}
}

void CommandPool::read_command_stats(std::vector<std::shared_ptr<Command>> &updated)
{
	std::shared_lock lg(m_lock);

	for (auto [cid, command] : m_commands) {
		assert(command);

		if (command->get_state() != Command::Spawned)
			continue;

		command->read_stats();

		updated.push_back(command);
	}
}

void CommandPool::sync_finished_commands(std::vector<std::shared_ptr<Command>> &finished)
{
	std::unique_lock lg(m_lock);

	for (auto it = m_commands.begin(); it != m_commands.end();) {
		auto &command = it->second;

		if (command->get_state() != Command::Spawned) {
			++it;
			continue;
		}

		if (!command->sync()) {
			++it;
			continue;
		}

		finished.push_back(command);
		m_nr_spawned--;
		it = m_commands.erase(it);
	}
}

size_t CommandPool::size() const
{
	std::shared_lock lg(m_lock);
	return m_commands.size();
}
