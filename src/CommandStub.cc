#include <cassert>

#include "CommandStub.hh"

CommandStub::CommandStub()
{ }

CommandStub::CommandStub(const Task &other)
: Command(other)
{ }

// CommandStub::CommandStub(const Task &other, CommandStub::id_type cid)
// : Command(other, cid)
// { }

CommandStub::~CommandStub()
{ }

void CommandStub::spawn()
{
	assert(false);
}

bool CommandStub::sync()
{
	assert(false);
	return false;
}

void CommandStub::stop(bool force)
{
	assert(false);
}

void CommandStub::read_stats()
{
	assert(false);
}

void CommandStub::freeze(bool freeze)
{
	assert(false);
}
