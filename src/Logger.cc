#include <sstream>
#include <cstring>

#include "Logger.hh"

Logger::Logger(const std::string &scope)
: m_enabled(is_set_in_env(scope))
, m_scope(scope)
, m_level(Level::Info)
{
	set_level_from_env();
}

Logger::~Logger()
{  }

void Logger::set_scope(const std::string &scope)
{
	m_scope = scope;
	m_enabled = is_set_in_env(scope);
}

const std::string &Logger::get_scope() const
{
	return m_scope;
}

void Logger::set_enabled(bool en)
{
	m_enabled = en;
}

bool Logger::is_enabled() const
{
	return m_enabled;
}

Logger::Level Logger::get_level() const
{
	return m_level;
}

void Logger::set_level(Logger::Level level)
{
	m_level = level;
}

bool Logger::is_set_in_env(const std::string &scope)
{
	const char *e = std::getenv(scope_var_name);
	if (!e)
		return false;

	std::stringstream ss(e);
	std::string s;

	while (std::getline(ss, s, ',')) {
		if (s == "*")
			return true;
		if (scope.empty() && s == "root")
			return true;
		if (!scope.empty() && s == scope)
			return true;
	}

	return false;
}

void Logger::set_level_from_env()
{
	const char *e = std::getenv(level_var_name);
	if (!e)
		return;

	for (size_t i = 0; i < nr_levels; ++i) {
		if (std::strcmp(e, m_level_str[i]) != 0)
			continue;

		m_level = static_cast<Level>(i);
		return;
	}
}
