#include <iostream>
#include <cassert>
#include <functional>
#include <tuple>
#include <sstream>
#include <cassert>

#include "Poll.hh"
#include "WorkerPool.hh"

#include "rpc/Runtime.hh"
#include "rpc/Connection.hh"

#include "DispatcherRemote.hh"

DispatcherRemote::DispatcherRemote(WorkerPool &workers)
: m_workers(workers)
{  }

DispatcherRemote::~DispatcherRemote()
{  }

void DispatcherRemote::bind_methods(rpc::Runtime &runtime)
{
	runtime.bind(RegisterWorker, &DispatcherRemote::register_worker, this);

	runtime.bind(UnregisterWorker, &DispatcherRemote::unregister_worker, this);

	runtime.bind(UpdateCommandState, &DispatcherRemote::update_command_state, this);

	runtime.bind(UpdateCommandRates, &DispatcherRemote::update_command_rates, this);

	runtime.bind(RunRemoteCommand, &DispatcherRemote::run_remote_command, this);

	runtime.on_disconnect(&DispatcherRemote::on_disconnect, this);
}

Worker::id_type DispatcherRemote::register_worker(
	std::shared_ptr<rpc::Connection> con,
	const std::string &name
) {
	auto worker = std::make_shared<WorkerClient>(con);
	worker->set_name(name);

	Worker::id_type wid = m_workers.add(worker);

	if (m_poll)
		m_poll->notify();

	return wid;
}

void DispatcherRemote::unregister_worker(
	std::shared_ptr<rpc::Connection>,
	Worker::id_type wid
) {
	m_workers.remove(wid);
}

std::vector<std::shared_ptr<Worker>> DispatcherRemote::get_workers(size_t n) const
{
	return m_workers.get(n);
}

std::shared_ptr<Assignment> DispatcherRemote::assign(
	const Task &task,
	const std::vector<std::shared_ptr<Worker>> &workers
) {
	assert(!workers.empty());

	auto assignment = std::make_shared<Assignment>();
	assignment->create_commands(task, workers);
	m_assignments.add(assignment);

	m_logger.debug("Created assignment ", assignment->get_id());

	auto [wrk, cmd] = assignment->find();
	assert(wrk);
	submit_next_command(wrk, cmd);

	return assignment;
}

bool DispatcherRemote::run_remote_command(
	std::shared_ptr<rpc::Connection>,
	const std::string &hostname,
	const std::string &shellcmd
) {
	m_logger.debug("RunRemoteCommand():", hostname, shellcmd);

	if (hostname.empty() || shellcmd.empty()) {
		m_logger.error("RunRemoteCommand failed, empty args");
		return false;
	}

	auto [wrk, cmd] = m_assignments.find(hostname);

	if (wrk && cmd) {
		submit_next_command(wrk, cmd, shellcmd);
		return true;
	}

	m_logger.error("RunRemoteCommand failed, assignment is not found");
	return false;
}

void DispatcherRemote::submit_next_command(
	std::shared_ptr<Worker> worker,
	std::shared_ptr<Command> command,
	const std::string &shellcmd
) {
	assert(worker);
	assert(command);

	// TODO: check if the same command shoudl be started on all nodes

	if (!shellcmd.empty())
		command->set_shell_cmd(shellcmd);

	m_commands.add_with_next_id(command);

	worker->submit_command(command);
}

bool DispatcherRemote::update_command_state(
	std::shared_ptr<rpc::Connection> con,
	Command::id_type cid,
	Command::State state,
	int return_code
) {
	m_logger.debug("UpdateCommandState(): command", cid, "state", state, return_code);

	std::shared_ptr<Command> cmd = m_commands.find(cid);

	if (!cmd) {
		std::cerr << "Unknown command, cid " << cid << std::endl;
		return false;
	}

	cmd->set_state(state);

	if (state == Command::Exited)
		cmd->set_return_code(return_code);

	if (cmd->finished()) {
		if (m_poll)
			m_poll->notify();
	}

	return true;
}

void DispatcherRemote::update_command_rates(
	std::shared_ptr<rpc::Connection> con,
	Command::id_type cid,
	const ResourceStats &stats
) {
	m_logger.debug("UpdateCommandRates, command", cid);

	std::shared_ptr<Command> cmd = m_commands.find(cid);
	if (!cmd) {
		std::cerr << "Unknown command, cid " << cid << std::endl;
		return;
	}

	cmd->update_rates(stats);
}

void DispatcherRemote::on_disconnect(std::shared_ptr<rpc::Connection>)
{
	m_stopped = true;
	if (m_poll)
		m_poll->notify();
}

// void DispatcherRemote::heartbeat(Worker::id_type id, uint64_t ts_sec)
// {
// 	m_logger.log("Worker", id, "heartbeat", ts_sec);
//
// 	auto worker = m_workers.safe_lookup(id);
//
// 	if (!worker) {
// 		std::cerr << "Heartbeat from unknown worker: " << id << std::endl;
// 		reset_unknown(id);
// 		return;
// 	}
//
// 	bool was_active = worker->is_active();
//
// 	std::chrono::duration<uint64_t> tp(ts_sec);
// 	worker->set_last_heartbeat(Worker::timepoint_type(tp));
//
// 	if (!was_active && worker->is_active())
// 		m_workers_cv.notify_all();
// }

// bool DispatcherRemote::handle_command_finish(
// 	Worker::id_type wid,
// 	Command::id_type cid,
// 	Command::State state,
// 	int return_code
// ) {
// 	m_logger.log("HandleCommandFinish(): worker", wid, "command", cid, "state", state, return_code);
//
// 	auto worker = find(wid);
//
// 	if (!worker) {
// 		std::cerr << "Finished command from unknown worker: " << wid << std::endl;
// 		reset_unknown(wid);
// 		return true;
// 	}
//
// 	auto command = worker->commands().safe_lookup(cid);
//
// 	if (!command) {
// 		std::cerr << "Finished unknown command " << cid << " from worker " << wid << std::endl;
// 		std::abort();
// 	}
//
// 	command->safe_set_state(state, return_code);
//
// 	if (state == Command::Terminated || (state == Command::Exited && return_code != 0))
// 	{
// 		for (auto &a : m_assignments) {
// 			if (!a->find_host(command))
// 				continue;
// 			a->kill_all();
// 		}
// 	}
//
// 	bool ok = worker->commands().safe_remove(cid);
//
// 	m_workers_cv.notify_all();
//
// 	assert(ok);
//
// 	return ok;
// }

// void DispatcherRemote::handle_command_stats(
// 	Worker::id_type wid,
// 	Command::id_type cid,
// 	const ResourceStats &stats
// ) {
// 	m_logger.log("HandleCommandStats(): worker", wid, "command", cid);
//
// 	auto worker = find(wid);
//
// 	if (!worker)
// 		return;
//
// 	auto command = worker->commands().safe_lookup(cid);
//
// 	if (!command)
// 		return;
//
// 	command->update_rates(stats);
//
// 	wirte_stats(wid, command, stats);
// }

// void DispatcherRemote::start()
// {
// 	m_thread_stopped = false;
//
// 	m_thread = std::thread([=, this] () {
// 		listen();
// 	});
// }
//
// void DispatcherRemote::stop()
// {
// 	m_logger.log("Stopping pool thread");
// 	m_thread_stopped = true;
// 	if (m_thread.joinable()) {
// 		m_logger.log("Joining pool thread");
// 		m_thread.join();
// 	}
//
// 	{
// 		auto lg = m_workers.lock();
// 		for (auto &w : m_workers) {
// 			w.second->reset();
// 		}
// 	}
// }
