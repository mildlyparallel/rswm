#include <iostream>
#include <cassert>
#include <functional>
#include <tuple>
#include <sstream>
#include <cassert>

#include "Dispatcher.hh"

#include "Task.hh"
#include "Assignment.hh"
#include "Worker.hh"

Dispatcher::Dispatcher()
: m_logger("dispatcher")
, m_poll(nullptr)
{  }

Dispatcher::~Dispatcher()
{  }

std::shared_ptr<Assignment> Dispatcher::assign(
	const Task &task,
	const std::vector<std::shared_ptr<Worker>> &workers
) {
	assert(false);
	return {};
}

void Dispatcher::set_poll(Poll *poll)
{
	m_poll = poll;
}

bool Dispatcher::stopped() const
{
	return m_stopped;
}

