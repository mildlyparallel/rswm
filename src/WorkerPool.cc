#include <cassert>

#include "WorkerPool.hh"

WorkerPool::WorkerPool()
: m_logger("workers")
, m_last_id(0)
{  }

WorkerPool::~WorkerPool()
{  }

Worker::id_type WorkerPool::add(std::shared_ptr<Worker> worker)
{
	std::unique_lock lg(m_lock);

	Worker::id_type wid = ++m_last_id;
	worker->set_id(wid);

	if (worker->get_name().empty()) 
		add_without_name(worker);
	else
		add_with_name(worker);

	return wid;
}

void WorkerPool::add_with_name(std::shared_ptr<Worker> worker)
{
	assert(!worker->get_name().empty());

	auto wid = worker->get_id();

	m_logger.info("New worker, id:", wid, "name:", worker->get_name());

	for (auto it = m_workers.begin(); it != m_workers.end();) {
		auto &w = (*it).second;

		if (w->get_name() != worker->get_name()) {
			++it;
			continue;
		}

		m_logger.info("Worker with name", w->get_name(), "is already in the pool, removing it");

		w->stop();

		it = m_workers.erase(it);
	}

	m_workers.insert({wid, worker});
}

void WorkerPool::add_without_name(std::shared_ptr<Worker> worker)
{
	auto wid = worker->get_id();

	assert(!m_workers.contains(wid));

	m_logger.info("New worker, id:", wid, "without name");

	m_workers.insert({wid, worker});
}

void WorkerPool::remove(Worker::id_type wid)
{
	std::unique_lock lg(m_lock);

	auto it = m_workers.find(wid);
	if (it == m_workers.end()) {
		m_logger.warning("Cant remove worker. Id", wid, "not found");
		return;
	}

	m_workers.erase(it);
}

std::shared_ptr<Worker> WorkerPool::find(Worker::id_type wid) const
{
	std::shared_lock lg(m_lock);

	auto it = m_workers.find(wid);
	if (it == m_workers.end())
		return {};

	return it->second;
}

std::vector<std::shared_ptr<Worker>> WorkerPool::get(size_t n) const
{
	std::shared_lock lg(m_lock);

	std::vector<std::shared_ptr<Worker>> workers;

	if (n == 0)
		n = m_workers.size();

	if (m_workers.size() < n)
		return {};

	for (auto [i, w] : m_workers) {
		workers.push_back(w);
		if (workers.size() == n)
			break;
	}

	return workers;
}

size_t WorkerPool::size() const
{
	std::shared_lock lg(m_lock);
	return m_workers.size();
}

void WorkerPool::stop_all()
{
	std::shared_lock lg(m_lock);

	for (auto [wid, wrk] : m_workers)
		wrk->stop();
}
