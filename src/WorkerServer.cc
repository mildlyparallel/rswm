#include <iostream>
#include <cassert>
#include <thread>

#include "WorkerServer.hh"

#include "Poll.hh"
#include "CommandPool.hh"
#include "DispatcherClient.hh"

#include "rpc/Connection.hh"
#include "rpc/Runtime.hh"

#include "posix/CommandPosix.hh"

WorkerServer::WorkerServer(CommandPool &pool, DispatcherClient &dispatcher)
: m_dispatcher(dispatcher)
, m_pool(pool)
, m_poll(nullptr)
{ }

WorkerServer::~WorkerServer()
{ }

void WorkerServer::bind_methods(rpc::Runtime &runtime)
{
	runtime.bind(SubmitCommand, &WorkerServer::submit_command, this);
	runtime.bind(StopAllCommands, &WorkerServer::stop_all_commands, this);
	runtime.bind(Stop, &WorkerServer::stop, this);

	// m_dispatcher.bind(Reset, &WorkerServer::reset, this);
	// m_dispatcher.bind(Kill, &WorkerServer::kill, this);
	// m_dispatcher.bind(Freeze, &WorkerServer::freeze, this);
}

bool WorkerServer::submit_command(
	std::shared_ptr<rpc::Connection> con,
	const Command &command
) {
	auto cmd = std::make_shared<CommandPosix>(command);

	m_pool.add_pending(cmd);

	if (m_poll)
		m_poll->notify();

	return true;
}

void WorkerServer::stop_all_commands(
	std::shared_ptr<rpc::Connection> /*con*/,
	bool force
) {
	m_pool.stop_all(force);
}

// void WorkerServer::reset()
// {
// 	m_logger.log("Reset called");
// 	m_stopped = true;
// }
//
// void WorkerServer::kill(Command::id_type cid)
// {
// 	m_logger.log("Kill called for", cid);
//
// 	auto lg = m_commands.lock();
//
// 	std::shared_ptr<Command> command;
//
// 	for (auto it = m_commands.begin(); it != m_commands.end(); ++it) {
// 		if (it->second->get_id() == cid) {
// 			command = it->second;
// 			break;
// 		}
// 	}
//
// 	if (!command) {
// 		m_logger.log("Command is not found: ", cid);
// 		return;
// 	}
//
// 	if (command->get_state() != Command::Spawned)
// 		return;
//
// 	command->stop(true);
//
// 	m_logger.log("Killed (hard) command:", command);
// }
//
// void WorkerServer::freeze(Command::id_type cid, bool freeze)
// {
// 	m_logger.log("Publish freeze(", cid, freeze, ")");
//
// 	auto lg = m_commands.lock();
//
// 	for (auto it = m_commands.begin(); it != m_commands.end(); ++it) {
// 		if (it->second->get_id() == cid) {
// 			it->second->freeze(freeze);
// 			return;
// 		}
// 	}
// }

void WorkerServer::set_poll(Poll *poll)
{
	m_poll = poll;
}

void WorkerServer::stop(std::shared_ptr<rpc::Connection>)
{
	m_logger.debug("stop() called");
	m_stopped = true;

	if (m_poll)
		m_poll->notify();
}
