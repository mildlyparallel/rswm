#include <cassert>
#include <stdexcept>
#include <iostream>

#include "TaskGraph.hh"

TaskGraph::TaskGraph()
{  }

TaskGraph::~TaskGraph()
{  }

TaskGraph::task_id TaskGraph::add(const Task &task)
{
	size_t s = m_tasks.size();
	m_tasks.push_back({s, s, task});

	return s;
}

TaskGraph::task_id TaskGraph::add_next(const Task &task)
{
	if (m_tasks.empty())
		return add(task);

	size_t s = m_tasks.size();
	m_tasks.push_back({s, s - 1, task});

	return s;
}

TaskGraph::task_id TaskGraph::add_after(TaskGraph::task_id prev, const Task &task)
{
	if (prev >= m_tasks.size())
		throw std::out_of_range("Specified previous task is not found.");

	size_t s = m_tasks.size();
	m_tasks.push_back({s, prev, task});

	return s;
}

const TaskGraph::node_type &TaskGraph::node_at(TaskGraph::task_id i) const
{
	assert(i < m_tasks.size());
	return m_tasks.at(i);
}

const Task &TaskGraph::at(TaskGraph::task_id i) const
{
	assert(i < m_tasks.size());
	return m_tasks.at(i).task;
}

Task &TaskGraph::at(TaskGraph::task_id i)
{
	assert(i < m_tasks.size());
	return m_tasks.at(i).task;
}

void TaskGraph::clear()
{
	m_tasks.clear();
}

size_t TaskGraph::size() const
{
	return m_tasks.size();
}

bool TaskGraph::empty() const
{
	return m_tasks.empty();
}

TaskGraph::iterator TaskGraph::begin()
{
	return m_tasks.begin();
}

TaskGraph::iterator TaskGraph::end()
{
	return m_tasks.end();
}

TaskGraph::const_iterator TaskGraph::cbegin() const
{
	return m_tasks.cbegin();
}

TaskGraph::const_iterator TaskGraph::cend() const
{
	return m_tasks.end();
}


void TaskGraph::set_name(const std::string &name)
{
	m_name = name;
}

const std::string &TaskGraph::get_name() const
{
	return m_name;
}

