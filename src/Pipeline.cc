#include <signal.h>
#include <cassert>

#include "Pipeline.hh"

#include "Poll.hh"
#include "Worker.hh"
#include "WorkerPool.hh"
#include "DispatcherLocal.hh"
#include "CommandPool.hh"
#include "SchedulerLifo.hh"
#include "SchedulerFreezer.hh"
#include "Config.hh"

Pipeline::Pipeline()
: m_logger()
{  }

Pipeline::~Pipeline()
{  }

void Pipeline::run(const TaskGraph &graph)
{
	Poll poll;
	poll.create();
	poll.notify_on_signals({SIGINT, SIGHUP, SIGTERM, SIGQUIT, SIGCHLD});
	poll.notify_on_timer(Config::stats_poll_interval);

	WorkerPool workers;
	workers.add(std::make_shared<Worker>());

	CommandPool commands;
	DispatcherLocal dispatcher(commands);

	SchedulerFreezer scheduler(workers, dispatcher);
	scheduler.set_max_accel_delta(m_max_accel_delta);
	scheduler.set_ucb_coeff(m_ucb_coeff);
	scheduler.set_fairness_coeff(m_fairness_coeff);
	scheduler.set_efficiency_coeff(m_efficiency_coeff);

	Poll::Alerts alerts;

	std::vector<std::shared_ptr<Command>> state_updates, stats_updates;

	while (!alerts.have_signals({SIGINT, SIGHUP, SIGTERM, SIGQUIT})) {
		state_updates.clear();
		stats_updates.clear();

		if (alerts.timer) {
			commands.stop_tardy_commands();
			commands.read_command_stats(stats_updates);
			write_counters(stats_updates);
		}

		if (alerts.have_signal(SIGCHLD))
			commands.sync_finished_commands(state_updates);

		auto res = scheduler.run(graph);

		if (res == Scheduler::Submitted)
			commands.spawn_pending_commands(state_updates);

		if (res == Scheduler::Done || res == Scheduler::Failed)
			break;

		poll.wait(alerts);
	}

	poll.close();
}

void Pipeline::run_remote(const TaskGraph &graph)
{
	// Poll poll;
	// poll.create();
	// poll.notify_on_events();
	// poll.notify_on_signals({SIGINT, SIGHUP, SIGTERM, SIGQUIT});
	//
	// rpc::Server server;
	//
	// WorkerPool workers;
	//
	// DispatcherRemote dispatcher(workers);
	// dispatcher.set_poll(&poll);
	// dispatcher.bind_methods(server);
	//
	// server.listen(9123);
	//
	// SchedulerLifo scheduler(workers, dispatcher);
	//
	// Poll::Alerts alerts;
	//
	// while (alerts.signals.empty()) {
	//
	// 	if (alerts.events) {
	// 		if (scheduler.run(graph))
	// 			break;
	//
	// 		if (dispatcher.stopped())
	// 			break;
	// 	}
	//
	// 	poll.wait(alerts);
	// }
	//
	// workers.stop_all();
	// server.stop();
	// poll.close();
}

void Pipeline::write_profile(const std::filesystem::path &p)
{
	if (m_stat_file.good())
		m_stat_file.close();

	m_stat_file = std::ofstream(p);

	m_stat_file << "name,cid,wid";
	for (size_t i = 0; i < ResourceStats::nr_entries; ++i)
		m_stat_file << "," << ResourceStats::get_entry_name(static_cast<ResourceStats::Entry>(i));

	m_stat_file << "\n";
}

void Pipeline::write_counters(const std::vector<std::shared_ptr<Command>> &commands)
{
	if (m_stat_file.bad())
		return;

	for (auto cmd : commands) {
		assert(cmd);

		if (cmd->get_name().empty())
			m_stat_file << "NA";
		else
			m_stat_file << cmd->get_name();

		m_stat_file << "," << cmd->get_id();

		if (cmd->get_worker())
			m_stat_file << "," << cmd->get_worker()->get_id();
		else
			m_stat_file << ",NA";

		auto &stats = cmd->get_stats();
		for (auto e : stats.get())
			m_stat_file << "," << e;

		m_stat_file << "\n";
	}
}

void Pipeline::set_max_accel_delta(double v)
{
	m_max_accel_delta = v;
}

void Pipeline::set_ucb_coeff(double v)
{
	m_ucb_coeff = v;
}

void Pipeline::set_fairness_coeff(double v)
{
	m_fairness_coeff = v;
}

void Pipeline::set_efficiency_coeff(double v)
{
	m_efficiency_coeff = v;
}

