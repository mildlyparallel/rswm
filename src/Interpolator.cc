#include "Interpolator.hh"

Interpolator::Interpolator()
{
	flush();
}

Interpolator::Interpolator(size_t task_id, size_t nr_tasks)
: m_task_id(task_id)
, m_nr_tasks(nr_tasks)
{
	flush();
}

void Interpolator::set_task_id(size_t task_id)
{
	m_task_id = task_id;
}

void Interpolator::set_nr_tasks(size_t nr_tasks)
{
	m_nr_tasks = nr_tasks;
}

void Interpolator::add(const Taskset &ts, double val)
{
	for (auto &b : m_data) {
		if (b.ts != ts)
			continue;
		b.val = val;
		return;
	}

	Bound b;
	b.ts = ts;
	b.val = val;

	m_data.push_back(b);
}

std::pair<double, double> Interpolator::get(const Taskset &ts) const
{
	assert(m_nr_tasks > 0);
	std::optional<Bound> bound = find(ts);

	if (bound)
		return {bound->val, 0};

	Estimate est = estimate(ts);
	if (est.delta < 1e-5)
		est.delta = 0;
	return {est.mean, est.delta};
}

void Interpolator::flush()
{
	m_data.clear();
}

void Interpolator::flush(size_t task_id)
{
	std::vector<Bound> new_data;

	for (auto &b : m_data) {
		if (!b.ts[task_id]) {
			new_data.push_back(b);
		}
	}

	m_data = new_data;
}

void Interpolator::simplify()
{
	struct Intrp {
		double error;
		size_t ts_id;
		size_t sub_id;
		size_t sup_id;
	};

	std::vector<Intrp> intr_errors;
	std::vector<Bound> new_data;
	std::set<size_t> marked;

	bool deleted = true;

	while (deleted) {
		for (size_t i = 0; i < m_data.size(); ++i) {
			auto &b = m_data[i];
			Estimate est = estimate(b.ts);

			Intrp intr; 
			intr.error = std::abs(b.val - est.mean);
			intr.ts_id = i;
			intr.sub_id = find_id(est.sub);
			intr.sup_id = find_id(est.sup);

			intr_errors.push_back(intr);
		}

		std::sort(intr_errors.begin(), intr_errors.end(), [](const Intrp &a, const Intrp &b) {
			return a.error < b.error;
		});

		for (size_t i = 0; i < intr_errors.size(); ++i) {
			auto &intr = intr_errors[i];
			if (intr.error > m_min_error) {
				marked.insert(intr.ts_id);
				continue;
			}

			if (marked.contains(intr.ts_id))
				continue;

			if (intr.sub_id < m_data.size())
				marked.insert(intr.sub_id);
			if (intr.sup_id < m_data.size())
				marked.insert(intr.sup_id);
		}

		deleted = false;
		for (size_t i = 0; i < m_data.size(); ++i) {
			if (marked.contains(i)) {
				new_data.push_back(m_data[i]);
			} else {
				deleted = true;
			}
		}

		if (deleted)
			m_data = new_data;

		marked.clear();
		intr_errors.clear();
		new_data.clear();
	}
}


Interpolator::Estimate Interpolator::estimate(const Taskset &ts) const
{
	Bound sub;
	sub.val = 1;
	sub.ts.set(m_task_id);
	bool found_sub = false;

	Bound sup;
	sup.val = 1. / m_nr_tasks;
	sup.ts.set_all();
	bool found_sup = false;

	for (auto &b : m_data) {
		if (b.ts == ts)
			continue;

		if (b.ts.subset_of(ts)) {
			if (!found_sub || b.val < sub.val) {
				sub = b;
				found_sub = true;
			}
		}

		if (ts.subset_of(b.ts)) {
			if (!found_sup || b.val > sup.val) {
				sup = b;
				found_sup = true;
			}
		}
	}

	if (ts == sub.ts) {
		Estimate est;
		est.mean = 1;
		est.delta = 0;
		est.sup = sup.ts;
		return est;
	}

	// if (ts == sup.ts) {
	// 	Estimate est;
	// 	est.mean = sub.val / 2;
	// 	est.delta = sub.val / 2;
	// 	est.sub = sub.ts;
	// 	return est;
	// }

	return estimate(ts, sub, sup);
}

Interpolator::Estimate Interpolator::estimate(
	const Taskset &ts,
	const Bound &sub,
	const Bound &sup
) const {
	assert(ts != sub.ts);
	assert(ts != sup.ts);
	assert(sub.ts.subset_of(sup.ts));

	double x = ts.count();
	double x0 = sub.ts.count();
	double y0 = sub.val;
	double x1 = std::min(sup.ts.count(), m_nr_tasks);
	// double x1 = sup.ts.count();
	double y1 = sup.val;
	double y = interploate_liniear(x, x0, x1, y0, y1);

	Estimate est;
	est.mean = y;
	est.delta = std::min(y0 - y, y - y1);
	est.sup = sup.ts;
	est.sub = sub.ts;

	// if (ts.count() == m_nr_tasks - 1) {
	// 	std::cerr << "x0 = " << x0 << "\n";
	// 	std::cerr << "y0 = " << y0 << "\n";
	// 	std::cerr << "x1 = " << x1 << "\n";
	// 	std::cerr << "y1 = " << y1 << "\n";
	// 	std::cerr << "x  = " << x  << "\n";
	// 	std::cerr << "y  = " << y  << "\n";
	// 	std::cerr << "\n";
    //
	// }

	return est;
}

double Interpolator::interploate_liniear(
	double x,
	double x0, double x1,
	double y0, double y1
) {
	// if (x0 > x1) {
	// 	std::swap(x0, x1);
	// 	std::swap(y0, y1);
	// }

	assert(x0 <= x);
	assert(x1 >= x);
	assert(x1 > x0);

	double y = y0 + (y1 - y0) * (x - x0) / (x1 - x0);
	return y;
}

std::optional<Interpolator::Bound> Interpolator::find(const Taskset &ts) const
{
	for (auto &b : m_data) {
		if (b.ts == ts)
			return b;
	}

	return {};
}

size_t Interpolator::find_id(const Taskset &ts) const
{
	for (size_t i = 0; i < m_data.size(); ++i) {
		if (m_data[i].ts == ts)
			return i;
	}

	return m_data.size() + 1;
}

