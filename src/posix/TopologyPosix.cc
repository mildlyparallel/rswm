#include <fstream>
#include <filesystem>
#include <iostream>

#include "posix/TopologyPosix.hh"

TopologyPosix::TopologyPosix()
: m_sysfs("/sys/devices/system/")
{  }

TopologyPosix::~TopologyPosix()
{  }

void TopologyPosix::set_sysfs_prefix(const std::filesystem::path &p)
{
	m_sysfs = p;
}

void TopologyPosix::read()
{
	for (int cpu_id = 0; ; cpu_id++) {
		std::filesystem::path p = m_sysfs / "cpu/cpu";
		p += std::to_string(cpu_id);

		if (!std::filesystem::exists(p))
			break;

		p /= "topology"; ;

		if (!std::filesystem::exists(p))
			continue;

		m_threads.push_back(cpu_id);

		cpulist_type threads = read_cpulist(p / "thread_siblings");
		if (std::find(m_cores.begin(), m_cores.end(), threads) == m_cores.end())
			m_cores.push_back(threads);

		cpulist_type cores = read_cpulist(p / "core_siblings");
		if (std::find(m_sockets.begin(), m_sockets.end(), cores) == m_sockets.end())
			m_sockets.push_back(cores);
	}
}

TopologyPosix::cpulist_type TopologyPosix::read_cpulist(const std::filesystem::path &p)
{
	cpulist_type mask;

	// TODO: what is the max map size? definetely not 64 bits

	uint64_t m;
	std::ifstream in(p);
	in >> std::hex >> m;
	mask = m;

	return mask;
}

