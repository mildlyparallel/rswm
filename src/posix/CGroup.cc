#include <fstream>
#include <cassert>

#include <signal.h>

#include "posix/CGroup.hh"

CGroup::CGroup(const std::string &loggername)
: m_logger(loggername)
{ }

CGroup::~CGroup()
{
	try {
		if (!m_dir.empty()) {
			kill_all();
			std::filesystem::remove(m_dir);
		}
	} catch(...) { }
}

void CGroup::set_dir(const std::filesystem::path &dir)
{
	assert(!dir.empty());
	m_dir = dir;
}

const std::filesystem::path &CGroup::get_dir() const
{
	return m_dir;
}

void CGroup::create()
{
	assert(!m_dir.empty());

	if (!std::filesystem::exists(m_dir)) {
		m_logger.debug("Creating", m_dir);
		std::filesystem::create_directories(m_dir);
	} else {
		m_logger.debug(m_dir, "is not created");
	}
}

void CGroup::remove()
{
	assert(!m_dir.empty());

	kill_all();

	m_logger.debug("Removing", m_dir);
	// std::filesystem::remove(m_dir);
	m_dir.clear();
}

void CGroup::kill_all() const
{
	assert(!m_dir.empty());

	std::stringstream content;

	{
		std::ifstream in(get_pids_path());
		content << in.rdbuf();
	}

	int pid;
	while (content >> pid) {
		::kill(pid, SIGKILL);
	}
}

void CGroup::add_pid(unsigned pid)
{
	assert(!m_dir.empty());

	m_logger.debug("Adding PID", pid, "to", m_dir);

	std::ofstream out(m_dir / "cgroup.procs");
	out << pid << "\n";
}

std::filesystem::path CGroup::get_tgids_path() const
{
	assert(!m_dir.empty());
	return m_dir / "cgroup.procs";
}

std::filesystem::path CGroup::get_pids_path() const
{
	assert(!m_dir.empty());
	return m_dir / "tasks";
}

void CGroup::update(ResourceStats &/*stats*/) const
{ }

