#include <cstring>
#include <iostream>
#include <cassert>
#include <filesystem>

#include <errno.h>
#include <signal.h>
#include <sys/wait.h>
#include <sched.h>

#include "posix/CommandPosix.hh"
#include "posix/ProcFS.hh"
#include "Utils.hh"
#include "Config.hh"

#if ENABLE_PCAP
#include "posix/PCap.hh"
#endif

CommandPosix::CommandPosix()
: m_pid(-1)
{ }

CommandPosix::CommandPosix(const Command &other)
: Command(other)
, m_pid(-1)
{ }

CommandPosix::~CommandPosix()
{ 
	if (m_pid == -1)
		return;

	int rc = ::kill(-m_pid, SIGKILL);
	if (rc < 0)
		return;

	int st;
	pid_t p;

	do {
		p = ::waitpid(m_pid, &st, WNOHANG);
	} while (p <= -1 && errno == EINTR);
}

void CommandPosix::append_runtime_env()
{
	std::string hostnames;
	for (auto &h : get_hostnames())
		hostnames += h + ' ';

	add_env("RSWM_HOSTNAMES", hostnames);
	add_env("RSWM_HOSTNAME", get_hostname());
}

bool CommandPosix::validate()
{
	if (!get_workdir().empty() && !std::filesystem::is_directory(get_workdir())) {
		m_logger.warning("Workdir", get_workdir(), "is not a directory");
		return false;
	}

	if (get_file().empty()) {
		m_logger.warning("Command file is empty");
		return false;
	}

	std::filesystem::path fp;
	if (get_file().is_absolute())
		fp = get_file();
	else
		fp = get_workdir() / get_file();

	if (access(fp.c_str(), X_OK) != 0) {
		m_logger.warning("Command file", get_file(), "is not executable");
		return false;
	}

	if (!get_stdout_log().empty()) {
		std::filesystem::path p = get_stdout_log();
		p = p.parent_path();
		if (!p.empty() && !std::filesystem::is_directory(p)) {
			m_logger.warning("Parent of", p, "is not a directory");
			return false;
		}
	}

	if (!get_stderr_log().empty()) {
		std::filesystem::path p = get_stderr_log();
		p = p.parent_path();
		if (!p.empty() && !std::filesystem::is_directory(p)) {
			m_logger.warning("Parent of", p, "is not a directory");
			return false;
		}
	}

	return true;
}

void CommandPosix::spawn()
{
	assert(m_pid <= 0);

	append_runtime_env();

	if (!validate()) {
		set_state(Failed);
		return;
	}

	if (get_state() == Failed)

	m_logger.debug("About to fork", get_file(), "at workdir", get_workdir());

#if ENABLE_CGROUPS
	std::filesystem::path p(Config::cgroup_freezer_root);
	p /= std::string("rswm-") + std::to_string(get_id());

	m_freezer.set_dir(p);
	m_freezer.create();
	m_freezer.freeze(false);
#endif

	int rc;

	int pipe_fd[2] = {0};
	rc = pipe(pipe_fd);
	THROW_IF(rc < 0);

	m_pid = ::fork();
	THROW_IF(m_pid < 0);

	if (m_pid > 0) {
#if ENABLE_CGROUPS
		m_freezer.add_pid(m_pid);
#endif

#if ENABLE_PERF
		m_perf.start_listening(m_pid);
#endif

		do {
			int proceed = 1;
			rc = write(pipe_fd[1], &proceed, sizeof(int));
		} while (rc < 0 && errno == EINTR);
		THROW_IF(rc < 0);

		rc = close(pipe_fd[0]);
		THROW_IF(rc < 0);
		rc = close(pipe_fd[1]);
		THROW_IF(rc < 0);

		return;
	}

	do {
		int proceed = 0;
		rc = read(pipe_fd[0], &proceed, sizeof(int));
	} while (rc < 0 && errno == EINTR);
	THROW_IF(rc < 0);

	rc = close(pipe_fd[0]);
	THROW_IF(rc < 0);
	rc = close(pipe_fd[1]);
	THROW_IF(rc < 0);

	rc = setpgid(0, 0);
	THROW_IF(rc < 0);

	cd_to_workdir();

	auto argv = args_to_argv();
	auto envp = envs_to_envp();
	if (Config::command_process_gid >= 0) {
		rc = setgid(Config::command_process_gid);
		THROW_IF(rc < 0);
	}

	if (Config::command_process_uid >= 0) {
		rc = setuid(Config::command_process_uid);
		THROW_IF(rc < 0);
	}

	set_cpu_affinity();

	redirect_outputs();

	rc = execvpe(get_file().c_str(), argv, envp);
	THROW_IF(rc < 0);
}

char * const *CommandPosix::args_to_argv() const
{
	const auto &args = get_args();
	char const* *argv = new char const *[args.size() + 2]();

	argv[0] = get_file().c_str();

	for (size_t i = 0; i < args.size(); ++i)
		argv[i+1] = args[i].c_str();

	argv[args.size()+1] = NULL;

	return (char * const *)(argv);
}

char * const *CommandPosix::envs_to_envp() const
{
	size_t nr_environ = 0;

	if (get_inherit_env()) {
		while (environ[nr_environ])
			nr_environ++;
	}

	const auto &envs = get_envs();

	const char **envp = new const char *[nr_environ + envs.size() + 1]();
	for (size_t i = 0; i < nr_environ; ++i)
		envp[i] = environ[i];

	for (size_t i = 0; i < envs.size(); ++i)
		envp[nr_environ + i] = envs[i].c_str();

	envp[nr_environ + envs.size()] = NULL;

	return (char * const *)(envp);
}

bool CommandPosix::sync()
{
	assert(m_pid > 0);

	int st;
	pid_t p;

	do {
		p = ::waitpid(m_pid, &st, WNOHANG);
		THROW_IF(p <= -1 && errno != EINTR);
		// XXX: child may double-fork and errno would be ECHILD
	} while (p <= -1 && errno == EINTR);

	if (p == 0)
		return false;

	if (WIFEXITED(st)) {
		set_state(Exited);
		set_return_code(WEXITSTATUS(st));

		m_logger.debug("PID", m_pid, "exited with return code", get_return_code());
	} else {
		set_state(Terminated);
		m_logger.debug("PID", m_pid, "terminated");
	}

	m_pid = -1;

#if ENABLE_PERF
	m_perf.stop_listening();
#endif

#if ENABLE_CGROUPS
	m_freezer.remove();
#endif

	return true;
}

void CommandPosix::redirect_outputs()
{
	redirect_output(STDOUT_FILENO, get_stdout_log());

	redirect_output(STDERR_FILENO, get_stderr_log());

	if (get_redirect_err_to_out()) {
		int rc = dup2(STDOUT_FILENO, STDERR_FILENO);
		THROW_IF(rc < 0);
	}
}

void CommandPosix::redirect_output(int fd, const std::string &file)
{
	auto p = file.empty() ? "/dev/null" : file.c_str();

	FILE *fp = fopen(p, "w");
	THROW_IF(fp == nullptr);

	int rc = dup2(fileno(fp), fd);
	// TODO: handle EINTR?
	THROW_IF(rc < 0);

	rc = fclose(fp);
	THROW_IF(rc != 0);
}

void CommandPosix::cd_to_workdir() const
{
	if (get_workdir().empty())
		return;

	int rc = chdir(get_workdir().c_str());
	THROW_IF(rc < 0);
}

void CommandPosix::stop(bool force)
{
	if (m_pid == -1)
		return;

	if (force) {
		int rc = ::kill(-m_pid, SIGKILL);
		THROW_IF(rc < 0);
	} else {
		int rc = ::kill(-m_pid, SIGTERM);
		THROW_IF(rc < 0);
	}
}

void CommandPosix::set_cpu_affinity()
{
	if (get_cpulist().empty())
		return;

	// TODO: use cgoups instead

	// TODO: use CPU_ALLOC
	cpu_set_t cpuset;

	CPU_ZERO(&cpuset);

	for (int cpu : get_cpulist())
		CPU_SET(cpu, &cpuset);

	int rc = sched_setaffinity(getpid(), sizeof(cpuset), &cpuset);
	THROW_IF(rc == -1);
}

void CommandPosix::read_stats()
{
	assert(m_pid > 0);

	ResourceStats stats;

#if ENABLE_CGROUPS
	m_freezer.update(stats);
	ProcFS::update(stats, m_freezer.get_pids_path());

#if ENABLE_PCAP
	// PCap::update(stats, m_freezer.get_procs_path());
#endif
#endif

#if ENABLE_PERF
	m_perf.update(stats);
#endif

	stats.get(ResourceStats::Timestamp) = std::chrono::duration_cast<std::chrono::milliseconds>(
		std::chrono::system_clock::now().time_since_epoch()
	).count();

	m_rates.push(stats);
}

#if ENABLE_CGROUPS
void CommandPosix::freeze(bool freeze)
{
	m_freezer.freeze(freeze);
}
#else
void CommandPosix::freeze(bool)
{ }
#endif
