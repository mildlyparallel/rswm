#include <cassert>
#include <fstream>

#include "posix/CGroupFreezer.hh"

CGroupFreezer::CGroupFreezer()
: CGroup("cg-freezer")
{ }

CGroupFreezer::~CGroupFreezer()
{ }

CGroupFreezer::State CGroupFreezer::read_state() const
{
	assert(!m_dir.empty());

	std::string state;

	// TODO: store paths in class field?
	std::ifstream in(m_dir / "freezer.state");
	in >> state;

	if (state == "FROZEN")
		return State::Frozen;
	if (state == "FREEZING")
		return State::Freezing;
	if (state == "THAWED")
		return State::Thawed;

	m_logger.error("Unknown state", state, "in cgroup freezer", m_dir);

	assert(false);
	return State::Freezing;
}

void CGroupFreezer::freeze(bool freeze)
{
	std::ofstream out(m_dir / "freezer.state");

	if (freeze)
		out << "FROZEN\n";
	else
		out << "THAWED\n";
}

bool CGroupFreezer::is_frozen() const
{
	return read_state() == State::Frozen;
}

void CGroupFreezer::update(ResourceStats &stats) const
{
	stats.get(ResourceStats::Frozen) = is_frozen();
}
