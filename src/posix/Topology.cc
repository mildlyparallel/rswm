#include <fstream>
#include <filesystem>
#include <iostream>

#include "posix/Topology.hh"

Topology::Topology()
: m_sysfs("/sys/devices/system/")
{  }

Topology::~Topology()
{  }

void Topology::set_sysfs_prefix(const std::filesystem::path &p)
{
	m_sysfs = p;
}

void Topology::read()
{
	for (int cpu_id = 0; ; cpu_id++) {
		std::filesystem::path p = m_sysfs / "cpu/cpu";
		p += std::to_string(cpu_id);

		if (!std::filesystem::exists(p))
			break;

		p /= "topology"; ;

		if (!std::filesystem::exists(p))
			continue;

		m_threads.push_back(cpu_id);

		cpulist_type threads = read_cpulist(p / "thread_siblings");
		if (std::find(m_cores.begin(), m_cores.end(), threads) == m_cores.end())
			m_cores.push_back(threads);

		cpulist_type cores = read_cpulist(p / "core_siblings");
		if (std::find(m_sockets.begin(), m_sockets.end(), cores) == m_sockets.end())
			m_sockets.push_back(cores);
	}
}

size_t Topology::get_nr_threads() const
{
	return m_threads.size();
}

size_t Topology::get_nr_cores() const
{
	return m_cores.size();
}

size_t Topology::get_nr_sockets() const
{
	return m_sockets.size();
}

Topology::cpulist_type Topology::read_cpulist(const std::filesystem::path &p)
{
	cpulist_type mask;

	uint64_t m;
	std::ifstream in(p);
	in >> std::hex >> m;
	mask = m;

	return mask;
}


