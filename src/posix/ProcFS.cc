#include <cassert>
#include <fstream>

#include "posix/ProcFS.hh"

void ProcFS::update(
	ResourceStats &stats,
	const std::filesystem::path &threads
) {
	std::ifstream in(threads);
	if (!in.good())
		return;

	uint64_t tid;
	while (in >> tid) {
		thread_stat_t t = {0};
		t.id = tid;

		if (!fill_thread_stats(t))
			continue;

		if (!fill_thread_io(t))
			continue;

		count_thread(t, stats);
	}
}

bool ProcFS::fill_thread_stats(thread_stat_t &thr)
{
	std::filesystem::path p = "/proc";
	p /= std::to_string(thr.id);
	p /= "stat";

	std::string content;

	{
		std::ifstream in(p);
		content = std::string(
			std::istreambuf_iterator<char>(in),
			std::istreambuf_iterator<char>()
		);
	}

	if (content.empty())
		return false;

	size_t pos = content.find_last_of(')');

	if (pos == std::string::npos)
		return false;

	pos++;

	for (size_t i = 3; i < 16; i++) {
		pos++;

		size_t n = content.find(' ', pos);
		if (n == std::string::npos)
			return false;

		const std::string s = content.substr(pos, n - pos);

		if (i == 3)
			thr.state = s[0];
		if (i == 14)
			thr.utime = std::stoull(s);
		if (i == 15)
			thr.stime = std::stoull(s);

		pos = n;
	}

	return true;
}

bool ProcFS::fill_thread_io(thread_stat_t &thr)
{
	std::filesystem::path p = "/proc";
	p /= std::to_string(thr.id);
	p /= "io";

	std::stringstream content;

	try {
		std::ifstream in(p);
		content << in.rdbuf();
	} catch (...) {
		return false;
	}

	std::string key;
	std::string value;

	while (content >> key >> value) {
		uint64_t v = std::stoll(value);
		if (key == "rchar:")
			thr.rchar = v;
		if (key == "wchar:")
			thr.wchar = v;
		if (key == "read_bytes:")
			thr.read_bytes = v;
		if (key == "write_bytes:")
			thr.write_bytes = v;
	}

	return true;
}

void ProcFS::count_thread(const ProcFS::thread_stat_t &ts, ResourceStats &stats)
{
	stats.get(ResourceStats::Threads)++;

	if (ts.state == 'R')
		stats.get(ResourceStats::RunningThreads)++;

	stats.get(ResourceStats::UserTime) += ts.utime;
	stats.get(ResourceStats::SystemTime) += ts.stime;

	stats.get(ResourceStats::WriteChars) += ts.wchar;
	stats.get(ResourceStats::ReadChars) += ts.rchar;

	stats.get(ResourceStats::StorageWrite) += ts.write_bytes;
	stats.get(ResourceStats::StorageRead) += ts.read_bytes;
}

