#include "Topology.hh"

Topology::Topology()
{  }

Topology::~Topology()
{  }

size_t Topology::get_nr_threads() const
{
	return m_threads.size();
}

size_t Topology::get_nr_cores() const
{
	return m_cores.size();
}

size_t Topology::get_nr_sockets() const
{
	return m_sockets.size();
}

